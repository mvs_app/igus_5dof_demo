﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGUS_5DoF_Demo
{
    class kinematic_5dof_robot
	{
		//intialize constant parameter
		public const double RAD_TO_DEG = (180.0f / Math.PI);    //convert radian to degree
		public const double DEG_TO_RAD = (Math.PI / 180.0f);    //convert degree to radian

		//public const double PULSE_TO_RAD = 0.000628318;
		//public const double RAD_TO_PULSE = 1591.549431;

		// robot spec.
		private readonly double[] LINK_LENGTH;
		private readonly double[] JOINT_DISTANCE;
		private readonly double[] TWIST_ANGLE;
		private readonly double[] JOINT_ANGLE;

		private readonly AXESDATA GEARRATIO;

		//angle limit of 5 axes
		private readonly AXESDATA THETA_MIN_RAD;
		private readonly AXESDATA THETA_MAX_RAD;

		private readonly AXESDATA THETA_MIN_PULSE;
		private readonly AXESDATA THETA_MAX_PULSE;

		public string m_resultmessage;

		public kinematic_5dof_robot(double[] linklength, double[] jointdistance, double[] twistangle, double[] jointangle, AXESDATA gearratio, AXESDATA thetamin, AXESDATA thetamax)
		{
			LINK_LENGTH = linklength;
			JOINT_DISTANCE = jointdistance;
			TWIST_ANGLE = twistangle;
			JOINT_ANGLE = jointangle;

			GEARRATIO = gearratio / (Math.PI * 2);

			THETA_MIN_RAD = thetamin * (Math.PI * 2) / 360;

			THETA_MAX_RAD = thetamax * (Math.PI * 2) / 360;

			THETA_MIN_PULSE = thetamin * gearratio / 360;

			THETA_MAX_PULSE = thetamax * gearratio / 360;
		}

		////----------------------------------------------------------------
		public List<AXESDATA> Inverse_kine(COORDINATE coord)
		{
			// Inverse Kinematic
			//   : 3D coordinate ==> Axis[5] position
			List<AXESDATA> lstThetas = new List<AXESDATA>();

			double theta1, theta2_1, theta2_2, theta3_1, theta3_2, theta4_1, theta4_2, theta5, theta234;
			AXESDATA[] theta_temp = new AXESDATA[4] { new AXESDATA(), new AXESDATA(), new AXESDATA(), new AXESDATA() };
			double nx, ny, nz, ox, oy, oz, ax, ay, az; //initialize variables of rotation matrix
			double B, C, D;

			//calculate rotation matrix element by roll, pitch, yaw angle
			nx = Math.Cos(coord.pitch) * Math.Cos(coord.yaw);
			ny = Math.Cos(coord.pitch) * Math.Sin(coord.yaw);
			nz = -Math.Sin(coord.pitch);
			ox = -Math.Cos(coord.roll) * Math.Sin(coord.yaw) + Math.Cos(coord.yaw) * Math.Sin(coord.roll) * Math.Sin(coord.pitch);
			oy = Math.Cos(coord.roll) * Math.Cos(coord.yaw) + Math.Sin(coord.yaw) * Math.Sin(coord.roll) * Math.Sin(coord.pitch);
			oz = Math.Sin(coord.roll) * Math.Cos(coord.pitch);
			ax = Math.Sin(coord.roll) * Math.Sin(coord.yaw) + Math.Cos(coord.roll) * Math.Cos(coord.yaw) * Math.Sin(coord.pitch);
			ay = -Math.Sin(coord.roll) * Math.Cos(coord.yaw) + Math.Cos(coord.roll) * Math.Sin(coord.yaw) * Math.Sin(coord.pitch);
			az = Math.Cos(coord.roll) * Math.Cos(coord.pitch);
			//////////////////////////////////////////////////////////////////
			//// i_px = c1*(d5* s234 + a3*c23 + a2*c2)
			//// i_py = s1*(d5* s234 + a3*c23 + a2*c2)
			//// i_pz = a3*s23 – d5* c234 + a2*s2 + d1
			//// nx = s1*s5 + c234*c1*c5
			//// ny = c234*c5* s1 - c1* s5
			//// nz = s234*c5
			//// ox = c5* s1 - c234* c1* s5
			//// oy = - c1*c5 – c234* s1* s5
			//// oz = -s234* s5
			//// ax = s234*c1
			//// ay = s234* s1
			//// az = -c234
			/////////////////////////////////////////////////////////////////////////////
			if (Math.Abs(coord.x * ay - coord.y * ax) >= 0.01) // check inverse kinemstic has solution or not
			{
				m_resultmessage = "Inverse Kinematic : (x * ay - y * ax) are bigger than 0.01 !";
				return lstThetas;
			}

			//Calculate theta1
			theta1 = Math.Atan2(coord.y, coord.x);
			//Theta1 will have two solution in range (-PI,PI)

			for (int k = 0; k < 2; k++)
			{
				// k = 0: the first solution of theta1
				// k = 1: the second solution of theta1
				if (k == 1)
				{
					if (theta1 > 0)
						theta1 -= Math.PI;
					else
						theta1 += Math.PI;
				}

				//calculate the theta5
				theta5 = Math.Atan2(nx * Math.Sin(theta1) - ny * Math.Cos(theta1), ox * Math.Sin(theta1) - oy * Math.Cos(theta1));

				//calculate sum of theta2, theta3, theta4
				double temp = nz * Math.Cos(theta5) - oz * Math.Sin(theta5);
				theta234 = Math.Atan2(temp, -az); // theta234 = theta2 + theta3 + theta4

				/////////////////////////////////////////////////////////////////////////
				//// C = i_px * c1 + i_py * s1 - d5 * s234 = a3 * c23 + a2 * c2
				//// B = i_pz + d5 * (-az) - d1 = a3 * s23 +  a2 * s2
				//////////////////////////////////////////////////////////////////////
				C = coord.x * Math.Cos(theta1) + coord.y * Math.Sin(theta1) - JOINT_DISTANCE[4] * temp;
				B = coord.z + JOINT_DISTANCE[4] * (-az) - JOINT_DISTANCE[0];
				D = (B * B + C * C + LINK_LENGTH[1] * LINK_LENGTH[1] - LINK_LENGTH[2] * LINK_LENGTH[2]) / (LINK_LENGTH[1] * 2);
				/////////////////////
				if (D * D > (B * B + C * C))
				{
					// No solution available
					theta2_1 = 1000;
					theta2_2 = 1000;
					theta3_1 = 1000;
					theta3_2 = 1000;
					theta4_1 = 1000;
					theta4_2 = 1000;
				}
				else
				{
					//Bsin(theta2) + Ccos(theta2) = D
					//calculate theta2
					theta2_1 = Math.Atan2(D / Math.Sqrt(B * B + C * C), Math.Sqrt(1 - D * D / (B * B + C * C))) - Math.Atan2(C / Math.Sqrt(B * B + C * C), B / Math.Sqrt(B * B + C * C));
					theta2_2 = Math.Atan2(D / Math.Sqrt(B * B + C * C), -Math.Sqrt(1 - D * D / (B * B + C * C))) - Math.Atan2(C / Math.Sqrt(B * B + C * C), B / Math.Sqrt(B * B + C * C));

					//calculate theta3 = theta23 - theta2
					theta3_1 = Math.Atan2(B - LINK_LENGTH[1] * Math.Sin(theta2_1), C - LINK_LENGTH[1] * Math.Cos(theta2_1)) - theta2_1;
					theta3_2 = Math.Atan2(B - LINK_LENGTH[1] * Math.Sin(theta2_2), C - LINK_LENGTH[1] * Math.Cos(theta2_2)) - theta2_2;

					//calculate theta4 = theta234 - theta2 - theta3
					theta4_1 = theta234 - theta2_1 - theta3_1;
					theta4_2 = theta234 - theta2_2 - theta3_2;
				}

				//save angle of 5 axes to temp variable
				theta_temp[2 * k].Axis0 = theta1;
				theta_temp[2 * k].Axis1 = theta2_1 - Math.PI / 2;
				theta_temp[2 * k].Axis2 = theta3_1 + Math.PI / 2;
				theta_temp[2 * k].Axis3 = theta4_1;
				theta_temp[2 * k].Axis4 = theta5;

				theta_temp[2 * k + 1].Axis0 = theta1;
				theta_temp[2 * k + 1].Axis1 = theta2_2 - Math.PI / 2;
				theta_temp[2 * k + 1].Axis2 = theta3_2 + Math.PI / 2;
				theta_temp[2 * k + 1].Axis3 = theta4_2;
				theta_temp[2 * k + 1].Axis4 = theta5;
			}

			for (int i = 0; i < 4; i++)
			{
				// check angle of each axis is in the range (theta_min, theta_max) or not
				// if the angle of 5 axes are not out of range (theta_min, theta_max) => save
				if (CheckAxisInRangeRad(theta_temp[i]))
					lstThetas.Add(theta_temp[i]);
			}

			//count_theta: the number of solution
			return lstThetas;
		}

		public COORDINATE Forward_kine(AXESDATA pos)
		{
			// Forward Kinematic
			//   : Axis[5] position ==> 3D coordinate

			// check angle of each axis is in the range (theta_min, theta_max) or not
			if (CheckAxisInRangeRad(pos) == false)
			{
				m_resultmessage = "Forward Kinematic : Axis Position out of range!";
				return null;
			}

			double[] theta_k = new double[5];

			theta_k[0] = pos.Axis0 + JOINT_ANGLE[0];
			theta_k[1] = pos.Axis1 + JOINT_ANGLE[1];
			theta_k[2] = pos.Axis2 + JOINT_ANGLE[2];
			theta_k[3] = pos.Axis3 + JOINT_ANGLE[3];
			theta_k[4] = pos.Axis4 + JOINT_ANGLE[4];

			//link transformation matrix: transform coordinate frames 1 to 0
			double[,] T01 = Trans_Matrix(0, theta_k, TWIST_ANGLE, LINK_LENGTH, JOINT_DISTANCE);
			//link transformation matrix: transform coordinate frames 2 to 1
			double[,] T12 = Trans_Matrix(1, theta_k, TWIST_ANGLE, LINK_LENGTH, JOINT_DISTANCE);
			//link transformation matrix: transform coordinate frames 3 to 2
			double[,] T23 = Trans_Matrix(2, theta_k, TWIST_ANGLE, LINK_LENGTH, JOINT_DISTANCE);
			//link transformation matrix: transform coordinate frames 4 to 3
			double[,] T34 = Trans_Matrix(3, theta_k, TWIST_ANGLE, LINK_LENGTH, JOINT_DISTANCE);
			//link transformation matrix: transform coordinate frames 5 to 4
			double[,] T45 = Trans_Matrix(4, theta_k, TWIST_ANGLE, LINK_LENGTH, JOINT_DISTANCE);
			//link transformation matrix: transform coordinate frames 2 to 0
			double[,] T02 = MultiplyMatrix(T01, T12);
			//link transformation matrix: transform coordinate frames 3 to 0
			double[,] T03 = MultiplyMatrix(T02, T23);
			//link transformation matrix: transform coordinate frames 4 to 0
			double[,] T04 = MultiplyMatrix(T03, T34);
			//link transformation matrix: transform coordinate frames 5 to 0
			double[,] T05 = MultiplyMatrix(T04, T45);

			double nx = T05[0, 0];
			double ny = T05[1, 0];
			double nz = T05[2, 0];
			double ox = T05[0, 1];
			double oy = T05[1, 1];
			double oz = T05[2, 1];
			double ax = T05[0, 2];
			double ay = T05[1, 2];
			double az = T05[2, 2];
			double px = T05[0, 3];
			double py = T05[1, 3];
			double pz = T05[2, 3];

			// calculate position x, y, z
			COORDINATE coord = new COORDINATE();

			coord.x = px;
			coord.y = py;
			coord.z = pz;

			// calculate rotation roll, pitch, yaw
			if (nx == 0 && ny == 0)
			{
				if (nz == 1)
				{
					coord.roll = -Math.Atan2(ox, oy);
					coord.pitch = -Math.PI / 2;
					coord.yaw = 0;
				}
				else
				{
					coord.roll = Math.Atan2(ox, oy);
					coord.pitch = Math.PI / 2;
					coord.yaw = 0;
				}

				m_resultmessage = "Forward Kinematic : nx and ny are Zero!";
			}
			else
			{
				coord.roll = Math.Atan2(oz, az);
				coord.pitch = Math.Atan2(-nz, Math.Sqrt(nx * nx + ny * ny));
				coord.yaw = Math.Atan2(ny, nx);
			}

			return coord; //return 1 if forward kinematic has solution
		}

		// Homogenous Transformation Matrix
		private double[,] Trans_Matrix(int axis, double[] th_i, double[] al_i, double[] l_i, double[] d_i)
		{
			double[,] Temp = new double[4, 4];
			Temp[0, 0] = Math.Cos(th_i[axis]);
			Temp[0, 1] = -Math.Cos(al_i[axis]) * Math.Sin(th_i[axis]);
			Temp[0, 2] = Math.Sin(al_i[axis]) * Math.Sin(th_i[axis]);
			Temp[0, 3] = l_i[axis] * Math.Cos(th_i[axis]);
			Temp[1, 0] = Math.Sin(th_i[axis]);
			Temp[1, 1] = Math.Cos(al_i[axis]) * Math.Cos(th_i[axis]);
			Temp[1, 2] = -Math.Sin(al_i[axis]) * Math.Cos(th_i[axis]);
			Temp[1, 3] = l_i[axis] * Math.Sin(th_i[axis]);
			Temp[2, 0] = 0;
			Temp[2, 1] = Math.Sin(al_i[axis]);
			Temp[2, 2] = Math.Cos(al_i[axis]);
			Temp[2, 3] = d_i[axis];
			Temp[3, 0] = 0;
			Temp[3, 1] = 0;
			Temp[3, 2] = 0;
			Temp[3, 3] = 1;
			return Temp;
		}

		// Multiply 2 matrices
		private double[,] MultiplyMatrix(double[,] A, double[,] B)
		{
			int rA = A.GetLength(0);
			int cA = A.GetLength(1);
			//int rB = B.GetLength(0);
			int cB = B.GetLength(1);
			double[,] MTC = new double[rA, cB];
			double tempss;

			for (int i = 0; i < rA; i++)
			{
				for (int j = 0; j < cB; j++)
				{
					tempss = 0;
					for (int k = 0; k < cA; k++)
						tempss += A[i, k] * B[k, j];

					MTC[i, j] = tempss;
				}
			}

			return MTC;
		}

		// Convert Pulse to Radian
		public AXESDATA ConvertPulseToRadian(AXESDATA data)
		{
			data /= GEARRATIO;

			return data;
		}

		// Convert Radian to Pulse
		public AXESDATA ConvertRadianToPulse(AXESDATA data)
		{
			data *= GEARRATIO;

			return data;
		}

		// Convert Radian to Degree
		public AXESDATA ConvertRadianToDegree(AXESDATA data)
		{
			data *= kinematic_5dof_robot.RAD_TO_DEG;
			return data;
		}

		// Convert Degree to Radian
		public AXESDATA ConvertDegreeToRadian(AXESDATA data)
		{
			data *= kinematic_5dof_robot.DEG_TO_RAD;

			return data;
		}

		public bool CheckAxisInRangePulse(AXESDATA data)
		{
			// check angle of each axis is in the range (theta_min, theta_max) or not
			if (data < THETA_MIN_PULSE || data > THETA_MAX_PULSE)
				return false;

			return true;
		}

		public bool CheckAxisInRangeRad(AXESDATA data)
		{
			// check angle of each axis is in the range (theta_min, theta_max) or not
			if (data < (THETA_MIN_RAD / (1/0.9999))|| data > (THETA_MAX_RAD / (1 / 0.9999)))
				return false;

			return true;
		}
	}

	public class COORDINATE
	{
		public double x;        // [mm]
		public double y;        // [mm]
		public double z;        // [mm]
		public double roll;     // [rad]
		public double pitch;    // [rad]
		public double yaw;      // [rad]

		public COORDINATE()
		{
			x = 0;
			y = 0;
			z = 0;
			roll = 0;
			pitch = 0;
			yaw = 0;
		}
	}

	public struct AXESDATA
	{
		public double Axis0;
		public double Axis1;
		public double Axis2;
		public double Axis3;
		public double Axis4;

		public AXESDATA(double a0, double a1, double a2, double a3, double a4)
		{
			Axis0 = a0;
			Axis1 = a1;
			Axis2 = a2;
			Axis3 = a3;
			Axis4 = a4;
		}

		public static AXESDATA operator +(AXESDATA a, AXESDATA b)
		{
			AXESDATA c = new AXESDATA();

			c.Axis0 = a.Axis0 + b.Axis0;
			c.Axis1 = a.Axis1 + b.Axis1;
			c.Axis2 = a.Axis2 + b.Axis2;
			c.Axis3 = a.Axis3 + b.Axis3;
			c.Axis4 = a.Axis4 + b.Axis4;

			return c;
		}

		public static AXESDATA operator -(AXESDATA a, AXESDATA b)
		{
			AXESDATA c = new AXESDATA();

			c.Axis0 = a.Axis0 - b.Axis0;
			c.Axis1 = a.Axis1 - b.Axis1;
			c.Axis2 = a.Axis2 - b.Axis2;
			c.Axis3 = a.Axis3 - b.Axis3;
			c.Axis4 = a.Axis4 - b.Axis4;

			return c;
		}

		public static AXESDATA operator *(AXESDATA a, AXESDATA b)
		{
			AXESDATA c = new AXESDATA();

			c.Axis0 = a.Axis0 * b.Axis0;
			c.Axis1 = a.Axis1 * b.Axis1;
			c.Axis2 = a.Axis2 * b.Axis2;
			c.Axis3 = a.Axis3 * b.Axis3;
			c.Axis4 = a.Axis4 * b.Axis4;

			return c;
		}

		public static AXESDATA operator *(AXESDATA a, double b)
		{
			AXESDATA c = new AXESDATA();

			c.Axis0 = a.Axis0 * b;
			c.Axis1 = a.Axis1 * b;
			c.Axis2 = a.Axis2 * b;
			c.Axis3 = a.Axis3 * b;
			c.Axis4 = a.Axis4 * b;

			return c;
		}

		public static AXESDATA operator /(AXESDATA a, AXESDATA b)
		{
			AXESDATA c = new AXESDATA();

			c.Axis0 = a.Axis0 / b.Axis0;
			c.Axis1 = a.Axis1 / b.Axis1;
			c.Axis2 = a.Axis2 / b.Axis2;
			c.Axis3 = a.Axis3 / b.Axis3;
			c.Axis4 = a.Axis4 / b.Axis4;

			return c;
		}

		public static AXESDATA operator /(AXESDATA a, double b)
		{
			AXESDATA c = new AXESDATA();

			c.Axis0 = a.Axis0 / b;
			c.Axis1 = a.Axis1 / b;
			c.Axis2 = a.Axis2 / b;
			c.Axis3 = a.Axis3 / b;
			c.Axis4 = a.Axis4 / b;

			return c;
		}

		public static bool operator <(AXESDATA a, AXESDATA b)
		{
			if (a.Axis0 < b.Axis0) return true;
			if (a.Axis1 < b.Axis1) return true;
			if (a.Axis2 < b.Axis2) return true;
			if (a.Axis3 < b.Axis3) return true;
			if (a.Axis4 < b.Axis4) return true;

			return false;
		}

		public static bool operator >(AXESDATA a, AXESDATA b)
		{
			if (a.Axis0 > b.Axis0) return true;
			if (a.Axis1 > b.Axis1) return true;
			if (a.Axis2 > b.Axis2) return true;
			if (a.Axis3 > b.Axis3) return true;
			if (a.Axis4 > b.Axis4) return true;

			return false;
		}
	}

}

