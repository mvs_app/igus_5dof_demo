﻿
namespace IGUS_5DoF_Demo
{
    partial class MainForm
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txtBox_SysMsg = new System.Windows.Forms.TextBox();
            this.tabControl_Motion = new System.Windows.Forms.TabControl();
            this.tabPage_JointJog = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtBox_JointJogVel = new System.Windows.Forms.TextBox();
            this.btn_Joint0_Neg = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btn_Joint0_Pos = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_Joint1_Neg = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_Joint2_Neg = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_Joint3_Neg = new System.Windows.Forms.Button();
            this.txtBox_JointJogAcc = new System.Windows.Forms.TextBox();
            this.btn_Joint4_Neg = new System.Windows.Forms.Button();
            this.btn_Joint1_Pos = new System.Windows.Forms.Button();
            this.btn_Joint4_Pos = new System.Windows.Forms.Button();
            this.btn_Joint2_Pos = new System.Windows.Forms.Button();
            this.btn_Joint3_Pos = new System.Windows.Forms.Button();
            this.grpBox_SingleAxisHome = new System.Windows.Forms.GroupBox();
            this.btn_HomeJoint4 = new System.Windows.Forms.Button();
            this.btn_HomeJoint3 = new System.Windows.Forms.Button();
            this.btn_HomeJoint2 = new System.Windows.Forms.Button();
            this.btn_HomeJoint1 = new System.Windows.Forms.Button();
            this.btn_HomeJoint0 = new System.Windows.Forms.Button();
            this.tabPage_CarteJog = new System.Windows.Forms.TabPage();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.txtBox_CartJogAngle = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.txtBox_CartJogDist = new System.Windows.Forms.TextBox();
            this.txtBox_JogAngAcc = new System.Windows.Forms.TextBox();
            this.txtBox_JogAngVel = new System.Windows.Forms.TextBox();
            this.btn_RzPos = new System.Windows.Forms.Button();
            this.btn_RyPos = new System.Windows.Forms.Button();
            this.btn_RzNeg = new System.Windows.Forms.Button();
            this.btn_RyNeg = new System.Windows.Forms.Button();
            this.btn_RxPos = new System.Windows.Forms.Button();
            this.btn_RxNeg = new System.Windows.Forms.Button();
            this.btn_TzPos = new System.Windows.Forms.Button();
            this.btn_TyPos = new System.Windows.Forms.Button();
            this.btn_TzNeg = new System.Windows.Forms.Button();
            this.btn_TyNeg = new System.Windows.Forms.Button();
            this.btn_TxPos = new System.Windows.Forms.Button();
            this.btn_TxNeg = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtBox_CartJogAcc = new System.Windows.Forms.TextBox();
            this.txtBox_CartJogVel = new System.Windows.Forms.TextBox();
            this.tabPage_Motion = new System.Windows.Forms.TabPage();
            this.grpBox_MultipleShape = new System.Windows.Forms.GroupBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.txtBox_DemoVelocity = new System.Windows.Forms.TextBox();
            this.chckBox_DemoLoop = new System.Windows.Forms.CheckBox();
            this.bt_DemoStart = new System.Windows.Forms.Button();
            this.txtBox_bitAddr = new System.Windows.Forms.TextBox();
            this.btn_DemoStop = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBox_byteAddr = new System.Windows.Forms.TextBox();
            this.btn_LaserSwitch = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.grpBox_ProgrammedMotion = new System.Windows.Forms.GroupBox();
            this.txtBox_angVel = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.btn_Circular = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.txtBox_circNum = new System.Windows.Forms.TextBox();
            this.txtBox_LineVel = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtBox_SquareLength = new System.Windows.Forms.TextBox();
            this.txtBox_Radius = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.btn_LineMotion = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.grpBox_P2PMotion = new System.Windows.Forms.GroupBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.btn_StopP2P = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_P2PStart = new System.Windows.Forms.Button();
            this.txtBox_Rz = new System.Windows.Forms.TextBox();
            this.txtBox_Tz = new System.Windows.Forms.TextBox();
            this.txtBox_Rx = new System.Windows.Forms.TextBox();
            this.txtBox_Ry = new System.Windows.Forms.TextBox();
            this.txtBox_Ty = new System.Windows.Forms.TextBox();
            this.txtBox_Tx = new System.Windows.Forms.TextBox();
            this.txtBox_P2PAcc = new System.Windows.Forms.TextBox();
            this.txtBox_P2PVel = new System.Windows.Forms.TextBox();
            this.panel_IndHome = new System.Windows.Forms.Panel();
            this.label_IndHome = new System.Windows.Forms.Label();
            this.panel_IndMotor = new System.Windows.Forms.Panel();
            this.label_IndMotor = new System.Windows.Forms.Label();
            this.panel_IndCom = new System.Windows.Forms.Panel();
            this.label_IndCom = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.Btn_Stop = new System.Windows.Forms.Button();
            this.btn_ClearAlarm = new System.Windows.Forms.Button();
            this.btn_Home = new System.Windows.Forms.Button();
            this.panel_Status = new System.Windows.Forms.Panel();
            this.label37 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.btn_ClearSysMsg = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.dataGridView_RobotPos = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Position = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Angle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridView_MotorStatus = new System.Windows.Forms.DataGridView();
            this.Axis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Op = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PosCmd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActualPos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AngleAxis = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel_TitleBar = new System.Windows.Forms.Panel();
            this.picBox_Close = new System.Windows.Forms.PictureBox();
            this.label23 = new System.Windows.Forms.Label();
            this.circularMotionToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.btn_MotorsOff = new System.Windows.Forms.Button();
            this.btn_MotorOn = new System.Windows.Forms.Button();
            this.btn_StopComm = new System.Windows.Forms.Button();
            this.btn_StartComm = new System.Windows.Forms.Button();
            this.panel_Control = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabControl_Motion.SuspendLayout();
            this.tabPage_JointJog.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grpBox_SingleAxisHome.SuspendLayout();
            this.tabPage_CarteJog.SuspendLayout();
            this.tabPage_Motion.SuspendLayout();
            this.grpBox_MultipleShape.SuspendLayout();
            this.grpBox_ProgrammedMotion.SuspendLayout();
            this.grpBox_P2PMotion.SuspendLayout();
            this.panel_IndHome.SuspendLayout();
            this.panel_IndMotor.SuspendLayout();
            this.panel_IndCom.SuspendLayout();
            this.panel_Status.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_RobotPos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_MotorStatus)).BeginInit();
            this.panel_TitleBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox_Close)).BeginInit();
            this.panel_Control.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtBox_SysMsg
            // 
            this.txtBox_SysMsg.BackColor = System.Drawing.Color.White;
            this.txtBox_SysMsg.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_SysMsg.Location = new System.Drawing.Point(7, 575);
            this.txtBox_SysMsg.Multiline = true;
            this.txtBox_SysMsg.Name = "txtBox_SysMsg";
            this.txtBox_SysMsg.ReadOnly = true;
            this.txtBox_SysMsg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtBox_SysMsg.Size = new System.Drawing.Size(913, 149);
            this.txtBox_SysMsg.TabIndex = 1;
            // 
            // tabControl_Motion
            // 
            this.tabControl_Motion.Controls.Add(this.tabPage_JointJog);
            this.tabControl_Motion.Controls.Add(this.tabPage_CarteJog);
            this.tabControl_Motion.Controls.Add(this.tabPage_Motion);
            this.tabControl_Motion.Location = new System.Drawing.Point(611, 3);
            this.tabControl_Motion.Name = "tabControl_Motion";
            this.tabControl_Motion.SelectedIndex = 0;
            this.tabControl_Motion.Size = new System.Drawing.Size(314, 554);
            this.tabControl_Motion.TabIndex = 2;
            // 
            // tabPage_JointJog
            // 
            this.tabPage_JointJog.BackColor = System.Drawing.Color.White;
            this.tabPage_JointJog.Controls.Add(this.groupBox1);
            this.tabPage_JointJog.Controls.Add(this.grpBox_SingleAxisHome);
            this.tabPage_JointJog.Location = new System.Drawing.Point(4, 22);
            this.tabPage_JointJog.Name = "tabPage_JointJog";
            this.tabPage_JointJog.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_JointJog.Size = new System.Drawing.Size(306, 528);
            this.tabPage_JointJog.TabIndex = 0;
            this.tabPage_JointJog.Text = "Joint Control";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtBox_JointJogVel);
            this.groupBox1.Controls.Add(this.btn_Joint0_Neg);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.btn_Joint0_Pos);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btn_Joint1_Neg);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.btn_Joint2_Neg);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btn_Joint3_Neg);
            this.groupBox1.Controls.Add(this.txtBox_JointJogAcc);
            this.groupBox1.Controls.Add(this.btn_Joint4_Neg);
            this.groupBox1.Controls.Add(this.btn_Joint1_Pos);
            this.groupBox1.Controls.Add(this.btn_Joint4_Pos);
            this.groupBox1.Controls.Add(this.btn_Joint2_Pos);
            this.groupBox1.Controls.Add(this.btn_Joint3_Pos);
            this.groupBox1.Location = new System.Drawing.Point(5, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(300, 322);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Single Joint Jog";
            // 
            // txtBox_JointJogVel
            // 
            this.txtBox_JointJogVel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_JointJogVel.Location = new System.Drawing.Point(109, 20);
            this.txtBox_JointJogVel.Name = "txtBox_JointJogVel";
            this.txtBox_JointJogVel.Size = new System.Drawing.Size(100, 23);
            this.txtBox_JointJogVel.TabIndex = 6;
            this.txtBox_JointJogVel.Text = "10000";
            this.txtBox_JointJogVel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_Joint0_Neg
            // 
            this.btn_Joint0_Neg.BackColor = System.Drawing.Color.White;
            this.btn_Joint0_Neg.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_Joint0_Neg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Joint0_Neg.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Joint0_Neg.ForeColor = System.Drawing.Color.Black;
            this.btn_Joint0_Neg.Location = new System.Drawing.Point(57, 94);
            this.btn_Joint0_Neg.Name = "btn_Joint0_Neg";
            this.btn_Joint0_Neg.Size = new System.Drawing.Size(90, 38);
            this.btn_Joint0_Neg.TabIndex = 0;
            this.btn_Joint0_Neg.Tag = "-1";
            this.btn_Joint0_Neg.Text = "Joint 0 -";
            this.circularMotionToolTip.SetToolTip(this.btn_Joint0_Neg, "Joint 0 jog motion in the negative directio");
            this.btn_Joint0_Neg.UseVisualStyleBackColor = false;
            this.btn_Joint0_Neg.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_Jog_Click);
            this.btn_Joint0_Neg.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_Jog_Unclick);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(216, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 15);
            this.label6.TabIndex = 8;
            this.label6.Text = "pulse/s2";
            // 
            // btn_Joint0_Pos
            // 
            this.btn_Joint0_Pos.BackColor = System.Drawing.Color.White;
            this.btn_Joint0_Pos.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_Joint0_Pos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Joint0_Pos.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Joint0_Pos.ForeColor = System.Drawing.Color.Black;
            this.btn_Joint0_Pos.Location = new System.Drawing.Point(153, 94);
            this.btn_Joint0_Pos.Name = "btn_Joint0_Pos";
            this.btn_Joint0_Pos.Size = new System.Drawing.Size(90, 38);
            this.btn_Joint0_Pos.TabIndex = 0;
            this.btn_Joint0_Pos.Tag = "1";
            this.btn_Joint0_Pos.Text = "Joint 0 +";
            this.circularMotionToolTip.SetToolTip(this.btn_Joint0_Pos, "Joint 0 jog motion in the positive direction");
            this.btn_Joint0_Pos.UseVisualStyleBackColor = false;
            this.btn_Joint0_Pos.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_Jog_Click);
            this.btn_Joint0_Pos.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_Jog_Unclick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(46, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 15);
            this.label4.TabIndex = 8;
            this.label4.Text = "Accel";
            // 
            // btn_Joint1_Neg
            // 
            this.btn_Joint1_Neg.BackColor = System.Drawing.Color.White;
            this.btn_Joint1_Neg.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_Joint1_Neg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Joint1_Neg.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Joint1_Neg.ForeColor = System.Drawing.Color.Black;
            this.btn_Joint1_Neg.Location = new System.Drawing.Point(57, 140);
            this.btn_Joint1_Neg.Name = "btn_Joint1_Neg";
            this.btn_Joint1_Neg.Size = new System.Drawing.Size(90, 38);
            this.btn_Joint1_Neg.TabIndex = 1;
            this.btn_Joint1_Neg.Tag = "-1";
            this.btn_Joint1_Neg.Text = "Joint 1 -";
            this.circularMotionToolTip.SetToolTip(this.btn_Joint1_Neg, "Joint 1 jog motion in the negative directio");
            this.btn_Joint1_Neg.UseVisualStyleBackColor = false;
            this.btn_Joint1_Neg.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_Jog_Click);
            this.btn_Joint1_Neg.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_Jog_Unclick);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(215, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "pulse/s";
            // 
            // btn_Joint2_Neg
            // 
            this.btn_Joint2_Neg.BackColor = System.Drawing.Color.White;
            this.btn_Joint2_Neg.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_Joint2_Neg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Joint2_Neg.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Joint2_Neg.ForeColor = System.Drawing.Color.Black;
            this.btn_Joint2_Neg.Location = new System.Drawing.Point(57, 184);
            this.btn_Joint2_Neg.Name = "btn_Joint2_Neg";
            this.btn_Joint2_Neg.Size = new System.Drawing.Size(90, 38);
            this.btn_Joint2_Neg.TabIndex = 2;
            this.btn_Joint2_Neg.Tag = "-1";
            this.btn_Joint2_Neg.Text = "Joint 2 -";
            this.circularMotionToolTip.SetToolTip(this.btn_Joint2_Neg, "Joint 2 jog motion in the negative directio");
            this.btn_Joint2_Neg.UseVisualStyleBackColor = false;
            this.btn_Joint2_Neg.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_Jog_Click);
            this.btn_Joint2_Neg.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_Jog_Unclick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(44, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 15);
            this.label3.TabIndex = 8;
            this.label3.Text = "Velocity";
            // 
            // btn_Joint3_Neg
            // 
            this.btn_Joint3_Neg.BackColor = System.Drawing.Color.White;
            this.btn_Joint3_Neg.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_Joint3_Neg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Joint3_Neg.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Joint3_Neg.ForeColor = System.Drawing.Color.Black;
            this.btn_Joint3_Neg.Location = new System.Drawing.Point(57, 228);
            this.btn_Joint3_Neg.Name = "btn_Joint3_Neg";
            this.btn_Joint3_Neg.Size = new System.Drawing.Size(90, 38);
            this.btn_Joint3_Neg.TabIndex = 3;
            this.btn_Joint3_Neg.Tag = "-1";
            this.btn_Joint3_Neg.Text = "Joint 3 -";
            this.circularMotionToolTip.SetToolTip(this.btn_Joint3_Neg, "Joint 3 jog motion in the negative directio");
            this.btn_Joint3_Neg.UseVisualStyleBackColor = false;
            this.btn_Joint3_Neg.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_Jog_Click);
            this.btn_Joint3_Neg.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_Jog_Unclick);
            // 
            // txtBox_JointJogAcc
            // 
            this.txtBox_JointJogAcc.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_JointJogAcc.Location = new System.Drawing.Point(109, 47);
            this.txtBox_JointJogAcc.Name = "txtBox_JointJogAcc";
            this.txtBox_JointJogAcc.Size = new System.Drawing.Size(100, 23);
            this.txtBox_JointJogAcc.TabIndex = 6;
            this.txtBox_JointJogAcc.Text = "100000";
            this.txtBox_JointJogAcc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_Joint4_Neg
            // 
            this.btn_Joint4_Neg.BackColor = System.Drawing.Color.White;
            this.btn_Joint4_Neg.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_Joint4_Neg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Joint4_Neg.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Joint4_Neg.ForeColor = System.Drawing.Color.Black;
            this.btn_Joint4_Neg.Location = new System.Drawing.Point(57, 272);
            this.btn_Joint4_Neg.Name = "btn_Joint4_Neg";
            this.btn_Joint4_Neg.Size = new System.Drawing.Size(90, 38);
            this.btn_Joint4_Neg.TabIndex = 4;
            this.btn_Joint4_Neg.Tag = "-1";
            this.btn_Joint4_Neg.Text = "Joint 4 -";
            this.circularMotionToolTip.SetToolTip(this.btn_Joint4_Neg, "Joint 4 jog motion in the negative directio");
            this.btn_Joint4_Neg.UseVisualStyleBackColor = false;
            this.btn_Joint4_Neg.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_Jog_Click);
            this.btn_Joint4_Neg.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_Jog_Unclick);
            // 
            // btn_Joint1_Pos
            // 
            this.btn_Joint1_Pos.BackColor = System.Drawing.Color.White;
            this.btn_Joint1_Pos.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_Joint1_Pos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Joint1_Pos.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Joint1_Pos.ForeColor = System.Drawing.Color.Black;
            this.btn_Joint1_Pos.Location = new System.Drawing.Point(153, 140);
            this.btn_Joint1_Pos.Name = "btn_Joint1_Pos";
            this.btn_Joint1_Pos.Size = new System.Drawing.Size(90, 38);
            this.btn_Joint1_Pos.TabIndex = 1;
            this.btn_Joint1_Pos.Tag = "1";
            this.btn_Joint1_Pos.Text = "Joint 1 +";
            this.circularMotionToolTip.SetToolTip(this.btn_Joint1_Pos, "Joint 1 jog motion in the negative directio");
            this.btn_Joint1_Pos.UseVisualStyleBackColor = false;
            this.btn_Joint1_Pos.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_Jog_Click);
            this.btn_Joint1_Pos.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_Jog_Unclick);
            // 
            // btn_Joint4_Pos
            // 
            this.btn_Joint4_Pos.BackColor = System.Drawing.Color.White;
            this.btn_Joint4_Pos.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_Joint4_Pos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Joint4_Pos.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Joint4_Pos.ForeColor = System.Drawing.Color.Black;
            this.btn_Joint4_Pos.Location = new System.Drawing.Point(153, 272);
            this.btn_Joint4_Pos.Name = "btn_Joint4_Pos";
            this.btn_Joint4_Pos.Size = new System.Drawing.Size(90, 38);
            this.btn_Joint4_Pos.TabIndex = 4;
            this.btn_Joint4_Pos.Tag = "1";
            this.btn_Joint4_Pos.Text = "Joint 4 +";
            this.circularMotionToolTip.SetToolTip(this.btn_Joint4_Pos, "Joint 4 jog motion in the negative directio");
            this.btn_Joint4_Pos.UseVisualStyleBackColor = false;
            this.btn_Joint4_Pos.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_Jog_Click);
            this.btn_Joint4_Pos.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_Jog_Unclick);
            // 
            // btn_Joint2_Pos
            // 
            this.btn_Joint2_Pos.BackColor = System.Drawing.Color.White;
            this.btn_Joint2_Pos.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_Joint2_Pos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Joint2_Pos.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Joint2_Pos.ForeColor = System.Drawing.Color.Black;
            this.btn_Joint2_Pos.Location = new System.Drawing.Point(153, 184);
            this.btn_Joint2_Pos.Name = "btn_Joint2_Pos";
            this.btn_Joint2_Pos.Size = new System.Drawing.Size(90, 38);
            this.btn_Joint2_Pos.TabIndex = 2;
            this.btn_Joint2_Pos.Tag = "1";
            this.btn_Joint2_Pos.Text = "Joint 2 +";
            this.circularMotionToolTip.SetToolTip(this.btn_Joint2_Pos, "Joint 2 jog motion in the negative directio");
            this.btn_Joint2_Pos.UseVisualStyleBackColor = false;
            this.btn_Joint2_Pos.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_Jog_Click);
            this.btn_Joint2_Pos.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_Jog_Unclick);
            // 
            // btn_Joint3_Pos
            // 
            this.btn_Joint3_Pos.BackColor = System.Drawing.Color.White;
            this.btn_Joint3_Pos.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_Joint3_Pos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Joint3_Pos.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Joint3_Pos.ForeColor = System.Drawing.Color.Black;
            this.btn_Joint3_Pos.Location = new System.Drawing.Point(153, 228);
            this.btn_Joint3_Pos.Name = "btn_Joint3_Pos";
            this.btn_Joint3_Pos.Size = new System.Drawing.Size(90, 38);
            this.btn_Joint3_Pos.TabIndex = 3;
            this.btn_Joint3_Pos.Tag = "1";
            this.btn_Joint3_Pos.Text = "Joint 3 +";
            this.circularMotionToolTip.SetToolTip(this.btn_Joint3_Pos, "Joint 3 jog motion in the negative directio");
            this.btn_Joint3_Pos.UseVisualStyleBackColor = false;
            this.btn_Joint3_Pos.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_Jog_Click);
            this.btn_Joint3_Pos.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_Jog_Unclick);
            // 
            // grpBox_SingleAxisHome
            // 
            this.grpBox_SingleAxisHome.Controls.Add(this.btn_HomeJoint4);
            this.grpBox_SingleAxisHome.Controls.Add(this.btn_HomeJoint3);
            this.grpBox_SingleAxisHome.Controls.Add(this.btn_HomeJoint2);
            this.grpBox_SingleAxisHome.Controls.Add(this.btn_HomeJoint1);
            this.grpBox_SingleAxisHome.Controls.Add(this.btn_HomeJoint0);
            this.grpBox_SingleAxisHome.Location = new System.Drawing.Point(3, 333);
            this.grpBox_SingleAxisHome.Name = "grpBox_SingleAxisHome";
            this.grpBox_SingleAxisHome.Size = new System.Drawing.Size(300, 192);
            this.grpBox_SingleAxisHome.TabIndex = 9;
            this.grpBox_SingleAxisHome.TabStop = false;
            this.grpBox_SingleAxisHome.Text = "Single Joint Homing";
            // 
            // btn_HomeJoint4
            // 
            this.btn_HomeJoint4.BackColor = System.Drawing.Color.White;
            this.btn_HomeJoint4.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_HomeJoint4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_HomeJoint4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_HomeJoint4.ForeColor = System.Drawing.Color.Black;
            this.btn_HomeJoint4.Location = new System.Drawing.Point(107, 139);
            this.btn_HomeJoint4.Name = "btn_HomeJoint4";
            this.btn_HomeJoint4.Size = new System.Drawing.Size(93, 38);
            this.btn_HomeJoint4.TabIndex = 5;
            this.btn_HomeJoint4.Tag = "4";
            this.btn_HomeJoint4.Text = "Joint 4 Home";
            this.circularMotionToolTip.SetToolTip(this.btn_HomeJoint4, "Send Joint 4 to home position");
            this.btn_HomeJoint4.UseVisualStyleBackColor = false;
            this.btn_HomeJoint4.Click += new System.EventHandler(this.btn_SingleJointHome_Click);
            // 
            // btn_HomeJoint3
            // 
            this.btn_HomeJoint3.BackColor = System.Drawing.Color.White;
            this.btn_HomeJoint3.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_HomeJoint3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_HomeJoint3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_HomeJoint3.ForeColor = System.Drawing.Color.Black;
            this.btn_HomeJoint3.Location = new System.Drawing.Point(156, 86);
            this.btn_HomeJoint3.Name = "btn_HomeJoint3";
            this.btn_HomeJoint3.Size = new System.Drawing.Size(93, 38);
            this.btn_HomeJoint3.TabIndex = 4;
            this.btn_HomeJoint3.Tag = "3";
            this.btn_HomeJoint3.Text = "Joint 3 Home";
            this.circularMotionToolTip.SetToolTip(this.btn_HomeJoint3, "Send Joint 3 to home position");
            this.btn_HomeJoint3.UseVisualStyleBackColor = false;
            this.btn_HomeJoint3.Click += new System.EventHandler(this.btn_SingleJointHome_Click);
            // 
            // btn_HomeJoint2
            // 
            this.btn_HomeJoint2.BackColor = System.Drawing.Color.White;
            this.btn_HomeJoint2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_HomeJoint2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_HomeJoint2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_HomeJoint2.ForeColor = System.Drawing.Color.Black;
            this.btn_HomeJoint2.Location = new System.Drawing.Point(57, 86);
            this.btn_HomeJoint2.Name = "btn_HomeJoint2";
            this.btn_HomeJoint2.Size = new System.Drawing.Size(93, 38);
            this.btn_HomeJoint2.TabIndex = 3;
            this.btn_HomeJoint2.Tag = "2";
            this.btn_HomeJoint2.Text = "Joint 2 Home";
            this.circularMotionToolTip.SetToolTip(this.btn_HomeJoint2, "Send Joint 2 to home position");
            this.btn_HomeJoint2.UseVisualStyleBackColor = false;
            this.btn_HomeJoint2.Click += new System.EventHandler(this.btn_SingleJointHome_Click);
            // 
            // btn_HomeJoint1
            // 
            this.btn_HomeJoint1.BackColor = System.Drawing.Color.White;
            this.btn_HomeJoint1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_HomeJoint1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_HomeJoint1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_HomeJoint1.ForeColor = System.Drawing.Color.Black;
            this.btn_HomeJoint1.Location = new System.Drawing.Point(156, 32);
            this.btn_HomeJoint1.Name = "btn_HomeJoint1";
            this.btn_HomeJoint1.Size = new System.Drawing.Size(93, 38);
            this.btn_HomeJoint1.TabIndex = 2;
            this.btn_HomeJoint1.Tag = "1";
            this.btn_HomeJoint1.Text = "Joint 1 Home";
            this.circularMotionToolTip.SetToolTip(this.btn_HomeJoint1, "Send Joint 1 to home position");
            this.btn_HomeJoint1.UseVisualStyleBackColor = false;
            this.btn_HomeJoint1.Click += new System.EventHandler(this.btn_SingleJointHome_Click);
            // 
            // btn_HomeJoint0
            // 
            this.btn_HomeJoint0.BackColor = System.Drawing.Color.White;
            this.btn_HomeJoint0.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_HomeJoint0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_HomeJoint0.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_HomeJoint0.ForeColor = System.Drawing.Color.Black;
            this.btn_HomeJoint0.Location = new System.Drawing.Point(57, 32);
            this.btn_HomeJoint0.Name = "btn_HomeJoint0";
            this.btn_HomeJoint0.Size = new System.Drawing.Size(93, 38);
            this.btn_HomeJoint0.TabIndex = 1;
            this.btn_HomeJoint0.Tag = "0";
            this.btn_HomeJoint0.Text = "Joint 0 Home";
            this.circularMotionToolTip.SetToolTip(this.btn_HomeJoint0, "Send Joint 0 to home position");
            this.btn_HomeJoint0.UseVisualStyleBackColor = false;
            this.btn_HomeJoint0.Click += new System.EventHandler(this.btn_SingleJointHome_Click);
            // 
            // tabPage_CarteJog
            // 
            this.tabPage_CarteJog.BackColor = System.Drawing.Color.White;
            this.tabPage_CarteJog.Controls.Add(this.label52);
            this.tabPage_CarteJog.Controls.Add(this.label51);
            this.tabPage_CarteJog.Controls.Add(this.label50);
            this.tabPage_CarteJog.Controls.Add(this.label49);
            this.tabPage_CarteJog.Controls.Add(this.txtBox_CartJogAngle);
            this.tabPage_CarteJog.Controls.Add(this.label48);
            this.tabPage_CarteJog.Controls.Add(this.label47);
            this.tabPage_CarteJog.Controls.Add(this.txtBox_CartJogDist);
            this.tabPage_CarteJog.Controls.Add(this.txtBox_JogAngAcc);
            this.tabPage_CarteJog.Controls.Add(this.txtBox_JogAngVel);
            this.tabPage_CarteJog.Controls.Add(this.btn_RzPos);
            this.tabPage_CarteJog.Controls.Add(this.btn_RyPos);
            this.tabPage_CarteJog.Controls.Add(this.btn_RzNeg);
            this.tabPage_CarteJog.Controls.Add(this.btn_RyNeg);
            this.tabPage_CarteJog.Controls.Add(this.btn_RxPos);
            this.tabPage_CarteJog.Controls.Add(this.btn_RxNeg);
            this.tabPage_CarteJog.Controls.Add(this.btn_TzPos);
            this.tabPage_CarteJog.Controls.Add(this.btn_TyPos);
            this.tabPage_CarteJog.Controls.Add(this.btn_TzNeg);
            this.tabPage_CarteJog.Controls.Add(this.btn_TyNeg);
            this.tabPage_CarteJog.Controls.Add(this.btn_TxPos);
            this.tabPage_CarteJog.Controls.Add(this.btn_TxNeg);
            this.tabPage_CarteJog.Controls.Add(this.label30);
            this.tabPage_CarteJog.Controls.Add(this.label15);
            this.tabPage_CarteJog.Controls.Add(this.label28);
            this.tabPage_CarteJog.Controls.Add(this.label16);
            this.tabPage_CarteJog.Controls.Add(this.label27);
            this.tabPage_CarteJog.Controls.Add(this.label29);
            this.tabPage_CarteJog.Controls.Add(this.label17);
            this.tabPage_CarteJog.Controls.Add(this.label18);
            this.tabPage_CarteJog.Controls.Add(this.txtBox_CartJogAcc);
            this.tabPage_CarteJog.Controls.Add(this.txtBox_CartJogVel);
            this.tabPage_CarteJog.Location = new System.Drawing.Point(4, 22);
            this.tabPage_CarteJog.Name = "tabPage_CarteJog";
            this.tabPage_CarteJog.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_CarteJog.Size = new System.Drawing.Size(306, 528);
            this.tabPage_CarteJog.TabIndex = 1;
            this.tabPage_CarteJog.Text = "Cartesian Based Jog";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(85, 272);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(142, 14);
            this.label52.TabIndex = 23;
            this.label52.Text = "Reference: Tool Frame";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(85, 15);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(144, 14);
            this.label51.TabIndex = 22;
            this.label51.Text = "Reference: Base Frame";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(218, 354);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(61, 15);
            this.label50.TabIndex = 21;
            this.label50.Text = "degrees(°)";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(47, 354);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(38, 15);
            this.label49.TabIndex = 20;
            this.label49.Text = "Angle";
            // 
            // txtBox_CartJogAngle
            // 
            this.txtBox_CartJogAngle.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_CartJogAngle.Location = new System.Drawing.Point(110, 351);
            this.txtBox_CartJogAngle.Name = "txtBox_CartJogAngle";
            this.txtBox_CartJogAngle.Size = new System.Drawing.Size(100, 23);
            this.txtBox_CartJogAngle.TabIndex = 19;
            this.txtBox_CartJogAngle.Text = "10";
            this.txtBox_CartJogAngle.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(47, 94);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(53, 15);
            this.label48.TabIndex = 18;
            this.label48.Text = "Distance";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(218, 94);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(29, 15);
            this.label47.TabIndex = 17;
            this.label47.Text = "mm";
            // 
            // txtBox_CartJogDist
            // 
            this.txtBox_CartJogDist.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_CartJogDist.Location = new System.Drawing.Point(110, 91);
            this.txtBox_CartJogDist.Name = "txtBox_CartJogDist";
            this.txtBox_CartJogDist.Size = new System.Drawing.Size(100, 23);
            this.txtBox_CartJogDist.TabIndex = 16;
            this.txtBox_CartJogDist.Text = "100";
            this.txtBox_CartJogDist.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBox_JogAngAcc
            // 
            this.txtBox_JogAngAcc.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_JogAngAcc.Location = new System.Drawing.Point(110, 322);
            this.txtBox_JogAngAcc.Name = "txtBox_JogAngAcc";
            this.txtBox_JogAngAcc.Size = new System.Drawing.Size(100, 23);
            this.txtBox_JogAngAcc.TabIndex = 15;
            this.txtBox_JogAngAcc.Text = "0.1";
            this.txtBox_JogAngAcc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBox_JogAngVel
            // 
            this.txtBox_JogAngVel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_JogAngVel.Location = new System.Drawing.Point(110, 293);
            this.txtBox_JogAngVel.Name = "txtBox_JogAngVel";
            this.txtBox_JogAngVel.Size = new System.Drawing.Size(100, 23);
            this.txtBox_JogAngVel.TabIndex = 15;
            this.txtBox_JogAngVel.Text = "0.1";
            this.txtBox_JogAngVel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_RzPos
            // 
            this.btn_RzPos.BackColor = System.Drawing.Color.White;
            this.btn_RzPos.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_RzPos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_RzPos.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_RzPos.ForeColor = System.Drawing.Color.Black;
            this.btn_RzPos.Location = new System.Drawing.Point(175, 473);
            this.btn_RzPos.Name = "btn_RzPos";
            this.btn_RzPos.Size = new System.Drawing.Size(90, 38);
            this.btn_RzPos.TabIndex = 2;
            this.btn_RzPos.Tag = "1";
            this.btn_RzPos.Text = "Rz +";
            this.circularMotionToolTip.SetToolTip(this.btn_RzPos, "CCW rotation about the z axis");
            this.btn_RzPos.UseVisualStyleBackColor = false;
            this.btn_RzPos.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_CartJogAngle_Down);
            // 
            // btn_RyPos
            // 
            this.btn_RyPos.BackColor = System.Drawing.Color.White;
            this.btn_RyPos.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_RyPos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_RyPos.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_RyPos.ForeColor = System.Drawing.Color.Black;
            this.btn_RyPos.Location = new System.Drawing.Point(175, 429);
            this.btn_RyPos.Name = "btn_RyPos";
            this.btn_RyPos.Size = new System.Drawing.Size(90, 38);
            this.btn_RyPos.TabIndex = 1;
            this.btn_RyPos.Tag = "1";
            this.btn_RyPos.Text = "Ry +";
            this.circularMotionToolTip.SetToolTip(this.btn_RyPos, "CCW rotation about the y axis");
            this.btn_RyPos.UseVisualStyleBackColor = false;
            this.btn_RyPos.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_CartJogAngle_Down);
            // 
            // btn_RzNeg
            // 
            this.btn_RzNeg.BackColor = System.Drawing.Color.White;
            this.btn_RzNeg.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_RzNeg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_RzNeg.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_RzNeg.ForeColor = System.Drawing.Color.Black;
            this.btn_RzNeg.Location = new System.Drawing.Point(50, 473);
            this.btn_RzNeg.Name = "btn_RzNeg";
            this.btn_RzNeg.Size = new System.Drawing.Size(90, 38);
            this.btn_RzNeg.TabIndex = 2;
            this.btn_RzNeg.Tag = "-1";
            this.btn_RzNeg.Text = "Rz -";
            this.circularMotionToolTip.SetToolTip(this.btn_RzNeg, "CW rotation about the z axis");
            this.btn_RzNeg.UseVisualStyleBackColor = false;
            this.btn_RzNeg.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_CartJogAngle_Down);
            // 
            // btn_RyNeg
            // 
            this.btn_RyNeg.BackColor = System.Drawing.Color.White;
            this.btn_RyNeg.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_RyNeg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_RyNeg.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_RyNeg.ForeColor = System.Drawing.Color.Black;
            this.btn_RyNeg.Location = new System.Drawing.Point(50, 429);
            this.btn_RyNeg.Name = "btn_RyNeg";
            this.btn_RyNeg.Size = new System.Drawing.Size(90, 38);
            this.btn_RyNeg.TabIndex = 1;
            this.btn_RyNeg.Tag = "-1";
            this.btn_RyNeg.Text = "Ry -";
            this.circularMotionToolTip.SetToolTip(this.btn_RyNeg, "CW rotation about the y axis");
            this.btn_RyNeg.UseVisualStyleBackColor = false;
            this.btn_RyNeg.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_CartJogAngle_Down);
            // 
            // btn_RxPos
            // 
            this.btn_RxPos.BackColor = System.Drawing.Color.White;
            this.btn_RxPos.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_RxPos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_RxPos.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_RxPos.ForeColor = System.Drawing.Color.Black;
            this.btn_RxPos.Location = new System.Drawing.Point(175, 383);
            this.btn_RxPos.Name = "btn_RxPos";
            this.btn_RxPos.Size = new System.Drawing.Size(90, 38);
            this.btn_RxPos.TabIndex = 0;
            this.btn_RxPos.Tag = "1";
            this.btn_RxPos.Text = "Rx +";
            this.circularMotionToolTip.SetToolTip(this.btn_RxPos, "CCW rotation about the x axis");
            this.btn_RxPos.UseVisualStyleBackColor = false;
            this.btn_RxPos.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_CartJogAngle_Down);
            // 
            // btn_RxNeg
            // 
            this.btn_RxNeg.BackColor = System.Drawing.Color.White;
            this.btn_RxNeg.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_RxNeg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_RxNeg.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_RxNeg.ForeColor = System.Drawing.Color.Black;
            this.btn_RxNeg.Location = new System.Drawing.Point(50, 383);
            this.btn_RxNeg.Name = "btn_RxNeg";
            this.btn_RxNeg.Size = new System.Drawing.Size(90, 38);
            this.btn_RxNeg.TabIndex = 0;
            this.btn_RxNeg.Tag = "-1";
            this.btn_RxNeg.Text = "Rx -";
            this.circularMotionToolTip.SetToolTip(this.btn_RxNeg, "CW rotation about the x axis");
            this.btn_RxNeg.UseVisualStyleBackColor = false;
            this.btn_RxNeg.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_CartJogAngle_Down);
            // 
            // btn_TzPos
            // 
            this.btn_TzPos.BackColor = System.Drawing.Color.White;
            this.btn_TzPos.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_TzPos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_TzPos.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TzPos.ForeColor = System.Drawing.Color.Black;
            this.btn_TzPos.Location = new System.Drawing.Point(175, 214);
            this.btn_TzPos.Name = "btn_TzPos";
            this.btn_TzPos.Size = new System.Drawing.Size(90, 38);
            this.btn_TzPos.TabIndex = 2;
            this.btn_TzPos.Tag = "1";
            this.btn_TzPos.Text = "Tz +";
            this.circularMotionToolTip.SetToolTip(this.btn_TzPos, "Jog Motion in the positive Z direction");
            this.btn_TzPos.UseVisualStyleBackColor = false;
            this.btn_TzPos.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_CartJog_Down);
            // 
            // btn_TyPos
            // 
            this.btn_TyPos.BackColor = System.Drawing.Color.White;
            this.btn_TyPos.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_TyPos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_TyPos.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TyPos.ForeColor = System.Drawing.Color.Black;
            this.btn_TyPos.Location = new System.Drawing.Point(175, 170);
            this.btn_TyPos.Name = "btn_TyPos";
            this.btn_TyPos.Size = new System.Drawing.Size(90, 38);
            this.btn_TyPos.TabIndex = 1;
            this.btn_TyPos.Tag = "1";
            this.btn_TyPos.Text = "Ty +";
            this.circularMotionToolTip.SetToolTip(this.btn_TyPos, "Jog Motion in the positive Y direction");
            this.btn_TyPos.UseVisualStyleBackColor = false;
            this.btn_TyPos.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_CartJog_Down);
            // 
            // btn_TzNeg
            // 
            this.btn_TzNeg.BackColor = System.Drawing.Color.White;
            this.btn_TzNeg.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_TzNeg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_TzNeg.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TzNeg.ForeColor = System.Drawing.Color.Black;
            this.btn_TzNeg.Location = new System.Drawing.Point(50, 214);
            this.btn_TzNeg.Name = "btn_TzNeg";
            this.btn_TzNeg.Size = new System.Drawing.Size(90, 38);
            this.btn_TzNeg.TabIndex = 2;
            this.btn_TzNeg.Tag = "-1";
            this.btn_TzNeg.Text = "Tz -";
            this.circularMotionToolTip.SetToolTip(this.btn_TzNeg, "Jog Motion in the negative Z direction");
            this.btn_TzNeg.UseVisualStyleBackColor = false;
            this.btn_TzNeg.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_CartJog_Down);
            // 
            // btn_TyNeg
            // 
            this.btn_TyNeg.BackColor = System.Drawing.Color.White;
            this.btn_TyNeg.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_TyNeg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_TyNeg.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TyNeg.ForeColor = System.Drawing.Color.Black;
            this.btn_TyNeg.Location = new System.Drawing.Point(50, 170);
            this.btn_TyNeg.Name = "btn_TyNeg";
            this.btn_TyNeg.Size = new System.Drawing.Size(90, 38);
            this.btn_TyNeg.TabIndex = 1;
            this.btn_TyNeg.Tag = "-1";
            this.btn_TyNeg.Text = "Ty -";
            this.circularMotionToolTip.SetToolTip(this.btn_TyNeg, "Jog Motion in the negative Y direction");
            this.btn_TyNeg.UseVisualStyleBackColor = false;
            this.btn_TyNeg.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_CartJog_Down);
            // 
            // btn_TxPos
            // 
            this.btn_TxPos.BackColor = System.Drawing.Color.White;
            this.btn_TxPos.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_TxPos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_TxPos.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TxPos.ForeColor = System.Drawing.Color.Black;
            this.btn_TxPos.Location = new System.Drawing.Point(50, 124);
            this.btn_TxPos.Name = "btn_TxPos";
            this.btn_TxPos.Size = new System.Drawing.Size(90, 38);
            this.btn_TxPos.TabIndex = 0;
            this.btn_TxPos.Tag = "-1";
            this.btn_TxPos.Text = "Tx -";
            this.circularMotionToolTip.SetToolTip(this.btn_TxPos, "Jog Motion in the negative X direction");
            this.btn_TxPos.UseVisualStyleBackColor = false;
            this.btn_TxPos.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_CartJog_Down);
            // 
            // btn_TxNeg
            // 
            this.btn_TxNeg.BackColor = System.Drawing.Color.White;
            this.btn_TxNeg.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_TxNeg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_TxNeg.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_TxNeg.ForeColor = System.Drawing.Color.Black;
            this.btn_TxNeg.Location = new System.Drawing.Point(175, 124);
            this.btn_TxNeg.Name = "btn_TxNeg";
            this.btn_TxNeg.Size = new System.Drawing.Size(90, 38);
            this.btn_TxNeg.TabIndex = 0;
            this.btn_TxNeg.Tag = "1";
            this.btn_TxNeg.Text = "Tx +";
            this.circularMotionToolTip.SetToolTip(this.btn_TxNeg, "Jog Motion in the positive X direction");
            this.btn_TxNeg.UseVisualStyleBackColor = false;
            this.btn_TxNeg.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_CartJog_Down);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(218, 326);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 15);
            this.label30.TabIndex = 11;
            this.label30.Text = "rad/s2";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(217, 67);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 15);
            this.label15.TabIndex = 11;
            this.label15.Text = "mm/s^2";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(47, 322);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(36, 15);
            this.label28.TabIndex = 12;
            this.label28.Text = "Accel";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(47, 67);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 15);
            this.label16.TabIndex = 12;
            this.label16.Text = "Accel";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(45, 294);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(49, 15);
            this.label27.TabIndex = 14;
            this.label27.Text = "Velocity";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(217, 298);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(34, 15);
            this.label29.TabIndex = 13;
            this.label29.Text = "rad/s";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(216, 39);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(39, 15);
            this.label17.TabIndex = 13;
            this.label17.Text = "mm/s";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(45, 39);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(49, 15);
            this.label18.TabIndex = 14;
            this.label18.Text = "Velocity";
            // 
            // txtBox_CartJogAcc
            // 
            this.txtBox_CartJogAcc.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_CartJogAcc.Location = new System.Drawing.Point(110, 63);
            this.txtBox_CartJogAcc.Name = "txtBox_CartJogAcc";
            this.txtBox_CartJogAcc.Size = new System.Drawing.Size(100, 23);
            this.txtBox_CartJogAcc.TabIndex = 9;
            this.txtBox_CartJogAcc.Text = "160";
            this.txtBox_CartJogAcc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBox_CartJogVel
            // 
            this.txtBox_CartJogVel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_CartJogVel.Location = new System.Drawing.Point(110, 36);
            this.txtBox_CartJogVel.Name = "txtBox_CartJogVel";
            this.txtBox_CartJogVel.Size = new System.Drawing.Size(100, 23);
            this.txtBox_CartJogVel.TabIndex = 10;
            this.txtBox_CartJogVel.Text = "80";
            this.txtBox_CartJogVel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tabPage_Motion
            // 
            this.tabPage_Motion.BackColor = System.Drawing.Color.White;
            this.tabPage_Motion.Controls.Add(this.grpBox_MultipleShape);
            this.tabPage_Motion.Controls.Add(this.grpBox_ProgrammedMotion);
            this.tabPage_Motion.Controls.Add(this.grpBox_P2PMotion);
            this.tabPage_Motion.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Motion.Name = "tabPage_Motion";
            this.tabPage_Motion.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Motion.Size = new System.Drawing.Size(306, 528);
            this.tabPage_Motion.TabIndex = 2;
            this.tabPage_Motion.Text = "Motion";
            // 
            // grpBox_MultipleShape
            // 
            this.grpBox_MultipleShape.Controls.Add(this.label39);
            this.grpBox_MultipleShape.Controls.Add(this.label40);
            this.grpBox_MultipleShape.Controls.Add(this.txtBox_DemoVelocity);
            this.grpBox_MultipleShape.Controls.Add(this.chckBox_DemoLoop);
            this.grpBox_MultipleShape.Controls.Add(this.bt_DemoStart);
            this.grpBox_MultipleShape.Controls.Add(this.txtBox_bitAddr);
            this.grpBox_MultipleShape.Controls.Add(this.btn_DemoStop);
            this.grpBox_MultipleShape.Controls.Add(this.label1);
            this.grpBox_MultipleShape.Controls.Add(this.txtBox_byteAddr);
            this.grpBox_MultipleShape.Controls.Add(this.btn_LaserSwitch);
            this.grpBox_MultipleShape.Controls.Add(this.label38);
            this.grpBox_MultipleShape.Location = new System.Drawing.Point(5, 414);
            this.grpBox_MultipleShape.Name = "grpBox_MultipleShape";
            this.grpBox_MultipleShape.Size = new System.Drawing.Size(298, 110);
            this.grpBox_MultipleShape.TabIndex = 117;
            this.grpBox_MultipleShape.TabStop = false;
            this.grpBox_MultipleShape.Text = "Multiple Shape Demo";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(3, 17);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(59, 15);
            this.label39.TabIndex = 115;
            this.label39.Text = "Byte Addr";
            this.circularMotionToolTip.SetToolTip(this.label39, "Byte Address");
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(6, 43);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(50, 15);
            this.label40.TabIndex = 116;
            this.label40.Text = "Bit Addr";
            this.circularMotionToolTip.SetToolTip(this.label40, "Bit Address");
            // 
            // txtBox_DemoVelocity
            // 
            this.txtBox_DemoVelocity.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_DemoVelocity.Location = new System.Drawing.Point(247, 37);
            this.txtBox_DemoVelocity.Name = "txtBox_DemoVelocity";
            this.txtBox_DemoVelocity.Size = new System.Drawing.Size(31, 23);
            this.txtBox_DemoVelocity.TabIndex = 107;
            this.txtBox_DemoVelocity.Text = "60";
            this.txtBox_DemoVelocity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // chckBox_DemoLoop
            // 
            this.chckBox_DemoLoop.AutoSize = true;
            this.chckBox_DemoLoop.Location = new System.Drawing.Point(208, 74);
            this.chckBox_DemoLoop.Name = "chckBox_DemoLoop";
            this.chckBox_DemoLoop.Size = new System.Drawing.Size(52, 16);
            this.chckBox_DemoLoop.TabIndex = 108;
            this.chckBox_DemoLoop.Text = "Loop";
            this.circularMotionToolTip.SetToolTip(this.chckBox_DemoLoop, "If checked demo will repeat");
            this.chckBox_DemoLoop.UseVisualStyleBackColor = true;
            // 
            // bt_DemoStart
            // 
            this.bt_DemoStart.BackColor = System.Drawing.Color.White;
            this.bt_DemoStart.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.bt_DemoStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bt_DemoStart.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_DemoStart.ForeColor = System.Drawing.Color.Black;
            this.bt_DemoStart.Location = new System.Drawing.Point(105, 33);
            this.bt_DemoStart.Name = "bt_DemoStart";
            this.bt_DemoStart.Size = new System.Drawing.Size(87, 32);
            this.bt_DemoStart.TabIndex = 106;
            this.bt_DemoStart.Tag = "0";
            this.bt_DemoStart.Text = "Demo Start";
            this.circularMotionToolTip.SetToolTip(this.bt_DemoStart, "Tool Frame draws 3 shapes: Star, Circle, Square");
            this.bt_DemoStart.UseVisualStyleBackColor = false;
            this.bt_DemoStart.Click += new System.EventHandler(this.bt_DemoStart_Click);
            // 
            // txtBox_bitAddr
            // 
            this.txtBox_bitAddr.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_bitAddr.Location = new System.Drawing.Point(66, 41);
            this.txtBox_bitAddr.Name = "txtBox_bitAddr";
            this.txtBox_bitAddr.Size = new System.Drawing.Size(31, 23);
            this.txtBox_bitAddr.TabIndex = 114;
            this.txtBox_bitAddr.Text = "0";
            this.txtBox_bitAddr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_DemoStop
            // 
            this.btn_DemoStop.BackColor = System.Drawing.Color.White;
            this.btn_DemoStop.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btn_DemoStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_DemoStop.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_DemoStop.ForeColor = System.Drawing.Color.Black;
            this.btn_DemoStop.Location = new System.Drawing.Point(105, 69);
            this.btn_DemoStop.Name = "btn_DemoStop";
            this.btn_DemoStop.Size = new System.Drawing.Size(87, 32);
            this.btn_DemoStop.TabIndex = 109;
            this.btn_DemoStop.Tag = "0";
            this.btn_DemoStop.Text = "Demo Stop";
            this.btn_DemoStop.UseVisualStyleBackColor = false;
            this.btn_DemoStop.Click += new System.EventHandler(this.btn_DemoStop_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(279, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 15);
            this.label1.TabIndex = 110;
            this.label1.Text = "%";
            // 
            // txtBox_byteAddr
            // 
            this.txtBox_byteAddr.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_byteAddr.Location = new System.Drawing.Point(66, 15);
            this.txtBox_byteAddr.Name = "txtBox_byteAddr";
            this.txtBox_byteAddr.Size = new System.Drawing.Size(31, 23);
            this.txtBox_byteAddr.TabIndex = 113;
            this.txtBox_byteAddr.Text = "0";
            this.txtBox_byteAddr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_LaserSwitch
            // 
            this.btn_LaserSwitch.BackColor = System.Drawing.Color.White;
            this.btn_LaserSwitch.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btn_LaserSwitch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_LaserSwitch.Font = new System.Drawing.Font("Segoe UI Symbol", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_LaserSwitch.ForeColor = System.Drawing.Color.Black;
            this.btn_LaserSwitch.Location = new System.Drawing.Point(10, 69);
            this.btn_LaserSwitch.Name = "btn_LaserSwitch";
            this.btn_LaserSwitch.Size = new System.Drawing.Size(87, 32);
            this.btn_LaserSwitch.TabIndex = 112;
            this.btn_LaserSwitch.Tag = "0";
            this.btn_LaserSwitch.Text = "Laser On/Off";
            this.circularMotionToolTip.SetToolTip(this.btn_LaserSwitch, "Set IO Bit on and off");
            this.btn_LaserSwitch.UseVisualStyleBackColor = false;
            this.btn_LaserSwitch.Click += new System.EventHandler(this.btn_LaserSwitch_Click);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(196, 41);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(49, 15);
            this.label38.TabIndex = 111;
            this.label38.Text = "Velocity";
            // 
            // grpBox_ProgrammedMotion
            // 
            this.grpBox_ProgrammedMotion.Controls.Add(this.txtBox_angVel);
            this.grpBox_ProgrammedMotion.Controls.Add(this.label36);
            this.grpBox_ProgrammedMotion.Controls.Add(this.btn_Circular);
            this.grpBox_ProgrammedMotion.Controls.Add(this.label35);
            this.grpBox_ProgrammedMotion.Controls.Add(this.txtBox_circNum);
            this.grpBox_ProgrammedMotion.Controls.Add(this.txtBox_LineVel);
            this.grpBox_ProgrammedMotion.Controls.Add(this.label34);
            this.grpBox_ProgrammedMotion.Controls.Add(this.txtBox_SquareLength);
            this.grpBox_ProgrammedMotion.Controls.Add(this.txtBox_Radius);
            this.grpBox_ProgrammedMotion.Controls.Add(this.label32);
            this.grpBox_ProgrammedMotion.Controls.Add(this.label33);
            this.grpBox_ProgrammedMotion.Controls.Add(this.label31);
            this.grpBox_ProgrammedMotion.Controls.Add(this.label24);
            this.grpBox_ProgrammedMotion.Controls.Add(this.btn_LineMotion);
            this.grpBox_ProgrammedMotion.Controls.Add(this.label25);
            this.grpBox_ProgrammedMotion.Controls.Add(this.label26);
            this.grpBox_ProgrammedMotion.Location = new System.Drawing.Point(6, 238);
            this.grpBox_ProgrammedMotion.Name = "grpBox_ProgrammedMotion";
            this.grpBox_ProgrammedMotion.Size = new System.Drawing.Size(295, 174);
            this.grpBox_ProgrammedMotion.TabIndex = 11;
            this.grpBox_ProgrammedMotion.TabStop = false;
            this.grpBox_ProgrammedMotion.Text = "Programmed Motion";
            // 
            // txtBox_angVel
            // 
            this.txtBox_angVel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_angVel.Location = new System.Drawing.Point(97, 19);
            this.txtBox_angVel.Name = "txtBox_angVel";
            this.txtBox_angVel.Size = new System.Drawing.Size(52, 23);
            this.txtBox_angVel.TabIndex = 11;
            this.txtBox_angVel.Text = "2";
            this.txtBox_angVel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(163, 146);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(39, 15);
            this.label36.TabIndex = 104;
            this.label36.Text = "mm/s";
            // 
            // btn_Circular
            // 
            this.btn_Circular.BackColor = System.Drawing.Color.White;
            this.btn_Circular.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btn_Circular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Circular.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Circular.ForeColor = System.Drawing.Color.Black;
            this.btn_Circular.Location = new System.Drawing.Point(208, 22);
            this.btn_Circular.Name = "btn_Circular";
            this.btn_Circular.Size = new System.Drawing.Size(83, 47);
            this.btn_Circular.TabIndex = 8;
            this.btn_Circular.Tag = "0";
            this.btn_Circular.Text = "Circular Motion";
            this.circularMotionToolTip.SetToolTip(this.btn_Circular, "Circular motion on the x-y plane");
            this.btn_Circular.UseVisualStyleBackColor = false;
            this.btn_Circular.Click += new System.EventHandler(this.btn_Circular_Click);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(165, 117);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(29, 15);
            this.label35.TabIndex = 104;
            this.label35.Text = "mm";
            // 
            // txtBox_circNum
            // 
            this.txtBox_circNum.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_circNum.Location = new System.Drawing.Point(97, 75);
            this.txtBox_circNum.Name = "txtBox_circNum";
            this.txtBox_circNum.Size = new System.Drawing.Size(52, 23);
            this.txtBox_circNum.TabIndex = 12;
            this.txtBox_circNum.Text = "1";
            this.txtBox_circNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBox_LineVel
            // 
            this.txtBox_LineVel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_LineVel.Location = new System.Drawing.Point(95, 143);
            this.txtBox_LineVel.Name = "txtBox_LineVel";
            this.txtBox_LineVel.Size = new System.Drawing.Size(66, 23);
            this.txtBox_LineVel.TabIndex = 102;
            this.txtBox_LineVel.Text = "80";
            this.txtBox_LineVel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(154, 49);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(29, 15);
            this.label34.TabIndex = 104;
            this.label34.Text = "mm";
            // 
            // txtBox_SquareLength
            // 
            this.txtBox_SquareLength.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_SquareLength.Location = new System.Drawing.Point(95, 114);
            this.txtBox_SquareLength.Name = "txtBox_SquareLength";
            this.txtBox_SquareLength.Size = new System.Drawing.Size(66, 23);
            this.txtBox_SquareLength.TabIndex = 101;
            this.txtBox_SquareLength.Text = "60";
            this.txtBox_SquareLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBox_Radius
            // 
            this.txtBox_Radius.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_Radius.Location = new System.Drawing.Point(97, 46);
            this.txtBox_Radius.Name = "txtBox_Radius";
            this.txtBox_Radius.Size = new System.Drawing.Size(52, 23);
            this.txtBox_Radius.TabIndex = 100;
            this.txtBox_Radius.Text = "50";
            this.txtBox_Radius.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(40, 146);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(50, 14);
            this.label32.TabIndex = 15;
            this.label32.Text = "Velocity";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(153, 22);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(34, 15);
            this.label33.TabIndex = 103;
            this.label33.Text = "rad/s";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(5, 117);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(88, 14);
            this.label31.TabIndex = 15;
            this.label31.Text = "Square Length";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(6, 22);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(69, 14);
            this.label24.TabIndex = 15;
            this.label24.Text = "Angular Vel";
            // 
            // btn_LineMotion
            // 
            this.btn_LineMotion.BackColor = System.Drawing.Color.White;
            this.btn_LineMotion.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btn_LineMotion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_LineMotion.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_LineMotion.ForeColor = System.Drawing.Color.Black;
            this.btn_LineMotion.Location = new System.Drawing.Point(208, 117);
            this.btn_LineMotion.Name = "btn_LineMotion";
            this.btn_LineMotion.Size = new System.Drawing.Size(83, 46);
            this.btn_LineMotion.TabIndex = 8;
            this.btn_LineMotion.Tag = "0";
            this.btn_LineMotion.Text = "Line Motion";
            this.circularMotionToolTip.SetToolTip(this.btn_LineMotion, "Square motion in the X-Y Plane");
            this.btn_LineMotion.UseVisualStyleBackColor = false;
            this.btn_LineMotion.Click += new System.EventHandler(this.btn_LineMotion_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(6, 78);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(84, 14);
            this.label25.TabIndex = 15;
            this.label25.Text = "Num of Circles";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(6, 50);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(41, 14);
            this.label26.TabIndex = 16;
            this.label26.Text = "Radius";
            // 
            // grpBox_P2PMotion
            // 
            this.grpBox_P2PMotion.Controls.Add(this.label46);
            this.grpBox_P2PMotion.Controls.Add(this.label45);
            this.grpBox_P2PMotion.Controls.Add(this.label44);
            this.grpBox_P2PMotion.Controls.Add(this.label43);
            this.grpBox_P2PMotion.Controls.Add(this.label42);
            this.grpBox_P2PMotion.Controls.Add(this.label41);
            this.grpBox_P2PMotion.Controls.Add(this.label22);
            this.grpBox_P2PMotion.Controls.Add(this.label21);
            this.grpBox_P2PMotion.Controls.Add(this.btn_StopP2P);
            this.grpBox_P2PMotion.Controls.Add(this.label8);
            this.grpBox_P2PMotion.Controls.Add(this.label14);
            this.grpBox_P2PMotion.Controls.Add(this.label13);
            this.grpBox_P2PMotion.Controls.Add(this.label11);
            this.grpBox_P2PMotion.Controls.Add(this.label12);
            this.grpBox_P2PMotion.Controls.Add(this.label10);
            this.grpBox_P2PMotion.Controls.Add(this.label9);
            this.grpBox_P2PMotion.Controls.Add(this.label7);
            this.grpBox_P2PMotion.Controls.Add(this.btn_P2PStart);
            this.grpBox_P2PMotion.Controls.Add(this.txtBox_Rz);
            this.grpBox_P2PMotion.Controls.Add(this.txtBox_Tz);
            this.grpBox_P2PMotion.Controls.Add(this.txtBox_Rx);
            this.grpBox_P2PMotion.Controls.Add(this.txtBox_Ry);
            this.grpBox_P2PMotion.Controls.Add(this.txtBox_Ty);
            this.grpBox_P2PMotion.Controls.Add(this.txtBox_Tx);
            this.grpBox_P2PMotion.Controls.Add(this.txtBox_P2PAcc);
            this.grpBox_P2PMotion.Controls.Add(this.txtBox_P2PVel);
            this.grpBox_P2PMotion.Location = new System.Drawing.Point(6, 5);
            this.grpBox_P2PMotion.Name = "grpBox_P2PMotion";
            this.grpBox_P2PMotion.Size = new System.Drawing.Size(295, 231);
            this.grpBox_P2PMotion.TabIndex = 0;
            this.grpBox_P2PMotion.TabStop = false;
            this.grpBox_P2PMotion.Text = "P2P Motion";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(271, 151);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(12, 15);
            this.label46.TabIndex = 108;
            this.label46.Text = "°";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(271, 117);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(12, 15);
            this.label45.TabIndex = 108;
            this.label45.Text = "°";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(271, 86);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(12, 15);
            this.label44.TabIndex = 108;
            this.label44.Text = "°";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(121, 151);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(29, 15);
            this.label43.TabIndex = 107;
            this.label43.Text = "mm";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(121, 119);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(29, 15);
            this.label42.TabIndex = 106;
            this.label42.Text = "mm";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(121, 85);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(29, 15);
            this.label41.TabIndex = 105;
            this.label41.Text = "mm";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(155, 49);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(52, 15);
            this.label22.TabIndex = 10;
            this.label22.Text = "pulse/s2";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(155, 20);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(45, 15);
            this.label21.TabIndex = 9;
            this.label21.Text = "pulse/s";
            // 
            // btn_StopP2P
            // 
            this.btn_StopP2P.BackColor = System.Drawing.Color.White;
            this.btn_StopP2P.BackgroundImage = global::IGUS_5DoF_Demo.Properties.Resources.btn_motion_stop_n;
            this.btn_StopP2P.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_StopP2P.FlatAppearance.BorderSize = 0;
            this.btn_StopP2P.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btn_StopP2P.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btn_StopP2P.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_StopP2P.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_StopP2P.ForeColor = System.Drawing.Color.White;
            this.btn_StopP2P.Location = new System.Drawing.Point(162, 177);
            this.btn_StopP2P.Name = "btn_StopP2P";
            this.btn_StopP2P.Size = new System.Drawing.Size(122, 51);
            this.btn_StopP2P.TabIndex = 8;
            this.btn_StopP2P.Tag = "0";
            this.btn_StopP2P.UseVisualStyleBackColor = false;
            this.btn_StopP2P.Click += new System.EventHandler(this.btn_StopP2P_Click);
            this.btn_StopP2P.MouseEnter += new System.EventHandler(this.btn_StopP2P_MouseEnter);
            this.btn_StopP2P.MouseLeave += new System.EventHandler(this.btn_StopP2P_MouseLeave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = "Accel";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(167, 151);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(20, 15);
            this.label14.TabIndex = 7;
            this.label14.Text = "Rz";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(167, 120);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(20, 15);
            this.label13.TabIndex = 7;
            this.label13.Text = "Ry";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(13, 151);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 15);
            this.label11.TabIndex = 7;
            this.label11.Text = "Tz";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(167, 87);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(20, 15);
            this.label12.TabIndex = 7;
            this.label12.Text = "Rx";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(13, 120);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(19, 15);
            this.label10.TabIndex = 7;
            this.label10.Text = "Ty";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(13, 87);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(19, 15);
            this.label9.TabIndex = 7;
            this.label9.Text = "Tx";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 15);
            this.label7.TabIndex = 7;
            this.label7.Text = "Velocity";
            // 
            // btn_P2PStart
            // 
            this.btn_P2PStart.BackColor = System.Drawing.Color.White;
            this.btn_P2PStart.BackgroundImage = global::IGUS_5DoF_Demo.Properties.Resources.btn_motion_start_n1;
            this.btn_P2PStart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_P2PStart.FlatAppearance.BorderSize = 0;
            this.btn_P2PStart.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btn_P2PStart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btn_P2PStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_P2PStart.Font = new System.Drawing.Font("Segoe UI Symbol", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_P2PStart.ForeColor = System.Drawing.Color.White;
            this.btn_P2PStart.Location = new System.Drawing.Point(20, 177);
            this.btn_P2PStart.Name = "btn_P2PStart";
            this.btn_P2PStart.Size = new System.Drawing.Size(122, 51);
            this.btn_P2PStart.TabIndex = 8;
            this.btn_P2PStart.Tag = "0";
            this.circularMotionToolTip.SetToolTip(this.btn_P2PStart, "Moves to specified coordinates by Point to Point Motion");
            this.btn_P2PStart.UseVisualStyleBackColor = false;
            this.btn_P2PStart.Click += new System.EventHandler(this.btn_P2PStart_Click);
            this.btn_P2PStart.MouseEnter += new System.EventHandler(this.btn_P2PStart_MouseEnter);
            this.btn_P2PStart.MouseLeave += new System.EventHandler(this.btn_P2PStart_MouseLeave);
            // 
            // txtBox_Rz
            // 
            this.txtBox_Rz.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_Rz.Location = new System.Drawing.Point(192, 148);
            this.txtBox_Rz.Name = "txtBox_Rz";
            this.txtBox_Rz.Size = new System.Drawing.Size(73, 23);
            this.txtBox_Rz.TabIndex = 7;
            this.txtBox_Rz.Text = "0";
            this.txtBox_Rz.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBox_Tz
            // 
            this.txtBox_Tz.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_Tz.Location = new System.Drawing.Point(41, 148);
            this.txtBox_Tz.Name = "txtBox_Tz";
            this.txtBox_Tz.Size = new System.Drawing.Size(74, 23);
            this.txtBox_Tz.TabIndex = 7;
            this.txtBox_Tz.Text = "472";
            this.txtBox_Tz.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBox_Rx
            // 
            this.txtBox_Rx.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_Rx.Location = new System.Drawing.Point(192, 83);
            this.txtBox_Rx.Name = "txtBox_Rx";
            this.txtBox_Rx.Size = new System.Drawing.Size(73, 23);
            this.txtBox_Rx.TabIndex = 7;
            this.txtBox_Rx.Text = "180";
            this.txtBox_Rx.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBox_Ry
            // 
            this.txtBox_Ry.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_Ry.Location = new System.Drawing.Point(192, 115);
            this.txtBox_Ry.Name = "txtBox_Ry";
            this.txtBox_Ry.Size = new System.Drawing.Size(73, 23);
            this.txtBox_Ry.TabIndex = 7;
            this.txtBox_Ry.Text = "0";
            this.txtBox_Ry.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBox_Ty
            // 
            this.txtBox_Ty.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_Ty.Location = new System.Drawing.Point(41, 115);
            this.txtBox_Ty.Name = "txtBox_Ty";
            this.txtBox_Ty.Size = new System.Drawing.Size(74, 23);
            this.txtBox_Ty.TabIndex = 7;
            this.txtBox_Ty.Text = "0";
            this.txtBox_Ty.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBox_Tx
            // 
            this.txtBox_Tx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBox_Tx.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_Tx.Location = new System.Drawing.Point(41, 83);
            this.txtBox_Tx.Name = "txtBox_Tx";
            this.txtBox_Tx.Size = new System.Drawing.Size(74, 23);
            this.txtBox_Tx.TabIndex = 7;
            this.txtBox_Tx.Text = "270";
            this.txtBox_Tx.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBox_P2PAcc
            // 
            this.txtBox_P2PAcc.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_P2PAcc.Location = new System.Drawing.Point(61, 46);
            this.txtBox_P2PAcc.Name = "txtBox_P2PAcc";
            this.txtBox_P2PAcc.Size = new System.Drawing.Size(89, 23);
            this.txtBox_P2PAcc.TabIndex = 7;
            this.txtBox_P2PAcc.Text = "10000";
            this.txtBox_P2PAcc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtBox_P2PVel
            // 
            this.txtBox_P2PVel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_P2PVel.Location = new System.Drawing.Point(61, 17);
            this.txtBox_P2PVel.Name = "txtBox_P2PVel";
            this.txtBox_P2PVel.Size = new System.Drawing.Size(89, 23);
            this.txtBox_P2PVel.TabIndex = 7;
            this.txtBox_P2PVel.Text = "1000";
            this.txtBox_P2PVel.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel_IndHome
            // 
            this.panel_IndHome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(16)))), ((int)(((byte)(36)))));
            this.panel_IndHome.Controls.Add(this.label_IndHome);
            this.panel_IndHome.Location = new System.Drawing.Point(5, 323);
            this.panel_IndHome.Name = "panel_IndHome";
            this.panel_IndHome.Size = new System.Drawing.Size(237, 44);
            this.panel_IndHome.TabIndex = 8;
            // 
            // label_IndHome
            // 
            this.label_IndHome.BackColor = System.Drawing.Color.Transparent;
            this.label_IndHome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_IndHome.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_IndHome.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.label_IndHome.Location = new System.Drawing.Point(0, 0);
            this.label_IndHome.Name = "label_IndHome";
            this.label_IndHome.Size = new System.Drawing.Size(237, 44);
            this.label_IndHome.TabIndex = 7;
            this.label_IndHome.Text = "Home Not Done";
            this.label_IndHome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.circularMotionToolTip.SetToolTip(this.label_IndHome, "Indicates whether all joints are in home position or not");
            // 
            // panel_IndMotor
            // 
            this.panel_IndMotor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(16)))), ((int)(((byte)(36)))));
            this.panel_IndMotor.Controls.Add(this.label_IndMotor);
            this.panel_IndMotor.Location = new System.Drawing.Point(5, 185);
            this.panel_IndMotor.Name = "panel_IndMotor";
            this.panel_IndMotor.Size = new System.Drawing.Size(237, 44);
            this.panel_IndMotor.TabIndex = 8;
            // 
            // label_IndMotor
            // 
            this.label_IndMotor.BackColor = System.Drawing.Color.Transparent;
            this.label_IndMotor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_IndMotor.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_IndMotor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.label_IndMotor.Location = new System.Drawing.Point(0, 0);
            this.label_IndMotor.Name = "label_IndMotor";
            this.label_IndMotor.Size = new System.Drawing.Size(237, 44);
            this.label_IndMotor.TabIndex = 7;
            this.label_IndMotor.Text = "Motors Off";
            this.label_IndMotor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.circularMotionToolTip.SetToolTip(this.label_IndMotor, "Indicates whether all the motors are turned on or off");
            // 
            // panel_IndCom
            // 
            this.panel_IndCom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(16)))), ((int)(((byte)(36)))));
            this.panel_IndCom.Controls.Add(this.label_IndCom);
            this.panel_IndCom.Location = new System.Drawing.Point(5, 46);
            this.panel_IndCom.Name = "panel_IndCom";
            this.panel_IndCom.Size = new System.Drawing.Size(237, 44);
            this.panel_IndCom.TabIndex = 8;
            // 
            // label_IndCom
            // 
            this.label_IndCom.BackColor = System.Drawing.Color.Transparent;
            this.label_IndCom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_IndCom.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_IndCom.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.label_IndCom.Location = new System.Drawing.Point(0, 0);
            this.label_IndCom.Name = "label_IndCom";
            this.label_IndCom.Size = new System.Drawing.Size(237, 44);
            this.label_IndCom.TabIndex = 7;
            this.label_IndCom.Text = "Communication Stopped";
            this.label_IndCom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.circularMotionToolTip.SetToolTip(this.label_IndCom, "Indicates whether communication has started or stopped");
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(57, 15);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(133, 19);
            this.label19.TabIndex = 6;
            this.label19.Text = "System Control";
            // 
            // Btn_Stop
            // 
            this.Btn_Stop.BackColor = System.Drawing.Color.White;
            this.Btn_Stop.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.Btn_Stop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_Stop.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Stop.ForeColor = System.Drawing.Color.Black;
            this.Btn_Stop.Location = new System.Drawing.Point(5, 504);
            this.Btn_Stop.Name = "Btn_Stop";
            this.Btn_Stop.Size = new System.Drawing.Size(237, 54);
            this.Btn_Stop.TabIndex = 3;
            this.Btn_Stop.Text = "Stop All Motors";
            this.circularMotionToolTip.SetToolTip(this.Btn_Stop, "Stop All Motors");
            this.Btn_Stop.UseVisualStyleBackColor = false;
            this.Btn_Stop.Click += new System.EventHandler(this.Btn_Stop_Click);
            // 
            // btn_ClearAlarm
            // 
            this.btn_ClearAlarm.BackColor = System.Drawing.Color.White;
            this.btn_ClearAlarm.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_ClearAlarm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ClearAlarm.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ClearAlarm.ForeColor = System.Drawing.Color.Black;
            this.btn_ClearAlarm.Location = new System.Drawing.Point(4, 440);
            this.btn_ClearAlarm.Name = "btn_ClearAlarm";
            this.btn_ClearAlarm.Size = new System.Drawing.Size(237, 54);
            this.btn_ClearAlarm.TabIndex = 3;
            this.btn_ClearAlarm.Text = "Clear Alarm";
            this.circularMotionToolTip.SetToolTip(this.btn_ClearAlarm, "Clear All Joint Alarm");
            this.btn_ClearAlarm.UseVisualStyleBackColor = false;
            this.btn_ClearAlarm.Click += new System.EventHandler(this.btn_ClearAlarm_Click);
            // 
            // btn_Home
            // 
            this.btn_Home.BackColor = System.Drawing.Color.White;
            this.btn_Home.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_Home.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Home.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Home.ForeColor = System.Drawing.Color.Black;
            this.btn_Home.Location = new System.Drawing.Point(5, 378);
            this.btn_Home.Name = "btn_Home";
            this.btn_Home.Size = new System.Drawing.Size(237, 54);
            this.btn_Home.TabIndex = 3;
            this.btn_Home.Text = "Home";
            this.circularMotionToolTip.SetToolTip(this.btn_Home, "Send all joints to home position");
            this.btn_Home.UseVisualStyleBackColor = false;
            this.btn_Home.Click += new System.EventHandler(this.btn_Home_Click);
            // 
            // panel_Status
            // 
            this.panel_Status.BackColor = System.Drawing.Color.White;
            this.panel_Status.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Status.Controls.Add(this.label37);
            this.panel_Status.Controls.Add(this.pictureBox3);
            this.panel_Status.Controls.Add(this.btn_ClearSysMsg);
            this.panel_Status.Controls.Add(this.label20);
            this.panel_Status.Controls.Add(this.dataGridView_RobotPos);
            this.panel_Status.Controls.Add(this.label2);
            this.panel_Status.Controls.Add(this.dataGridView_MotorStatus);
            this.panel_Status.Controls.Add(this.tabControl_Motion);
            this.panel_Status.Controls.Add(this.txtBox_SysMsg);
            this.panel_Status.Location = new System.Drawing.Point(260, 33);
            this.panel_Status.Name = "panel_Status";
            this.panel_Status.Size = new System.Drawing.Size(930, 762);
            this.panel_Status.TabIndex = 1;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(3, 3);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(188, 19);
            this.label37.TabIndex = 11;
            this.label37.Text = "Robot Frame Diagram";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::IGUS_5DoF_Demo.Properties.Resources.IGUS_FRAME2;
            this.pictureBox3.Location = new System.Drawing.Point(219, 15);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(188, 238);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 10;
            this.pictureBox3.TabStop = false;
            this.circularMotionToolTip.SetToolTip(this.pictureBox3, "Robot Base Frame and Tool Frame Relationship Diagram");
            // 
            // btn_ClearSysMsg
            // 
            this.btn_ClearSysMsg.BackColor = System.Drawing.Color.White;
            this.btn_ClearSysMsg.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(170)))), ((int)(((byte)(170)))));
            this.btn_ClearSysMsg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ClearSysMsg.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ClearSysMsg.ForeColor = System.Drawing.Color.Black;
            this.btn_ClearSysMsg.Location = new System.Drawing.Point(841, 730);
            this.btn_ClearSysMsg.Name = "btn_ClearSysMsg";
            this.btn_ClearSysMsg.Size = new System.Drawing.Size(79, 24);
            this.btn_ClearSysMsg.TabIndex = 9;
            this.btn_ClearSysMsg.Text = "Clear";
            this.btn_ClearSysMsg.UseVisualStyleBackColor = false;
            this.btn_ClearSysMsg.Click += new System.EventHandler(this.btn_ClearSysMsg_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(14, 257);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(112, 19);
            this.label20.TabIndex = 9;
            this.label20.Text = "Status Panel";
            // 
            // dataGridView_RobotPos
            // 
            this.dataGridView_RobotPos.AllowUserToAddRows = false;
            this.dataGridView_RobotPos.AllowUserToDeleteRows = false;
            this.dataGridView_RobotPos.AllowUserToResizeColumns = false;
            this.dataGridView_RobotPos.AllowUserToResizeRows = false;
            this.dataGridView_RobotPos.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView_RobotPos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView_RobotPos.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(215)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_RobotPos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView_RobotPos.ColumnHeadersHeight = 25;
            this.dataGridView_RobotPos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView_RobotPos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Position,
            this.Column2,
            this.Angle});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_RobotPos.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView_RobotPos.EnableHeadersVisualStyles = false;
            this.dataGridView_RobotPos.Location = new System.Drawing.Point(8, 449);
            this.dataGridView_RobotPos.Name = "dataGridView_RobotPos";
            this.dataGridView_RobotPos.ReadOnly = true;
            this.dataGridView_RobotPos.RowHeadersVisible = false;
            this.dataGridView_RobotPos.RowTemplate.Height = 23;
            this.dataGridView_RobotPos.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dataGridView_RobotPos.Size = new System.Drawing.Size(597, 94);
            this.dataGridView_RobotPos.TabIndex = 4;
            this.dataGridView_RobotPos.SelectionChanged += new System.EventHandler(this.dataGridView_RobotPos_SelectionChanged);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Tool Frame Pos";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.ToolTipText = "Tool Frame Position (Origin: Base Frame) ";
            // 
            // Position
            // 
            this.Position.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Position.HeaderText = "Position(mm)";
            this.Position.Name = "Position";
            this.Position.ReadOnly = true;
            this.Position.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Position.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Position.ToolTipText = "X, Y, Z Position (mm)";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Tool Frame Angle";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column2.ToolTipText = "Tool Frame Angle (Origin: Base Frame) ";
            this.Column2.Width = 105;
            // 
            // Angle
            // 
            this.Angle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Angle.HeaderText = "Angle(°)";
            this.Angle.Name = "Angle";
            this.Angle.ReadOnly = true;
            this.Angle.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Angle.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Angle.ToolTipText = "X, Y, Z Angle (°)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 557);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 15);
            this.label2.TabIndex = 7;
            this.label2.Text = "System Message";
            // 
            // dataGridView_MotorStatus
            // 
            this.dataGridView_MotorStatus.AllowUserToAddRows = false;
            this.dataGridView_MotorStatus.AllowUserToDeleteRows = false;
            this.dataGridView_MotorStatus.AllowUserToResizeColumns = false;
            this.dataGridView_MotorStatus.AllowUserToResizeRows = false;
            this.dataGridView_MotorStatus.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView_MotorStatus.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView_MotorStatus.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(215)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_MotorStatus.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView_MotorStatus.ColumnHeadersHeight = 25;
            this.dataGridView_MotorStatus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dataGridView_MotorStatus.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Axis,
            this.Op,
            this.PosCmd,
            this.ActualPos,
            this.AngleAxis});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_MotorStatus.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView_MotorStatus.EnableHeadersVisualStyles = false;
            this.dataGridView_MotorStatus.Location = new System.Drawing.Point(8, 290);
            this.dataGridView_MotorStatus.MultiSelect = false;
            this.dataGridView_MotorStatus.Name = "dataGridView_MotorStatus";
            this.dataGridView_MotorStatus.ReadOnly = true;
            this.dataGridView_MotorStatus.RowHeadersVisible = false;
            this.dataGridView_MotorStatus.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView_MotorStatus.RowTemplate.Height = 23;
            this.dataGridView_MotorStatus.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dataGridView_MotorStatus.Size = new System.Drawing.Size(597, 141);
            this.dataGridView_MotorStatus.TabIndex = 3;
            this.dataGridView_MotorStatus.SelectionChanged += new System.EventHandler(this.dataGridView_MotorStatus_SelectionChanged);
            // 
            // Axis
            // 
            this.Axis.FillWeight = 101.5228F;
            this.Axis.HeaderText = "Axis";
            this.Axis.Name = "Axis";
            this.Axis.ReadOnly = true;
            this.Axis.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Axis.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Axis.ToolTipText = "Axis Number";
            this.Axis.Width = 50;
            // 
            // Op
            // 
            this.Op.FillWeight = 99.49239F;
            this.Op.HeaderText = "Op";
            this.Op.Name = "Op";
            this.Op.ReadOnly = true;
            this.Op.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Op.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Op.ToolTipText = "Axis Operation State";
            this.Op.Width = 80;
            // 
            // PosCmd
            // 
            this.PosCmd.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PosCmd.FillWeight = 99.49239F;
            this.PosCmd.HeaderText = "PosCmd (Pulse)";
            this.PosCmd.Name = "PosCmd";
            this.PosCmd.ReadOnly = true;
            this.PosCmd.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.PosCmd.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PosCmd.ToolTipText = "Axis Position Command";
            // 
            // ActualPos
            // 
            this.ActualPos.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ActualPos.FillWeight = 99.49239F;
            this.ActualPos.HeaderText = "ActualPos (Pulse)";
            this.ActualPos.Name = "ActualPos";
            this.ActualPos.ReadOnly = true;
            this.ActualPos.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.ActualPos.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ActualPos.ToolTipText = "Axis Actual Position";
            // 
            // AngleAxis
            // 
            this.AngleAxis.HeaderText = "Angle(°)";
            this.AngleAxis.Name = "AngleAxis";
            this.AngleAxis.ReadOnly = true;
            this.AngleAxis.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.AngleAxis.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.AngleAxis.ToolTipText = "Axis Angle";
            this.AngleAxis.Width = 60;
            // 
            // panel_TitleBar
            // 
            this.panel_TitleBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(184)))), ((int)(((byte)(186)))));
            this.panel_TitleBar.Controls.Add(this.picBox_Close);
            this.panel_TitleBar.Controls.Add(this.label23);
            this.panel_TitleBar.Location = new System.Drawing.Point(0, 0);
            this.panel_TitleBar.Name = "panel_TitleBar";
            this.panel_TitleBar.Size = new System.Drawing.Size(1200, 28);
            this.panel_TitleBar.TabIndex = 8;
            this.panel_TitleBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel_TitleBar_MouseDown);
            this.panel_TitleBar.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel_TitleBar_MouseMove);
            this.panel_TitleBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel_TitleBar_MouseUp);
            // 
            // picBox_Close
            // 
            this.picBox_Close.Image = global::IGUS_5DoF_Demo.Properties.Resources.X_noMouse;
            this.picBox_Close.Location = new System.Drawing.Point(1173, 2);
            this.picBox_Close.Name = "picBox_Close";
            this.picBox_Close.Size = new System.Drawing.Size(25, 25);
            this.picBox_Close.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox_Close.TabIndex = 2;
            this.picBox_Close.TabStop = false;
            this.picBox_Close.Click += new System.EventHandler(this.picBox_Close_Click);
            this.picBox_Close.MouseEnter += new System.EventHandler(this.picBox_Close_MouseEnter);
            this.picBox_Close.MouseLeave += new System.EventHandler(this.picBox_Close_MouseLeave);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(4, 7);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(136, 14);
            this.label23.TabIndex = 1;
            this.label23.Text = "IGUS 5DoF Controller";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // circularMotionToolTip
            // 
            this.circularMotionToolTip.Tag = "";
            // 
            // btn_MotorsOff
            // 
            this.btn_MotorsOff.BackColor = System.Drawing.Color.Silver;
            this.btn_MotorsOff.BackgroundImage = global::IGUS_5DoF_Demo.Properties.Resources.btn_moter_off_n;
            this.btn_MotorsOff.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_MotorsOff.FlatAppearance.BorderSize = 0;
            this.btn_MotorsOff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_MotorsOff.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_MotorsOff.ForeColor = System.Drawing.Color.White;
            this.btn_MotorsOff.Location = new System.Drawing.Point(129, 237);
            this.btn_MotorsOff.Name = "btn_MotorsOff";
            this.btn_MotorsOff.Size = new System.Drawing.Size(114, 74);
            this.btn_MotorsOff.TabIndex = 3;
            this.circularMotionToolTip.SetToolTip(this.btn_MotorsOff, "All Motors Off");
            this.btn_MotorsOff.UseVisualStyleBackColor = false;
            this.btn_MotorsOff.Click += new System.EventHandler(this.btn_MotorsOff_Click);
            this.btn_MotorsOff.MouseEnter += new System.EventHandler(this.btn_MotorsOff_MouseEnter);
            this.btn_MotorsOff.MouseLeave += new System.EventHandler(this.btn_MotorsOff_MouseLeave);
            // 
            // btn_MotorOn
            // 
            this.btn_MotorOn.BackColor = System.Drawing.Color.Silver;
            this.btn_MotorOn.BackgroundImage = global::IGUS_5DoF_Demo.Properties.Resources.btn_moter_on_n;
            this.btn_MotorOn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_MotorOn.FlatAppearance.BorderSize = 0;
            this.btn_MotorOn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_MotorOn.Font = new System.Drawing.Font("Segoe UI Symbol", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_MotorOn.ForeColor = System.Drawing.Color.White;
            this.btn_MotorOn.Location = new System.Drawing.Point(5, 237);
            this.btn_MotorOn.Name = "btn_MotorOn";
            this.btn_MotorOn.Size = new System.Drawing.Size(114, 74);
            this.btn_MotorOn.TabIndex = 3;
            this.circularMotionToolTip.SetToolTip(this.btn_MotorOn, "All Motors On");
            this.btn_MotorOn.UseVisualStyleBackColor = false;
            this.btn_MotorOn.Click += new System.EventHandler(this.btn_MotorOn_Click);
            this.btn_MotorOn.MouseEnter += new System.EventHandler(this.btn_MotorOn_MouseEnter);
            this.btn_MotorOn.MouseLeave += new System.EventHandler(this.btn_MotorOn_MouseLeave);
            // 
            // btn_StopComm
            // 
            this.btn_StopComm.BackColor = System.Drawing.Color.Silver;
            this.btn_StopComm.BackgroundImage = global::IGUS_5DoF_Demo.Properties.Resources.btn_stop_comm_n;
            this.btn_StopComm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_StopComm.FlatAppearance.BorderSize = 0;
            this.btn_StopComm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_StopComm.Font = new System.Drawing.Font("Segoe UI Symbol", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_StopComm.ForeColor = System.Drawing.Color.White;
            this.btn_StopComm.Location = new System.Drawing.Point(128, 101);
            this.btn_StopComm.Name = "btn_StopComm";
            this.btn_StopComm.Size = new System.Drawing.Size(114, 74);
            this.btn_StopComm.TabIndex = 3;
            this.circularMotionToolTip.SetToolTip(this.btn_StopComm, "Stop Communication");
            this.btn_StopComm.UseVisualStyleBackColor = false;
            this.btn_StopComm.Click += new System.EventHandler(this.btn_StopComm_Click);
            this.btn_StopComm.MouseEnter += new System.EventHandler(this.btn_StopComm_MouseEnter);
            this.btn_StopComm.MouseLeave += new System.EventHandler(this.btn_StopComm_MouseLeave);
            // 
            // btn_StartComm
            // 
            this.btn_StartComm.BackColor = System.Drawing.Color.White;
            this.btn_StartComm.BackgroundImage = global::IGUS_5DoF_Demo.Properties.Resources.btn_start_comm_n;
            this.btn_StartComm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_StartComm.FlatAppearance.BorderSize = 0;
            this.btn_StartComm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_StartComm.Font = new System.Drawing.Font("Segoe UI Symbol", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_StartComm.ForeColor = System.Drawing.Color.White;
            this.btn_StartComm.Location = new System.Drawing.Point(5, 101);
            this.btn_StartComm.Name = "btn_StartComm";
            this.btn_StartComm.Size = new System.Drawing.Size(114, 74);
            this.btn_StartComm.TabIndex = 3;
            this.circularMotionToolTip.SetToolTip(this.btn_StartComm, "Start Communication");
            this.btn_StartComm.UseVisualStyleBackColor = false;
            this.btn_StartComm.Click += new System.EventHandler(this.btn_StartComm_Click);
            this.btn_StartComm.MouseEnter += new System.EventHandler(this.btn_StartComm_MouseEnter);
            this.btn_StartComm.MouseLeave += new System.EventHandler(this.btn_StartComm_MouseLeave);
            // 
            // panel_Control
            // 
            this.panel_Control.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.panel_Control.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panel_Control.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Control.Controls.Add(this.pictureBox1);
            this.panel_Control.Controls.Add(this.panel_IndHome);
            this.panel_Control.Controls.Add(this.panel_IndCom);
            this.panel_Control.Controls.Add(this.label19);
            this.panel_Control.Controls.Add(this.panel_IndMotor);
            this.panel_Control.Controls.Add(this.Btn_Stop);
            this.panel_Control.Controls.Add(this.btn_ClearAlarm);
            this.panel_Control.Controls.Add(this.btn_Home);
            this.panel_Control.Controls.Add(this.btn_MotorsOff);
            this.panel_Control.Controls.Add(this.btn_MotorOn);
            this.panel_Control.Controls.Add(this.btn_StopComm);
            this.panel_Control.Controls.Add(this.btn_StartComm);
            this.panel_Control.Location = new System.Drawing.Point(5, 33);
            this.panel_Control.Name = "panel_Control";
            this.panel_Control.Size = new System.Drawing.Size(249, 761);
            this.panel_Control.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::IGUS_5DoF_Demo.Properties.Resources.MicrosoftTeams_image__6_;
            this.pictureBox1.Location = new System.Drawing.Point(8, 684);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(223, 62);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.ClientSize = new System.Drawing.Size(1200, 800);
            this.Controls.Add(this.panel_TitleBar);
            this.Controls.Add(this.panel_Status);
            this.Controls.Add(this.panel_Control);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.Text = "Line Motion";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.tabControl_Motion.ResumeLayout(false);
            this.tabPage_JointJog.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpBox_SingleAxisHome.ResumeLayout(false);
            this.tabPage_CarteJog.ResumeLayout(false);
            this.tabPage_CarteJog.PerformLayout();
            this.tabPage_Motion.ResumeLayout(false);
            this.grpBox_MultipleShape.ResumeLayout(false);
            this.grpBox_MultipleShape.PerformLayout();
            this.grpBox_ProgrammedMotion.ResumeLayout(false);
            this.grpBox_ProgrammedMotion.PerformLayout();
            this.grpBox_P2PMotion.ResumeLayout(false);
            this.grpBox_P2PMotion.PerformLayout();
            this.panel_IndHome.ResumeLayout(false);
            this.panel_IndMotor.ResumeLayout(false);
            this.panel_IndCom.ResumeLayout(false);
            this.panel_Status.ResumeLayout(false);
            this.panel_Status.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_RobotPos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_MotorStatus)).EndInit();
            this.panel_TitleBar.ResumeLayout(false);
            this.panel_TitleBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBox_Close)).EndInit();
            this.panel_Control.ResumeLayout(false);
            this.panel_Control.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox txtBox_SysMsg;
        private System.Windows.Forms.TabControl tabControl_Motion;
        private System.Windows.Forms.TabPage tabPage_CarteJog;
        private System.Windows.Forms.TabPage tabPage_Motion;
        private System.Windows.Forms.Panel panel_Status;
        private System.Windows.Forms.Button btn_StartComm;
        private System.Windows.Forms.Button btn_Home;
        private System.Windows.Forms.Button btn_MotorOn;
        private System.Windows.Forms.DataGridView dataGridView_RobotPos;
        private System.Windows.Forms.DataGridView dataGridView_MotorStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage_JointJog;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBox_JointJogAcc;
        private System.Windows.Forms.TextBox txtBox_JointJogVel;
        private System.Windows.Forms.Button btn_Joint4_Pos;
        private System.Windows.Forms.Button btn_Joint3_Pos;
        private System.Windows.Forms.Button btn_Joint2_Pos;
        private System.Windows.Forms.Button btn_Joint1_Pos;
        private System.Windows.Forms.Button btn_Joint4_Neg;
        private System.Windows.Forms.Button btn_Joint3_Neg;
        private System.Windows.Forms.Button btn_Joint2_Neg;
        private System.Windows.Forms.Button btn_Joint1_Neg;
        private System.Windows.Forms.Button btn_Joint0_Pos;
        private System.Windows.Forms.Button btn_Joint0_Neg;
        private System.Windows.Forms.GroupBox grpBox_P2PMotion;
        private System.Windows.Forms.TextBox txtBox_P2PVel;
        private System.Windows.Forms.Button btn_StopP2P;
        private System.Windows.Forms.Button btn_P2PStart;
        private System.Windows.Forms.TextBox txtBox_Rz;
        private System.Windows.Forms.TextBox txtBox_Tz;
        private System.Windows.Forms.TextBox txtBox_Rx;
        private System.Windows.Forms.TextBox txtBox_Ry;
        private System.Windows.Forms.TextBox txtBox_Ty;
        private System.Windows.Forms.TextBox txtBox_Tx;
        private System.Windows.Forms.TextBox txtBox_P2PAcc;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_TzPos;
        private System.Windows.Forms.Button btn_TyPos;
        private System.Windows.Forms.Button btn_TzNeg;
        private System.Windows.Forms.Button btn_TyNeg;
        private System.Windows.Forms.Button btn_TxPos;
        private System.Windows.Forms.Button btn_TxNeg;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtBox_CartJogAcc;
        private System.Windows.Forms.TextBox txtBox_CartJogVel;
        private System.Windows.Forms.Button btn_ClearAlarm;
        private System.Windows.Forms.Button btn_MotorsOff;
        private System.Windows.Forms.Button btn_StopComm;
        private System.Windows.Forms.Panel panel_IndHome;
        private System.Windows.Forms.Label label_IndHome;
        private System.Windows.Forms.Panel panel_IndMotor;
        private System.Windows.Forms.Label label_IndMotor;
        private System.Windows.Forms.Panel panel_IndCom;
        private System.Windows.Forms.Label label_IndCom;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel_TitleBar;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Axis;
        private System.Windows.Forms.DataGridViewTextBoxColumn Op;
        private System.Windows.Forms.DataGridViewTextBoxColumn PosCmd;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActualPos;
        private System.Windows.Forms.DataGridViewTextBoxColumn AngleAxis;
        private System.Windows.Forms.PictureBox picBox_Close;
        private System.Windows.Forms.Button btn_LineMotion;
        private System.Windows.Forms.Button btn_Circular;
        private System.Windows.Forms.Button btn_ClearSysMsg;
        private System.Windows.Forms.Button btn_RzPos;
        private System.Windows.Forms.Button btn_RyPos;
        private System.Windows.Forms.Button btn_RzNeg;
        private System.Windows.Forms.Button btn_RyNeg;
        private System.Windows.Forms.Button btn_RxPos;
        private System.Windows.Forms.Button btn_RxNeg;
        private System.Windows.Forms.TextBox txtBox_circNum;
        private System.Windows.Forms.TextBox txtBox_angVel;
        private System.Windows.Forms.TextBox txtBox_Radius;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtBox_JogAngAcc;
        private System.Windows.Forms.TextBox txtBox_JogAngVel;
        private System.Windows.Forms.Button Btn_Stop;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TextBox txtBox_LineVel;
        private System.Windows.Forms.TextBox txtBox_SquareLength;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.GroupBox grpBox_ProgrammedMotion;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Position;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Angle;
        private System.Windows.Forms.ToolTip circularMotionToolTip;
        private System.Windows.Forms.Panel panel_Control;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button bt_DemoStart;
        private System.Windows.Forms.TextBox txtBox_DemoVelocity;
        private System.Windows.Forms.CheckBox chckBox_DemoLoop;
        private System.Windows.Forms.Button btn_DemoStop;
        private System.Windows.Forms.TextBox txtBox_bitAddr;
        private System.Windows.Forms.TextBox txtBox_byteAddr;
        private System.Windows.Forms.Button btn_LaserSwitch;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.GroupBox grpBox_MultipleShape;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox txtBox_CartJogDist;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox txtBox_CartJogAngle;
        private System.Windows.Forms.GroupBox grpBox_SingleAxisHome;
        private System.Windows.Forms.Button btn_HomeJoint4;
        private System.Windows.Forms.Button btn_HomeJoint3;
        private System.Windows.Forms.Button btn_HomeJoint2;
        private System.Windows.Forms.Button btn_HomeJoint1;
        private System.Windows.Forms.Button btn_HomeJoint0;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
    }
}

