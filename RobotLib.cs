﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace IGUS_5DoF_Demo
{
    public static class RobotLib
    {
        static kinematic_5dof_robot kinematics = new kinematic_5dof_robot(Constants.LINK_LENGTH, Constants.JOINT_DISTANCE,
                Constants.TWIST_ANGLE, Constants.JOINT_ANGLE, Constants.gearratio, Constants.thetamin, Constants.thetamax);


        public static bool demoDone = false;
        private static AXESDATA currentPos = new AXESDATA();
        private static AXESDATA prevPos = new AXESDATA();
        private static COORDINATE currentCoord = new COORDINATE();

        private static List<AXESDATA> list_Jog = new List<AXESDATA>();

        private static double REC_initialX = 0;
        private static double REC_initialY = 0;
        private static double REC_initialZ = 0;
        private static double REC_initialRx = 0;
        private static double REC_initialRy = 0;
        private static double REC_initialRz = 0;

        private static object lockobject = new object();

        public static double ConvertPulseToDegrees(double pulse, int axis)
        {
            double degrees = 0;
            switch (axis)
            {
                case 0:
                    degrees = (pulse / (Constants.gearratio.Axis0 / (Math.PI * 2))) * kinematic_5dof_robot.RAD_TO_DEG;
                    break;
                case 1:
                    degrees = (pulse / (Constants.gearratio.Axis1 / (Math.PI * 2))) * kinematic_5dof_robot.RAD_TO_DEG;
                    break;
                case 2:
                    degrees = (pulse / (Constants.gearratio.Axis2 / (Math.PI * 2))) * kinematic_5dof_robot.RAD_TO_DEG;
                    break;
                case 3:
                    degrees = (pulse / (Constants.gearratio.Axis3 / (Math.PI * 2))) * kinematic_5dof_robot.RAD_TO_DEG;
                    break;
                case 4:
                    degrees = (pulse / (Constants.gearratio.Axis4 / (Math.PI * 2))) * kinematic_5dof_robot.RAD_TO_DEG;
                    break;
            }

            return degrees;
        }


        public static COORDINATE ForwardKin(AXESDATA data)
        {
            return kinematics.Forward_kine(data);
        }

        public static AXESDATA ConvertPulsetoRad(AXESDATA data)
        {
            return kinematics.ConvertPulseToRadian(data);
        }

        public static int P2P(COORDINATE finalCoord, double vel, double acc)
        {
            //check input param
            bool error = GetCurrentPos_ForwardKin();
            if (!error)
            {
                return (int)Constants.ErrorNum.ForwardKineFail;
            }

            prevPos = kinematics.ConvertPulseToRadian(currentPos);

            int errorNum;

            errorNum = ConvertCoordinateToPulse(finalCoord.x, finalCoord.y, finalCoord.z, finalCoord.roll, finalCoord.pitch, finalCoord.yaw);
            if (errorNum != 0)
            {
                return errorNum;
            }

            return Wmx3Lib.runIntpl(currentPos, vel, acc);
        }

        private static bool GetCurrentPos_ForwardKin()
        {
            //AXESDATA data = new AXESDATA();

            //Global.rwStatusLock.WaitOne();
            currentPos.Axis0 = Wmx3Lib.coreMotionStatus.AxesStatus[Constants.Axis0].ActualPos;
            currentPos.Axis1 = Wmx3Lib.coreMotionStatus.AxesStatus[Constants.Axis1].ActualPos;
            currentPos.Axis2 = Wmx3Lib.coreMotionStatus.AxesStatus[Constants.Axis2].ActualPos;
            currentPos.Axis3 = Wmx3Lib.coreMotionStatus.AxesStatus[Constants.Axis3].ActualPos;
            currentPos.Axis4 = Wmx3Lib.coreMotionStatus.AxesStatus[Constants.Axis4].ActualPos;

            currentPos = kinematics.ConvertPulseToRadian(currentPos);

            COORDINATE coord = kinematics.Forward_kine(currentPos);
            if (coord != null)
            {
                currentCoord.x = coord.x;
                currentCoord.y = coord.y;
                currentCoord.z = coord.z;
                currentCoord.roll = coord.roll;
                currentCoord.pitch = coord.pitch;
                currentCoord.yaw = coord.yaw;

                return true;
            }
            return false;
        }

        private static int ConvertCoordinateToPulse(double x, double y, double z, double roll, double pitch, double yaw)
        {
            // Inverse Kinematic
            if (TryPosInverseKine(x, y, z, roll, pitch, yaw) == false)
            {
                return (int)Constants.ErrorNum.InverseKineFail;
            }

            if (kinematics.CheckAxisInRangePulse(currentPos) == false)
            {
                return (int)Constants.ErrorNum.AngleOutOfRange;
            }

            //prevPos = currentPos;

            return 0;
        }

        private static bool TryPosInverseKine(double i_x, double i_y, double i_z, double i_roll, double i_pitch, double i_yaw)
        {
            COORDINATE coord = new COORDINATE() { x = i_x, y = i_y, z = i_z, roll = i_roll, pitch = i_pitch, yaw = i_yaw };

            List<AXESDATA> lstSolution = kinematics.Inverse_kine(coord); //num_in: number of inverse kinematic solutions

            //display_infor(lstSolution.Count.ToString());

            if (lstSolution.Count == 1)
            {
                currentPos = kinematics.ConvertRadianToPulse(lstSolution[0]);
                return true;
            }
            else if (lstSolution.Count >= 2)
            {
                double temp1 = 0;
                double temp2 = 0;

                AXESDATA data1 = kinematics.ConvertRadianToPulse(lstSolution[0]);
                temp1 += Math.Abs(data1.Axis0 - prevPos.Axis0);
                temp1 += Math.Abs(data1.Axis1 - prevPos.Axis1);
                temp1 += Math.Abs(data1.Axis2 - prevPos.Axis2);
                temp1 += Math.Abs(data1.Axis3 - prevPos.Axis3);
                temp1 += Math.Abs(data1.Axis4 - prevPos.Axis4);

                AXESDATA data2 = kinematics.ConvertRadianToPulse(lstSolution[1]);
                temp2 += Math.Abs(data2.Axis0 - prevPos.Axis0);
                temp2 += Math.Abs(data2.Axis1 - prevPos.Axis1);
                temp2 += Math.Abs(data2.Axis2 - prevPos.Axis2);
                temp2 += Math.Abs(data2.Axis3 - prevPos.Axis3);
                temp2 += Math.Abs(data2.Axis4 - prevPos.Axis4);

                currentPos = (temp1 <= temp2) ? data1 : data2;
                return true;
            }

            return false;
        }

        public static int JogMotion(double dist, double vel, double acc, int plane, int direction)
        {
            list_Jog.Clear();
            GetCurrentPos_ForwardKin();

            double initialX = currentCoord.x;
            double initialY = currentCoord.y;
            double initialZ = currentCoord.z;
            double initialRx = currentCoord.roll;
            double initialRy = currentCoord.pitch;
            double initialRz = currentCoord.yaw;

            double t_acc = vel / acc;
            double t_final = t_acc + dist / vel;
            double t_mid = t_final / 2;

            double s_max = 1 / (t_final - t_acc);
            double interval = 0.001;
            double s = 0;

            

            if (t_acc >= t_mid)
                return (int)Constants.ErrorNum.NoConstantVelocityPeriod;

            int count = 0;

            for (double t = interval; t <= t_final; t = t + interval)
            {
                if (t <= t_acc)
                {
                    s = ((s_max / 2) / t_acc) * t * t;
                }
                else if (t > t_acc && t <= (t_final - t_acc))
                {
                    s = s_max * (t - t_acc / 2);
                }
                else if (t > (t_final - t_acc))
                {
                    s = 1 - ((s_max / 2) / t_acc) * (t_final - t) * (t_final - t);
                }

                int error = 0;
                switch (plane)
                {
                    case 0:
                        error = ConvertCoordinateToPulse(initialX + dist * s * direction, initialY, initialZ, initialRx, initialRy, initialRz);
                        break;
                    case 1:
                        error = ConvertCoordinateToPulse(initialX, initialY + dist * s * direction, initialZ, initialRx, initialRy, initialRz);
                        break;
                    case 2:
                        error = ConvertCoordinateToPulse(initialX, initialY, initialZ + dist * s * direction, initialRx, initialRy, initialRz);
                        break;
                }

                if (error != 0)
                {
                    return error;
                }
                else
                {
                    if(list_Jog.Count > 0)
                    {
                        int errorCount = 0;
                        if (Math.Abs(currentPos.Axis0 - list_Jog[list_Jog.Count - 1].Axis0) > Constants.Pulse_Max_Diff)
                            errorCount++;
                        if (Math.Abs(currentPos.Axis1 - list_Jog[list_Jog.Count - 1].Axis1) > Constants.Pulse_Max_Diff)
                            errorCount++;
                        if (Math.Abs(currentPos.Axis2 - list_Jog[list_Jog.Count - 1].Axis2) > Constants.Pulse_Max_Diff)
                            errorCount++;
                        if (Math.Abs(currentPos.Axis3 - list_Jog[list_Jog.Count - 1].Axis3) > Constants.Pulse_Max_Diff)
                            errorCount++;
                        if (Math.Abs(currentPos.Axis4 - list_Jog[list_Jog.Count - 1].Axis4) > Constants.Pulse_Max_Diff)
                            errorCount++;

                        if (errorCount > 0)
                        {
                            list_Jog.Clear();
                            return (int)Constants.ErrorNum.ExceedPulseDiffLimit;
                        }
                    }                   
                }

                list_Jog.Add(currentPos);

            }
            Wmx3Lib.CyclicMotion(list_Jog.Count, list_Jog);
            list_Jog.Clear();
            return 0;
        }

        //public static int JogMotion(double vel, double acc, int plane, int direction)
        //{
        //    list_Jog.Clear();
        //    if (!GetCurrentPos_ForwardKin())
        //    {
        //        return (int)Constants.ErrorNum.ForwardKineFail;
        //    }

        //    double initialX = currentCoord.x;
        //    double initialY = currentCoord.y;
        //    double initialZ = currentCoord.z;
        //    double initialRx = currentCoord.roll;
        //    double initialRy = currentCoord.pitch;
        //    double initialRz = currentCoord.yaw;

        //    int solutionExists = 0;

        //    double t_acc = 0;
        //    double t_vel = 0;
        //    double s_acc = 0;
        //    double s_vel = 0;
        //    double s_fin = 0;

        //    double accTime = vel / acc;

        //    while (solutionExists == 0)
        //    {
        //        if (list_Jog.Count > 35000)
        //        {
        //            list_Jog.Clear();
        //            return (int)Constants.ErrorNum.VelocityAccTooSlow;
        //        }

        //        if (t_acc < accTime)
        //        {
        //            t_acc += 0.001;
        //            // displacement(acc) = ut + 0.5 * a * t^2
        //            s_acc = (0.5 * acc * (t_acc * t_acc));
        //            s_fin = s_acc;
        //        }
        //        else
        //        {
        //            t_vel += 0.001;
        //            // displacement (constant velocity) = vel * t + displacement(acc)
        //            s_vel = vel * t_vel + s_acc;
        //            s_fin = s_vel;
        //        }

        //        switch (plane)
        //        {
        //            case 0:
        //                solutionExists = ConvertCoordinateToPulse(s_fin * direction + initialX, initialY, initialZ, initialRx, initialRy, initialRz);
        //                break;
        //            case 1:
        //                solutionExists = ConvertCoordinateToPulse(initialX, s_fin * direction + initialY, initialZ, initialRx, initialRy, initialRz);
        //                break;
        //            case 2:
        //                solutionExists = ConvertCoordinateToPulse(initialX, initialY, s_fin * direction + initialZ, initialRx, initialRy, initialRz);
        //                break;
        //        }

        //        if (solutionExists == 0)
        //        {
        //            if (list_Jog.Count > 0)
        //            {
        //                int errorCount = 0;
        //                if (Math.Abs(currentPos.Axis0 - list_Jog[list_Jog.Count - 1].Axis0) > Constants.Pulse_Max_Diff)
        //                    errorCount++;
        //                if (Math.Abs(currentPos.Axis1 - list_Jog[list_Jog.Count - 1].Axis1) > Constants.Pulse_Max_Diff)
        //                    errorCount++;
        //                if (Math.Abs(currentPos.Axis2 - list_Jog[list_Jog.Count - 1].Axis2) > Constants.Pulse_Max_Diff)
        //                    errorCount++;
        //                if (Math.Abs(currentPos.Axis3 - list_Jog[list_Jog.Count - 1].Axis3) > Constants.Pulse_Max_Diff)
        //                    errorCount++;
        //                if (Math.Abs(currentPos.Axis4 - list_Jog[list_Jog.Count - 1].Axis4) > Constants.Pulse_Max_Diff)
        //                    errorCount++;

        //                if (errorCount > 0)
        //                {
        //                    list_Jog.Clear();
        //                    return (int)Constants.ErrorNum.ExceedPulseDiffLimit;
        //                }
        //            }

        //            list_Jog.Add(currentPos);
        //            prevPos = currentPos;
        //        }
        //        else
        //        {
        //            //Modify cyclic buffer for deccelertion

        //            //if (list_Jog.Count < 50)
        //            //    return (int)Constants.ErrorNum.MovingDistanceShort;
        //            double decDist = -(vel * vel) / (2 * -acc);

        //            //s_fin = s_fin - (vel * (0.001));

        //            double timeTaken = decDist / vel;
        //            double cycles = timeTaken / 0.001;

        //            if (list_Jog.Count <= cycles)
        //            {
        //                list_Jog.Clear();
        //                return (int)Constants.ErrorNum.InverseKineFail;
        //            }


        //            list_Jog.RemoveRange(list_Jog.Count - (int)Math.Round(cycles - 1), (int)Math.Round(cycles - 1));

        //            double t_dec = 0.001;
        //            double s_Dec = 0;
        //            double s_b4Dec = s_fin - decDist;

        //            bool done = false;
        //            while (!done && s_Dec <= s_fin)
        //            {
        //                s_Dec = s_b4Dec + vel * t_dec + (0.5 * -acc * t_dec * t_dec);

        //                int error = 0;

        //                switch (plane)
        //                {
        //                    case 0:
        //                        error = ConvertCoordinateToPulse(s_Dec * direction + initialX, initialY, initialZ, initialRx, initialRy, initialRz);
        //                        break;
        //                    case 1:
        //                        error = ConvertCoordinateToPulse(initialX, s_Dec * direction + initialY, initialZ, initialRx, initialRy, initialRz);
        //                        break;
        //                    case 2:
        //                        error = ConvertCoordinateToPulse(initialX, initialY, s_Dec * direction + initialZ, initialRx, initialRy, initialRz);
        //                        break;
        //                }

        //                if (error != 0)
        //                    break;
        //                else
        //                {
        //                    int errorCount = 0;
        //                    if (Math.Abs(currentPos.Axis0 - list_Jog[list_Jog.Count - 1].Axis0) > Constants.Pulse_Max_Diff)
        //                        errorCount++;
        //                    if (Math.Abs(currentPos.Axis1 - list_Jog[list_Jog.Count - 1].Axis1) > Constants.Pulse_Max_Diff)
        //                        errorCount++;
        //                    if (Math.Abs(currentPos.Axis2 - list_Jog[list_Jog.Count - 1].Axis2) > Constants.Pulse_Max_Diff)
        //                        errorCount++;
        //                    if (Math.Abs(currentPos.Axis3 - list_Jog[list_Jog.Count - 1].Axis3) > Constants.Pulse_Max_Diff)
        //                        errorCount++;
        //                    if (Math.Abs(currentPos.Axis4 - list_Jog[list_Jog.Count - 1].Axis4) > Constants.Pulse_Max_Diff)
        //                        errorCount++;

        //                    if (errorCount > 0)
        //                    {
        //                        list_Jog.Clear();
        //                        return (int)Constants.ErrorNum.ExceedPulseDiffLimit;
        //                    }

        //                    list_Jog.Add(currentPos);
        //                    t_dec += 0.001;
        //                    t_dec = Math.Round(t_dec, 3);
        //                }
        //            }
        //        }
        //    }

        //    Task.Factory.StartNew(() => Wmx3Lib.CyclicMotion(list_Jog.Count, list_Jog));
        //    return 0;
        //}

        public static int JogMotionAngular(double angle, double vel, double acc, int plane, int direction)
        {
            list_Jog.Clear();
            GetCurrentPos_ForwardKin();

            double initialX = currentCoord.x;
            double initialY = currentCoord.y;
            double initialZ = currentCoord.z;
            double initialRx = currentCoord.roll;
            double initialRy = currentCoord.pitch;
            double initialRz = currentCoord.yaw;

            //double t_acc = 0;
            double t_vel = 0;
            double theta_acc = 0;
            double theta_vel = 0;
            double theta_fin = 0;

            double w = vel;
            double a = acc;

            double accTime = w / a;

            //double t_acc = vel / acc;
            double t_final = accTime + angle / w;
            double t_mid = t_final / 2;

            double s_max = 1 / (t_final - accTime);
            double interval = 0.001;
            double s = 0;



            if (accTime >= t_mid)
                return (int)Constants.ErrorNum.NoConstantVelocityPeriod;

            int count = 0;

            for (double t = interval; t <= t_final; t = t + interval)
            {
                if (t <= accTime)
                {
                    s = ((s_max / 2) / accTime) * t * t;
                }
                else if (t > accTime && t <= (t_final - accTime))
                {
                    s = s_max * (t - accTime / 2);
                }
                else if (t > (t_final - accTime))
                {
                    s = 1 - ((s_max / 2) / accTime) * (t_final - t) * (t_final - t);
                }

                int error = 0;
                switch (plane)
                {
                    case 0:
                        error = ConvertCoordinateToPulse(initialX , initialY, initialZ, initialRx + angle * s * direction, initialRy, initialRz);
                        break;
                    case 1:
                        error = ConvertCoordinateToPulse(initialX, initialY, initialZ, initialRx, initialRy + angle * s * direction, initialRz);
                        break;
                    case 2:
                        error = ConvertCoordinateToPulse(initialX, initialY, initialZ , initialRx, initialRy, initialRz + angle * s * direction);
                        break;
                }

                if (error != 0)
                {
                    return error;
                }
                else
                {
                    if (list_Jog.Count > 0)
                    {
                        int errorCount = 0;
                        if (Math.Abs(currentPos.Axis0 - list_Jog[list_Jog.Count - 1].Axis0) > Constants.Pulse_Max_Diff)
                            errorCount++;
                        if (Math.Abs(currentPos.Axis1 - list_Jog[list_Jog.Count - 1].Axis1) > Constants.Pulse_Max_Diff)
                            errorCount++;
                        if (Math.Abs(currentPos.Axis2 - list_Jog[list_Jog.Count - 1].Axis2) > Constants.Pulse_Max_Diff)
                            errorCount++;
                        if (Math.Abs(currentPos.Axis3 - list_Jog[list_Jog.Count - 1].Axis3) > Constants.Pulse_Max_Diff)
                            errorCount++;
                        if (Math.Abs(currentPos.Axis4 - list_Jog[list_Jog.Count - 1].Axis4) > Constants.Pulse_Max_Diff)
                            errorCount++;

                        if (errorCount > 0)
                        {
                            list_Jog.Clear();
                            return (int)Constants.ErrorNum.ExceedPulseDiffLimit;
                        }
                    }
                }

                list_Jog.Add(currentPos);

            }
            Wmx3Lib.CyclicMotion(list_Jog.Count, list_Jog);
            list_Jog.Clear();
            return 0;
        }

        //public static int JogMotionAngular(double vel, double acc, int plane, int direction)
        //{
        //    list_Jog.Clear();
        //    if (!GetCurrentPos_ForwardKin())
        //    {
        //        return (int)Constants.ErrorNum.ForwardKineFail;
        //    }

        //    double initialX = currentCoord.x;
        //    double initialY = currentCoord.y;
        //    double initialZ = currentCoord.z;
        //    double initialRx = currentCoord.roll;
        //    double initialRy = currentCoord.pitch;
        //    double initialRz = currentCoord.yaw;

        //    int kinematicsError = 0;

        //    double t_acc = 0;
        //    double t_vel = 0;
        //    double theta_acc = 0;
        //    double theta_vel = 0;
        //    double theta_fin = 0;

        //    double w = vel;
        //    double a = acc;

        //    double accTime = w / a;

        //    while (kinematicsError == 0)
        //    {
        //        if (t_acc < accTime)
        //        {
        //            t_acc += 0.001;
        //            // displacement(acc) = ut + 0.5 * a * t^2
        //            theta_acc = (0.5 * a * (t_acc * t_acc));
        //            theta_fin = theta_acc;
        //        }
        //        else
        //        {
        //            t_vel += 0.001;
        //            // displacement (constant velocity) = vel * t + displacement(acc)
        //            theta_vel = w * t_vel + theta_acc;
        //            theta_fin = theta_vel;
        //        }

        //        switch (plane)
        //        {
        //            case 0:
        //                kinematicsError = ConvertCoordinateToPulse(initialX, initialY, initialZ, initialRx + theta_fin * direction, initialRy, initialRz);
        //                break;
        //            case 1:
        //                kinematicsError = ConvertCoordinateToPulse(initialX, initialY, initialZ, initialRx, initialRy + theta_fin * direction, initialRz);
        //                break;
        //            case 2:
        //                kinematicsError = ConvertCoordinateToPulse(initialX, initialY, initialZ, initialRx, initialRy, initialRz + theta_fin * direction);
        //                break;
        //        }

        //        if (kinematicsError == 0)
        //        {
        //            if (list_Jog.Count > 0)
        //            {
        //                int errorCount = 0;
        //                if (Math.Abs(currentPos.Axis0 - list_Jog[list_Jog.Count - 1].Axis0) > Constants.Pulse_Max_Diff)
        //                    errorCount++;
        //                if (Math.Abs(currentPos.Axis1 - list_Jog[list_Jog.Count - 1].Axis1) > Constants.Pulse_Max_Diff)
        //                    errorCount++;
        //                if (Math.Abs(currentPos.Axis2 - list_Jog[list_Jog.Count - 1].Axis2) > Constants.Pulse_Max_Diff)
        //                    errorCount++;
        //                if (Math.Abs(currentPos.Axis3 - list_Jog[list_Jog.Count - 1].Axis3) > Constants.Pulse_Max_Diff)
        //                    errorCount++;
        //                if (Math.Abs(currentPos.Axis4 - list_Jog[list_Jog.Count - 1].Axis4) > Constants.Pulse_Max_Diff)
        //                    errorCount++;

        //                if (errorCount > 0)
        //                {
        //                    list_Jog.Clear();
        //                    return (int)Constants.ErrorNum.ExceedPulseDiffLimit;
        //                }
        //            }
        //            list_Jog.Add(currentPos);
        //            prevPos = currentPos;
        //        }
        //        else
        //        {
        //            //if (theta_vel == 0)
        //            //    return (int)Constants.ErrorNum.NoConstantVelocityPeriod;

        //            double decTheta = -(w * w) / (2 * -a);
        //            double timeTaken = decTheta / a;
        //            double cycles = timeTaken / 0.001;

        //            if (list_Jog.Count <= cycles)
        //            {
        //                list_Jog.Clear();
        //                return (int)Constants.ErrorNum.InverseKineFail;
        //            }

        //            list_Jog.RemoveRange(list_Jog.Count - (int)Math.Round(cycles - 1), (int)Math.Round(cycles - 1));

        //            double t_dec = 0.001;
        //            double theta_dec = 0;
        //            double theta_b4Dec = theta_fin - decTheta;

        //            bool done = false;
        //            while (!done && theta_dec <= theta_fin)
        //            {
        //                theta_dec = theta_b4Dec + vel * t_dec + (0.5 * -acc * t_dec * t_dec);

        //                int error = 0;

        //                switch (plane)
        //                {
        //                    case 0:
        //                        error = ConvertCoordinateToPulse(initialX, initialY, initialZ, initialRx + theta_dec * direction, initialRy, initialRz);
        //                        break;
        //                    case 1:
        //                        error = ConvertCoordinateToPulse(initialX, initialY, initialZ, initialRx, initialRy + theta_dec * direction, initialRz);
        //                        break;
        //                    case 2:
        //                        error = ConvertCoordinateToPulse(initialX, initialY, initialZ, initialRx, initialRy, initialRz + theta_dec * direction);
        //                        break;
        //                }

        //                if (error != 0)
        //                    break;
        //                else
        //                {
        //                    int errorCount = 0;
        //                    if (Math.Abs(currentPos.Axis0 - list_Jog[list_Jog.Count - 1].Axis0) > Constants.Pulse_Max_Diff)
        //                        errorCount++;
        //                    if (Math.Abs(currentPos.Axis1 - list_Jog[list_Jog.Count - 1].Axis1) > Constants.Pulse_Max_Diff)
        //                        errorCount++;
        //                    if (Math.Abs(currentPos.Axis2 - list_Jog[list_Jog.Count - 1].Axis2) > Constants.Pulse_Max_Diff)
        //                        errorCount++;
        //                    if (Math.Abs(currentPos.Axis3 - list_Jog[list_Jog.Count - 1].Axis3) > Constants.Pulse_Max_Diff)
        //                        errorCount++;
        //                    if (Math.Abs(currentPos.Axis4 - list_Jog[list_Jog.Count - 1].Axis4) > Constants.Pulse_Max_Diff)
        //                        errorCount++;

        //                    if (errorCount > 0)
        //                    {
        //                        list_Jog.Clear();
        //                        return (int)Constants.ErrorNum.ExceedPulseDiffLimit;
        //                    }

        //                    list_Jog.Add(currentPos);
        //                    t_dec += 0.001;
        //                }
        //            }
        //        }
        //    }
        //    Task.Factory.StartNew(() => Wmx3Lib.CyclicMotion(list_Jog.Count, list_Jog));
        //    return 0;
        //}

        public static int CircularMotion(double vel, int num_circle, double radius, bool multishapeDemo = false)
        {
            list_Jog.Clear();
            GetCurrentPos_ForwardKin();
            double initialX = currentCoord.x;
            double initialY = currentCoord.y;
            double initialZ = currentCoord.z;
            double initialRx = currentCoord.roll;
            double initialRy = currentCoord.pitch;
            double initialRz = currentCoord.yaw;

            double acc = 0;

            double x_cen = initialX + radius;
            double y_cen = initialY;

            //double xdiff = x_cen - initialX;
            //double ydiff = y_cen - initialY;
            //double radius = Math.Sqrt(Math.Pow(xdiff, 2) + Math.Pow(ydiff, 2));

            double s_x = 0;
            double s_y = 0;
            double theta_acc = 0;
            double theta_fin = 0;
            double theta_dec = 0;
            double theta_vel = 0;

            double theta_0 = Math.PI;
            double theta_f = -Math.PI - 2 * Math.PI * (num_circle - 1);
            double t_final = Math.Abs(theta_f - theta_0) / vel;
            double tm = t_final / 2;

            double theta_f_temp = -Math.PI;
            double t_final_temp = Math.Abs(theta_f_temp - theta_0) / vel;
            double tm_temp = t_final_temp / 2;
            double w_max;

            if (vel > 2)
            {
                w_max = vel * 1.25;
            }
            else
            {
                w_max = vel * 1.1;
            }

            double accTime = t_final_temp - Math.Abs(theta_f_temp - theta_0) / w_max;
            acc = w_max / accTime;

            if (num_circle > 1)
            {
                acc = Math.Abs(theta_f - theta_0) / (t_final * accTime - accTime * accTime);
                w_max = acc * accTime;
            }

            double interval = 0.001;
            for (double t = interval; t <= t_final; t = t + interval)
            {

                if (t <= accTime)
                {
                    theta_acc = theta_0 - (0.5 * acc * (t * t));
                    theta_fin = theta_acc;
                }
                else if (t > accTime && t <= t_final - accTime)
                {
                    theta_vel = theta_0 - w_max * (t - accTime / 2);
                    theta_fin = theta_vel;
                }
                else if (t > (t_final - accTime))
                {
                    theta_dec = theta_f + (w_max / (2 * accTime)) * (t_final - t) * (t_final - t);
                    theta_fin = theta_dec;
                }

                s_x = radius * Math.Cos(theta_fin);
                s_y = radius * Math.Sin(theta_fin);

                if(multishapeDemo == true)
                {
                    double x = s_x + radius;
                    double offset = Math.Sqrt((((Math.Pow((x - 40), 2) - Math.Pow(40, 2)) * -0.03189)));
                    if (s_y > 0)
                        s_y = s_y - offset;
                    else
                        s_y = s_y + offset;
                }
                

                int error = ConvertCoordinateToPulse(x_cen + s_x, y_cen + s_y, currentCoord.z, currentCoord.roll, currentCoord.pitch, currentCoord.yaw);
                if (error != 0)
                {
                    list_Jog.Clear();
                    return (int)Constants.ErrorNum.CircleExceedsWrkSpace;
                }

                if (theta_fin <= Math.PI * 2)
                {
                    list_Jog.Add(currentPos);
                    prevPos = currentPos;
                }
                else
                {
                    if (list_Jog.Count == 0)
                        return -1;
                    else
                    {
                        break;
                    }
                }
            }
            Wmx3Lib.CyclicMotion(list_Jog.Count, list_Jog);
            list_Jog.Clear();
            return 0;
        }

        public static int LineMotion(double vel, double length)
        {
            list_Jog.Clear();
            GetCurrentPos_ForwardKin();

            double initialX = currentCoord.x;
            double initialY = currentCoord.y;
            double initialZ = currentCoord.z;
            double initialRx = currentCoord.roll;
            double initialRy = currentCoord.pitch;
            double initialRz = currentCoord.yaw;

            double acc = vel * 10;

            double t_acc = vel / acc;
            double t_final = t_acc + length / vel;
            double t_mid = t_final / 2;

            double s_max = 1 / (t_final - t_acc);
            double interval = 0.001;
            double s = 0;

            double x_dis = 0;
            double y_dis = 0;

            if (t_acc >= t_mid)
                return (int)Constants.ErrorNum.NoConstantVelocityPeriod;

            int count = 0;

            while (count < 4)
            {
                for (double t = interval; t <= t_final; t = t + interval)
                {
                    if (t <= t_acc)
                    {
                        s = ((s_max / 2) / t_acc) * t * t;
                    }
                    else if (t > t_acc && t <= (t_final - t_acc))
                    {
                        s = s_max * (t - t_acc / 2);
                    }
                    else if (t > (t_final - t_acc))
                    {
                        s = 1 - ((s_max / 2) / t_acc) * (t_final - t) * (t_final - t);
                    }

                    switch (count)
                    {
                        case 0:
                            x_dis = initialX + length * s;
                            y_dis = initialY;
                            break;
                        case 1:
                            y_dis = initialY + length * s;
                            break;
                        case 2:
                            x_dis = initialX - length * s;
                            break;
                        case 3:
                            y_dis = initialY - length * s;
                            break;

                    }
                    int error;
                    error = ConvertCoordinateToPulse(x_dis, y_dis, initialZ, initialRx, initialRy, initialRz);
                    if (error != 0)
                    {
                        return error;
                    }

                    list_Jog.Add(currentPos);
                }
                initialX = x_dis;
                initialY = y_dis;
                count++;
            }
            Wmx3Lib.CyclicMotion(list_Jog.Count, list_Jog);
            list_Jog.Clear();
            return 0;
        }

        private static int YLineMotion(int xdirection, int ydirection, double xLength, double yLength, double vel, bool first = false)
        {
            //list_Jog.Clear();
            double velX = 0;
            double velY = 0;
            double accX = 0;
            double accY = 0;

            double t_accXY = 0;
            double t_finalXY = 0;
            double t_midXY = 0;

            double s_maxXY = 0;

            double interval = 0.001;
            double s = 0;

            double x_dis = 0;
            double y_dis = 0;
            int delayCount = 0;
            bool delayFlag = false;

            if (first)
                delayCount = 500;


            //xLength = -120 * Math.Sin(36 * kinematic_5dof_robot.DEG_TO_RAD);
            //yLength = -120 * Math.Cos(36 * kinematic_5dof_robot.DEG_TO_RAD);
            velX = vel;
            velY = vel;
            accX = velX * 7;
            accY = velY * 7;


            //must use Y
            t_accXY = velY / accY;
            if (yLength != 0)
                t_finalXY = t_accXY + Math.Abs(yLength) / velY;
            else
                t_finalXY = t_accXY + Math.Abs(xLength) / velY;
            t_midXY = t_finalXY / 2;

            s_maxXY = 1 / (t_finalXY - t_accXY);

            if (t_accXY >= t_midXY)
                return (int)Constants.ErrorNum.NoConstantVelocityPeriod;

            for (double t = interval; t <= t_finalXY; t = t + interval)
            {
                if (t <= t_accXY)
                {
                    s = ((s_maxXY / 2) / t_accXY) * t * t;
                }
                else if (t > t_accXY && t <= (t_finalXY - t_accXY))
                {
                    s = s_maxXY * (t - t_accXY / 2);
                }
                else if (t > (t_finalXY - t_accXY))
                {
                    s = 1 - ((s_maxXY / 2) / t_accXY) * (t_finalXY - t) * (t_finalXY - t);
                }

                if(delayCount < 500)
                {
                    delayCount++;
                    delayFlag = true;
                    x_dis = REC_initialX;
                    y_dis = REC_initialY;
                    t = t - interval;
                }
                else
                {
                    delayFlag = false;
                    x_dis = REC_initialX + xLength * s * xdirection;
                    y_dis = REC_initialY + yLength * s * ydirection;
                }

                

                int error;
                if (delayFlag)
                    error = 0;
                else
                    error = ConvertCoordinateToPulse(x_dis, y_dis, REC_initialZ, REC_initialRx, REC_initialRy, REC_initialRz);

                if (error != 0)
                {
                    return error;
                }

                list_Jog.Add(currentPos);
            }

            REC_initialX = x_dis;
            REC_initialY = y_dis;

            //Wmx3Lib.CyclicMotion(list_Jog.Count, list_Jog);
            //list_Jog.Clear();
            return 0;
        }

        public static int StarMotion(double vel)
        {
            list_Jog.Clear();
            GetCurrentPos_ForwardKin();

            double initialX = currentCoord.x;
            double initialY = currentCoord.y;
            double initialZ = currentCoord.z;
            double initialRx = currentCoord.roll;
            double initialRy = currentCoord.pitch;
            double initialRz = currentCoord.yaw;

            //const double sidelength = 114.2;
            //const double sidelength = 95.1057;
            const double sidelength = 76.0845;
            double xLength = 0;
            double yLength = 0;
            int count = 0;
            int delayCount = 0;
            bool delayFlag = false;
            double velX = 0;
            double velY = 0;
            double accX = 0;
            double accY = 0;

            double t_accXY = 0;
            double t_finalXY = 0;
            double t_midXY = 0;

            double s_maxXY = 0;

            double interval = 0.001;
            double s = 0;

            double x_dis = 0;
            double y_dis = 0;

            while (count < 5)
            {
                delayCount = 0;
                switch (count)
                {
                    case 0:
                        xLength = sidelength * Math.Sin(72 * kinematic_5dof_robot.DEG_TO_RAD);
                        yLength = -sidelength * Math.Cos(72 * kinematic_5dof_robot.DEG_TO_RAD) + 5.9798;
                        velX = vel * Math.Sin(76.38 * kinematic_5dof_robot.DEG_TO_RAD); //72
                        velY = vel * Math.Cos(76.38 * kinematic_5dof_robot.DEG_TO_RAD);
                        accX = velX * 8;
                        accY = velY * 8;
                        break;
                    case 1:
                        xLength = -sidelength * Math.Sin(36 * kinematic_5dof_robot.DEG_TO_RAD);
                        //yLength = sidelength * Math.Cos(36 * kinematic_5dof_robot.DEG_TO_RAD) - 11.95775;
                        yLength = 48.5396;
                        velX = vel * Math.Cos(42.6555 * kinematic_5dof_robot.DEG_TO_RAD); //36
                        velY = vel * Math.Sin(42.6555 * kinematic_5dof_robot.DEG_TO_RAD);
                        accX = velX * 8;
                        accY = velY * 8;
                        break;
                    case 2:
                        xLength = 0;
                        //yLength = -sidelength + 11.95775;
                        yLength = -62.0165;
                        velX = 0;
                        velY = vel;
                        accX = 0;
                        accY = velY * 8;
                        break;
                    case 3:
                        xLength = sidelength * Math.Sin(36 * kinematic_5dof_robot.DEG_TO_RAD);
                        //yLength = sidelength * Math.Cos(36 * kinematic_5dof_robot.DEG_TO_RAD)- 11.95775;
                        yLength = 48.5396;
                        velX = vel * Math.Sin(42.6555 * kinematic_5dof_robot.DEG_TO_RAD);
                        velY = vel * Math.Cos(42.6555 * kinematic_5dof_robot.DEG_TO_RAD);
                        accX = velX * 8;
                        accY = velY * 8;
                        break;
                    case 4:
                        xLength = -sidelength * Math.Sin(72 * kinematic_5dof_robot.DEG_TO_RAD);
                        yLength = -sidelength * Math.Cos(72 * kinematic_5dof_robot.DEG_TO_RAD) + 5.9798;
                        velX = vel * Math.Sin(76.38 * kinematic_5dof_robot.DEG_TO_RAD);
                        velY = vel * Math.Cos(76.38 * kinematic_5dof_robot.DEG_TO_RAD);
                        accX = velX * 8;
                        accY = velY * 8;
                        break;
                }

                //must use Y
                t_accXY = velY / accY;
                t_finalXY = t_accXY + Math.Abs(yLength) / velY;
                t_midXY = t_finalXY / 2;

                s_maxXY = 1 / (t_finalXY - t_accXY);

                if (t_accXY >= t_midXY)
                    return (int)Constants.ErrorNum.NoConstantVelocityPeriod;


                for (double t = interval; t <= t_finalXY; t = t + interval)
                {
                    if (t <= t_accXY)
                    {
                        s = ((s_maxXY / 2) / t_accXY) * t * t;
                    }
                    else if (t > t_accXY && t <= (t_finalXY - t_accXY))
                    {
                        s = s_maxXY * (t - t_accXY / 2);
                    }
                    else if (t > (t_finalXY - t_accXY))
                    {
                        s = 1 - ((s_maxXY / 2) / t_accXY) * (t_finalXY - t) * (t_finalXY - t);
                    }

                    switch (count)
                    {
                        case 0:
                            delayFlag = false;
                            x_dis = initialX + xLength * s;
                            y_dis = initialY + yLength * s;
                            break;
                        case 1:
                            if (delayCount < 500)
                            {
                                delayCount++;
                                t = t - interval;
                                delayFlag = true;
                                break;
                            }
                            else
                                delayFlag = false;
                            x_dis = initialX + xLength * s;
                            y_dis = initialY + yLength * s;
                            break;
                        case 2:
                            if (delayCount < 500)
                            {
                                delayCount++;
                                t = t - interval;
                                delayFlag = true;
                                break;
                            }
                            else
                                delayFlag = false;
                            x_dis = initialX + xLength * s;
                            y_dis = initialY + yLength * s;
                            break;
                        case 3:
                            if (delayCount < 500)
                            {
                                delayCount++;
                                t = t - interval;
                                delayFlag = true;
                                break;
                            }
                            else
                                delayFlag = false;
                            x_dis = initialX + xLength * s;
                            y_dis = initialY + yLength * s;
                            break;
                        case 4:
                            if (delayCount < 500)
                            {
                                delayCount++;
                                t = t - interval;
                                delayFlag = true;
                                break;
                            }
                            else
                                delayFlag = false;
                            x_dis = initialX + xLength * s;
                            y_dis = initialY + yLength * s;
                            break;

                    }
                    int error;
                    if(delayFlag)
                    {
                        error = 0;
                    }
                    else
                        error = ConvertCoordinateToPulse(x_dis, y_dis, initialZ, initialRx, initialRy, initialRz);

                    if (error != 0)
                    {
                        return error;
                    }

                    list_Jog.Add(currentPos);
                }

                initialX = x_dis;
                initialY = y_dis;
                count++;
            }
            Wmx3Lib.CyclicMotion(list_Jog.Count, list_Jog);
            list_Jog.Clear();
            return 0;
        }

        public static void StopDemo()
        {
            demoDone = true;
            Global.demoStart = false;
        }

        public static int MultipleShapeFSM(double velOverride, bool loop)
        {
            Wmx3Lib.AbortCyclicBuffer();
            double vel = 300;
            vel *= velOverride;

            double circleVel = 6;
            circleVel *= velOverride;
            demoDone = false;
            int state = 0;
            int errorCount = 0;
            while (!demoDone)
            {
                switch(state)
                {
                    case 0:
                        COORDINATE cood = new COORDINATE();
                        cood.x = 315;
                        cood.y = 0;
                        cood.z = 430;
                        cood.roll = 180;
                        cood.roll *= kinematic_5dof_robot.DEG_TO_RAD;
                        cood.pitch = 0;
                        cood.yaw = 0;

                        P2P(cood, 20000, 200000);
                        Thread.Sleep(100);
                        state++;
                        break;
                    case 1:
                        if (Wmx3Lib.coreMotionStatus.AxesStatus[2 + Constants.AXES_OFFSET].OpState != WMX3ApiCLR.OperationState.Idle)
                            break;

                        Global.CheckCyclicBuffer = true;
                        Global.startCyclicBuffer = true;
                        StarMotion(vel);
                        //Wmx3Lib.StartCyclicBuffer();
                        state++;
                        Thread.Sleep(100);
                        break;
                    case 2:
                        if (Global.CheckCyclicBuffer == true)
                            break;

                        CircularMotion(circleVel, 1, 40, true);
                        Global.CheckCyclicBuffer = true;
                        Global.startCyclicBuffer = true;
                        //Wmx3Lib.StartCyclicBuffer();
                        Thread.Sleep(100);
                        state++;
                        break;
                    case 3:
                        if (Global.CheckCyclicBuffer == true)
                            break;

                        COORDINATE cood2 = new COORDINATE();
                        cood2.x = 315;
                        cood2.y = 0;
                        cood2.z = 430;
                        cood2.roll = 180;
                        cood2.roll *= kinematic_5dof_robot.DEG_TO_RAD;
                        cood2.pitch = 0;
                        cood2.yaw = 0;

                        P2P(cood2, 20000, 200000);
                        state++;
                        break;
                    case 4:
                        if (Wmx3Lib.coreMotionStatus.AxesStatus[2 + Constants.AXES_OFFSET].OpState != WMX3ApiCLR.OperationState.Idle)
                            break;

                        Thread.Sleep(100);

                        list_Jog.Clear();
                        GetCurrentPos_ForwardKin();

                        REC_initialX = currentCoord.x;
                        REC_initialY = currentCoord.y;
                        REC_initialZ = currentCoord.z;
                        REC_initialRx = currentCoord.roll;
                        REC_initialRy = currentCoord.pitch;
                        REC_initialRz = currentCoord.yaw;

                        int error = YLineMotion(1, -1, 0, 31, vel - (150 * velOverride),true);
                        
                        //Global.CheckCyclicBuffer = true;
                        //Wmx3Lib.StartCyclicBuffer();
                        state++;
                        break;
                    case 5:
                        //REC_initialX = 420;
                        REC_initialY = -31;
                        

                        YLineMotion(1, 1, 80, 0, vel - (50 * velOverride));
                        //Global.CheckCyclicBuffer = true;
                        //Wmx3Lib.StartCyclicBuffer();
                        state++;
                        break;
                    case 6:
                        REC_initialX = 395;

                        error = YLineMotion(1, 1, 0, 62, vel - (50 * velOverride));
                        //Global.CheckCyclicBuffer = true;
                        //Wmx3Lib.StartCyclicBuffer();
                        errorCount = 0;
                        state++;
                        break;
                    case 7:
                        REC_initialY = 31;

                        error = YLineMotion(-1, 1, 80, 0, vel - (50 * velOverride));
                        //Global.CheckCyclicBuffer = true;
                        //Wmx3Lib.StartCyclicBuffer();
                        errorCount = 0;
                        state++;
                        break;
                    case 8:
                        REC_initialX = 315; 

                        error = YLineMotion(1,-1, 0, 31, vel - (150 * velOverride));

                        Wmx3Lib.CyclicMotion(list_Jog.Count, list_Jog);
                        list_Jog.Clear();

                        Global.CheckCyclicBuffer = true;
                        Global.startCyclicBuffer = true;
                        //Wmx3Lib.StartCyclicBuffer();
                        errorCount = 0;
                        state++;
                        break;
                    case 9:
                        if (Global.CheckCyclicBuffer == true)
                            break;

                        if (loop == true)
                            state = 0;
                        else
                        {
                            demoDone = true;
                            Global.demoStart = false;
                        }

                        Thread.Sleep(100);
                        break;
                }
                Thread.Sleep(100);
            }
            list_Jog.Clear();
            return 0;
        }
    }
}

