﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGUS_5DoF_Demo
{
    public static class Constants
    {
		public const int AXES_OFFSET = 0;
        public const int NUM_OF_AXIS = 5;
		public const int Axis0 = 0 + AXES_OFFSET;
		public const int Axis1 = 1 + AXES_OFFSET;
		public const int Axis2 = 2 + AXES_OFFSET;
		public const int Axis3 = 3 + AXES_OFFSET;
		public const int Axis4 = 4 + AXES_OFFSET;

		public const int Pulse_Max_Diff = 200;


		public static  double[] LINK_LENGTH = new double[] { 0, 350, 270, 0, 0 };
        public static double[] JOINT_DISTANCE = new double[] { 312.5, 0, 0, 0, 190.2 };
        public static double[] TWIST_ANGLE = new double[] { Math.PI / 2, 0, 0, Math.PI / 2, 0 };
        public static double[] JOINT_ANGLE = new double[] { 0, Math.PI / 2, -Math.PI / 2, 0, 0 };


		public static AXESDATA gearratio = new AXESDATA()
		{
			Axis0 = 480000,
			Axis1 = 480000,
			Axis2 = 500000,
			Axis3 = 380000,
			Axis4 = 280000
		};

		// Robot's Specification: + Direction limit AND - Direction limit
		//		Axis0: -140 ~ 140
		//		Axis1:  -90 ~  50
		//		Axis2:  -25 ~ 110
		//		Axis3:  -10 ~ 160
		//		Axis4: -175 ~ 175
		public static AXESDATA thetamax = new AXESDATA()
		{
			Axis0 = 140,
			Axis1 = 50,
			Axis2 = 110,
			Axis3 = 160,
			Axis4 = 175
		};

		public static AXESDATA thetamin = new AXESDATA()
		{
			Axis0 = -140,
			Axis1 = -90,
			Axis2 = -25,
			Axis3 = -10,
			Axis4 = -175
		};

		public enum ErrorNum
        {
			ForwardKineFail = -1000,
			InverseKineFail,
			AngleOutOfRange,

			ExceedPulseDiffLimit,
			MovingDistanceShort,
			VelocityAccTooSlow,

			

			NoConstantVelocityPeriod,

			CircleExceedsWrkSpace
        }
	}
}
