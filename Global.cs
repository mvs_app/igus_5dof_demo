﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using WMX3ApiCLR;

namespace IGUS_5DoF_Demo
{
    public static class Global
    {
        public static bool isCommunicating = false;
        public static bool motorsOn = false;
        public static bool closeUpdateThread = false;

        public static bool isInJogMotion = false;

        public static int commandCount = 0;
        public static bool CheckCyclicBuffer = false;

        public static bool HomingStart = false;
        public static bool SingleHomingStart = false;

        public static bool programmedMotionStart = false;
        public static bool p2pStart = false;

        public static bool demoStart = false;
        public static bool StopBuffer = false;

        public static bool cyclicbufferopen = false;

        public static bool startCyclicBuffer = false;
        public static bool cyclicBufferRunning = false;

        public static EngineState prevEngineState = EngineState.Shutdown;

        public static COORDINATE currentCoord = new COORDINATE();

        public static Mutex rwStatusLock = new Mutex();
        
        public static string lastError = "";
    }
}
