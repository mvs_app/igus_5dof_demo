﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using WMX3ApiCLR;
using System.Reflection;


namespace IGUS_5DoF_Demo
{
    public partial class MainForm : Form
    {
        private BackgroundWorker uiUpdate;

        AXESDATA data = new AXESDATA();

        public MainForm()
        {
            int error;
            InitializeComponent();

            Wmx3Lib.Open();

            error = Wmx3Lib.SetParam();
            if (error != ErrorCode.None)
                DisplayMessage("[System] Failed to set parameters");
            else
                DisplayMessage("[System] Succesfully set parameters");

            Wmx3Lib.SetAxis();

            DataGridViewInit();

            uiUpdate = new BackgroundWorker();
            uiUpdate.DoWork += new DoWorkEventHandler(uiUpdate_DoWork);
            uiUpdate.ProgressChanged += new ProgressChangedEventHandler(UpdateUI);
            uiUpdate.WorkerReportsProgress = true;

            uiUpdate.RunWorkerAsync();

            typeof(DataGridView).InvokeMember("DoubleBuffered", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Instance | BindingFlags.SetProperty, null,
                                          dataGridView_MotorStatus, new object[] { true });
        }

        private void DataGridViewInit()
        {
            for(int axis = Constants.Axis0; axis < Constants.NUM_OF_AXIS + Constants.AXES_OFFSET; axis++)
            {
                dataGridView_MotorStatus.Rows.Add(axis.ToString(), "Idle", "0", "0", "0");
            }

            dataGridView_RobotPos.Rows.Add("Tx", "0", "Rx", "0");
            dataGridView_RobotPos.Rows.Add("Ty", "0", "Ry", "0");
            dataGridView_RobotPos.Rows.Add("Tz", "0", "Rz", "0");

            dataGridView_MotorStatus.Refresh();
        }

        int prevHomecount = 0;

        #region UpdateUI BackgroundWorker
        private void UpdateUI(object sender, ProgressChangedEventArgs e)
        {
            if (Global.closeUpdateThread)
                return;

            //txtBox_commandCount.Text = Global.startCyclicBuffer.ToString();

            if (Wmx3Lib.coreMotionStatus.EngineState != Global.prevEngineState)
            {
                if (Wmx3Lib.coreMotionStatus.EngineState == EngineState.Communicating)
                {
                    label_IndCom.Text = "Communicating";
                    label_IndCom.ForeColor = Color.FromArgb(156, 255, 0);
                    //dataGridView_MotorStatus.DefaultCellStyle.ForeColor = SystemColors.ControlText;
                    dataGridView_MotorStatus.Enabled = true;
                }
                else
                {
                    label_IndCom.Text = "Communication Stopped";
                    label_IndCom.ForeColor = Color.FromArgb(255, 55, 55);
                    //dataGridView_MotorStatus.DefaultCellStyle.ForeColor = Color.White;
                    dataGridView_MotorStatus.Enabled = false;
                }
            }

            int Oncount = 0;
            int homeCount = 0;
            
            for (int i = 0; i < Constants.NUM_OF_AXIS; i++)
            {
                if(Wmx3Lib.coreMotionStatus.AxesStatus[i + Constants.AXES_OFFSET].AmpAlarm)
                    dataGridView_MotorStatus.Rows[i].Cells[1].Value = "ALARM";
                else if (Wmx3Lib.coreMotionStatus.EngineState != EngineState.Communicating)
                    dataGridView_MotorStatus.Rows[i].Cells[1].Value = "Offline";
                else
                {
                    if (Wmx3Lib.coreMotionStatus.AxesStatus[i + Constants.AXES_OFFSET].ServoOn)
                    {
                        Oncount++;
                        dataGridView_MotorStatus.Rows[i].Cells[1].Value = Wmx3Lib.coreMotionStatus.AxesStatus[i + Constants.AXES_OFFSET].OpState.ToString();
                    }
                    else
                        dataGridView_MotorStatus.Rows[i].Cells[1].Value = "OFF";
                }

                dataGridView_MotorStatus.Rows[i].Cells[2].Value = Math.Round(Wmx3Lib.coreMotionStatus.AxesStatus[i + Constants.AXES_OFFSET].PosCmd).ToString();
                dataGridView_MotorStatus.Rows[i].Cells[3].Value = Math.Round(Wmx3Lib.coreMotionStatus.AxesStatus[i + Constants.AXES_OFFSET].ActualPos).ToString();
                dataGridView_MotorStatus.Rows[i].Cells[4].Value = Math.Round(RobotLib.ConvertPulseToDegrees(Wmx3Lib.coreMotionStatus.AxesStatus[i + Constants.AXES_OFFSET].ActualPos,i),1);

                switch (i)
                {
                    case 0:
                        data.Axis0 = Wmx3Lib.coreMotionStatus.AxesStatus[i + Constants.AXES_OFFSET].ActualPos;
                        break;
                    case 1:
                        data.Axis1 = Wmx3Lib.coreMotionStatus.AxesStatus[i + Constants.AXES_OFFSET].ActualPos;
                        break;
                    case 2:
                        data.Axis2 = Wmx3Lib.coreMotionStatus.AxesStatus[i + Constants.AXES_OFFSET].ActualPos;
                        break;
                    case 3:
                        data.Axis3 = Wmx3Lib.coreMotionStatus.AxesStatus[i + Constants.AXES_OFFSET].ActualPos;
                        break;
                    case 4:
                        data.Axis4 = Wmx3Lib.coreMotionStatus.AxesStatus[i + Constants.AXES_OFFSET].ActualPos;
                        break;
                }

                if (Wmx3Lib.coreMotionStatus.AxesStatus[i + Constants.AXES_OFFSET].HomeDone)
                {
                    homeCount++;
                }
                    
            }

            data = RobotLib.ConvertPulsetoRad(data);

            COORDINATE coord = RobotLib.ForwardKin(data); 
            if(coord != null)
            {
                dataGridView_RobotPos.Rows[0].Cells[1].Value = Math.Round(coord.x, 2);
                dataGridView_RobotPos.Rows[1].Cells[1].Value = Math.Round(coord.y, 2);
                dataGridView_RobotPos.Rows[2].Cells[1].Value = Math.Round(coord.z, 2);
                dataGridView_RobotPos.Rows[0].Cells[3].Value = Math.Round(coord.roll * kinematic_5dof_robot.RAD_TO_DEG, 2);
                dataGridView_RobotPos.Rows[1].Cells[3].Value = Math.Round(coord.pitch * kinematic_5dof_robot.RAD_TO_DEG, 2);
                dataGridView_RobotPos.Rows[2].Cells[3].Value = Math.Round(coord.yaw * kinematic_5dof_robot.RAD_TO_DEG, 2);
            }

            if (Oncount == Constants.NUM_OF_AXIS)
            {
                label_IndMotor.Text = "Motors On";
                label_IndMotor.ForeColor = Color.FromArgb(156, 255, 0);
            }
            else
            {
                label_IndMotor.Text = "Motors Off";
                label_IndMotor.ForeColor = Color.FromArgb(255, 55, 55);
            }

            if(homeCount != prevHomecount)
            {
                if (homeCount == Constants.NUM_OF_AXIS)
                {
                    prevHomecount = homeCount;
                    label_IndHome.Text = "Home Done";
                    label_IndHome.ForeColor = Color.FromArgb(156, 255, 0);
                    DisplayMessage("[Homing] Homing Done");
                }
                else
                {
                    prevHomecount = homeCount;
                    label_IndHome.Text = "Home Not Done";
                    label_IndHome.ForeColor = Color.FromArgb(255, 55, 55);
                }
            }

            //only for jog motion
            if (Global.startCyclicBuffer & Wmx3Lib.cycle_status.Status[0].RemainCount > 50)
            {
                Wmx3Lib.StartCyclicBuffer();
                Global.startCyclicBuffer = false;
            }

            if (Global.isInJogMotion & !Global.startCyclicBuffer & Wmx3Lib.cycle_status.Status[2].RemainCount == 0)
            {
                Global.isInJogMotion = false;
                Wmx3Lib.StopCyclicMotion();
            }


            //txtBox_cyclicbufferStatus.Text = Wmx3Lib.cycle_status.Status[0].RemainCount.ToString(); 
            if (Global.CheckCyclicBuffer)
            {
                if (Wmx3Lib.cycle_status.Status[0].RemainCount == 0)
                {                    
                    Wmx3Lib.StopCyclicMotion();
                    Global.CheckCyclicBuffer = false;
                    if(!Global.demoStart)
                    {
                        tabControl_Motion.Enabled = true;
                    }     
                }
                else
                {
                    if (!Global.demoStart && tabControl_Motion.Enabled == true)
                    {
                        tabControl_Motion.Enabled = false;
                    }
                    else
                    {
                        grpBox_ProgrammedMotion.Enabled = false;
                        grpBox_P2PMotion.Enabled = false;
                        bt_DemoStart.Enabled = false;
                    }
                }
            }

            if(!Global.demoStart && grpBox_P2PMotion.Enabled == false)
            {
                grpBox_ProgrammedMotion.Enabled = true;
                grpBox_P2PMotion.Enabled = true;
                bt_DemoStart.Enabled = true;
            }

            
            if(Global.HomingStart)
            {
                if (homeCount == Constants.NUM_OF_AXIS)
                {
                    Global.HomingStart = false;
                    tabControl_Motion.Enabled = true;

                    for (int i = Constants.Axis0; i < Constants.NUM_OF_AXIS; i++)
                    {
                        Wmx3Lib.SetFeedbackPos(i, 0);

                        Thread.Sleep(50);
                        Wmx3Lib.SetCommandPosToFeedbackPos(i);
                    }

                }
                else
                {
                    tabControl_Motion.Enabled = false;
                }
            }

            if (Global.p2pStart)
            {
                grpBox_ProgrammedMotion.Enabled = false;
                grpBox_MultipleShape.Enabled = false;
                int count= 0;
                for (int i = Constants.Axis0; i < Constants.NUM_OF_AXIS + Constants.AXES_OFFSET; i++)
                {
                    if (Wmx3Lib.coreMotionStatus.AxesStatus[i].OpState == OperationState.Idle)
                        count++;
                }
                if(count == Constants.NUM_OF_AXIS)
                {
                    Global.p2pStart = false;
                    grpBox_ProgrammedMotion.Enabled = true;
                    grpBox_MultipleShape.Enabled = true;
                }
            }
        }

        private void uiUpdate_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            while (!Global.closeUpdateThread)
            {
                Wmx3Lib.UpdateStatus();
                worker.ReportProgress(1);
                Thread.Sleep(50);
            }

        }
        #endregion

        private void DisplayMessage(string msg)
        {
            txtBox_SysMsg.Text += msg + "\r\n";
            txtBox_SysMsg.SelectionStart = txtBox_SysMsg.Text.Length;
            txtBox_SysMsg.ScrollToCaret();
        }

        #region DataGridView + Panel Events
        private void dataGridView_MotorStatus_SelectionChanged(object sender, EventArgs e)
        {
            dataGridView_MotorStatus.ClearSelection();
        }

        private void dataGridView_RobotPos_SelectionChanged(object sender, EventArgs e)
        {
            dataGridView_RobotPos.ClearSelection();
        }

        bool TagMove;
        int mValX, mValY;

        private void panel_TitleBar_MouseMove(object sender, MouseEventArgs e)
        {
            if (TagMove == true)
            {
                this.SetDesktopLocation(MousePosition.X - mValX, MousePosition.Y - mValY);
            }
        }

        private void panel_TitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            TagMove = true;
            mValX = e.X;
            mValY = e.Y;
        }

        private void panel_TitleBar_MouseUp(object sender, MouseEventArgs e)
        {
            TagMove = false;
        }
        #endregion

        #region Button Events
        private void btn_StartComm_Click(object sender, EventArgs e)
        {
            int error = Wmx3Lib.StartCommunication();

            if (error != ErrorCode.None)
                DisplayMessage("[System] Failed to start communication");
            else
                DisplayMessage("[System] Communication started");
        }

        private void btn_MotorOn_Click(object sender, EventArgs e)
        {
            int error = Wmx3Lib.ServoOn();

            if(!Global.cyclicbufferopen)
            {
                if (Wmx3Lib.OpenCyclicBuffer())
                    Global.cyclicbufferopen = true;
            }

            if (error != ErrorCode.None)
            {
                Global.lastError = WMX3Api.ErrorToString(error);
                DisplayMessage("[System] Failed to turn on all motors. ErrorCode: " + error.ToString() + ", " + Global.lastError);
            }
                
        }

        private void btn_Home_Click(object sender, EventArgs e)
        {
            int error = Wmx3Lib.StartHome();

            if (error != ErrorCode.None)
            {
                DisplayMessage("[Homing] Failed to move to home position, ErrorCode: " + error.ToString() + ", " + Global.lastError);

            }  
            else
            {
                DisplayMessage("[Homing] Homing Start");
                Global.HomingStart = true;
            }
                
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Global.closeUpdateThread = true;
            uiUpdate.Dispose();
            Wmx3Lib.ServoOff();

            Thread.Sleep(100);
            Wmx3Lib.Close();
        }

        private void btn_Jog_Click(object sender, MouseEventArgs e)
        {
            Global.StopBuffer = false;
            int velocity;
            int acceleration;
            int error = 0;

            if (!Int32.TryParse(txtBox_JointJogVel.Text, out velocity))
                error++;
            if (!Int32.TryParse(txtBox_JointJogAcc.Text, out acceleration))
                error++;

            if (error > 0)
            {
                DisplayMessage("[Joint JOG] Invalid velocity and acceleration inputs");
                return;
            }

            Button BtnEvent = (Button)sender;

            int axis = Convert.ToInt32(BtnEvent.TabIndex.ToString()) + Constants.AXES_OFFSET;
            int tag = Convert.ToInt32(BtnEvent.Tag.ToString());
            bool forwardDir;
            if (tag == -1)
                forwardDir = false;
            else
                forwardDir = true;

            error = Wmx3Lib.Jog(forwardDir, velocity, acceleration, axis);
            if (error != ErrorCode.None)
            {
                Global.lastError = CoreMotion.ErrorToString(error);
                DisplayMessage("[Joint JOG] Failed Jog Motion, ErrorCode: " + error.ToString() + ", " + Global.lastError);
            }
            else
                DisplayMessage("[Joint JOG] Jog Motion Start, Joint: " + axis.ToString() + ", Velocity: " + (velocity * tag).ToString() + ", Accel: " + acceleration.ToString());

        }

        private void btn_Jog_Unclick(object sender, MouseEventArgs e)
        {
            Button BtnEvent = (Button)sender;

            int axis = Convert.ToInt32(BtnEvent.TabIndex.ToString());

            Wmx3Lib.Stop(axis + Constants.AXES_OFFSET);
        }

        private int homeErrorCode;

        private void btn_SingleJointHome_Click(object sender, EventArgs e)
        {
            Button BtnEvent = (Button)sender;

            int axis = Convert.ToInt32(BtnEvent.Tag.ToString());
            Global.SingleHomingStart = true;
            //int error = Wmx3Lib.SingleJointHome(axis + Constants.AXES_OFFSET);
            JointHomeTask(axis + Constants.AXES_OFFSET);

            
        }

        private async void JointHomeTask(int axis)
        {
            Task<int> task = Task.Run(() =>
            {
                return Wmx3Lib.SingleJointHome(axis);
            });

            await task;
            if(Global.SingleHomingStart)
            {
                Thread.Sleep(100);
                Wmx3Lib.SetFeedbackPos(axis, 0);
                Thread.Sleep(50);
                Wmx3Lib.SetCommandPosToFeedbackPos(axis);

                homeErrorCode = task.Result;
                if (homeErrorCode != ErrorCode.None)
                {
                    Global.lastError = CoreMotion.ErrorToString(homeErrorCode);
                    DisplayMessage("[Joint Home] Failed to send Joint " + axis.ToString() + " to home position, ErrorCode: " + homeErrorCode.ToString() + ", " + Global.lastError);
                }
                else
                    DisplayMessage("[Joint Home] Successfully Sent Joint " + axis.ToString() + " to Home Position");
            }
            else
            {
                DisplayMessage("[Joint Home] Stopped sending Joint " + axis.ToString() + " to Home Position");
            }
            Global.SingleHomingStart = false;
        }

        private void btn_CartJog_Down(object sender, MouseEventArgs e)
        {
            if (Global.isInJogMotion == true)
                return;

            Button BtnEvent = (Button)sender;
            Global.StopBuffer = false;

            double vel;
            double acc;
            double dist;

            int error_input = 0;
            if (!Double.TryParse(txtBox_CartJogVel.Text, out vel))
                error_input++;
            if (!Double.TryParse(txtBox_CartJogAcc.Text, out acc))
                error_input++;
            if (!Double.TryParse(txtBox_CartJogDist.Text, out dist))
                error_input++;

            if (error_input > 0)
            {
                DisplayMessage("[Cartesian JOG] Invalid velocity, acceleration, and distance inputs");
                return;
            }


            Wmx3Lib.AbortCyclicBuffer();

            int direction = Convert.ToInt32(BtnEvent.Tag.ToString());
            int plane = Convert.ToInt32(BtnEvent.TabIndex.ToString());
            string s_plane = "X";

            switch(plane)
            {
                case 0:
                    s_plane = "X";
                    break;
                case 1:
                    s_plane = "Y";
                    break;
                case 2:
                    s_plane = "Z";
                    break;
            }

            CartesianJog(dist, vel, acc, plane, direction, s_plane);
              
        }

        private async Task CartesianJog(double dist, double vel, double acc, int plane, int direction, string s_plane)
        {
            int error = 0;
            await Task.Run(() =>
            {
                Global.startCyclicBuffer = true;
                Global.isInJogMotion = true;
                error = RobotLib.JogMotion(dist, vel, acc, plane, direction);

            });

            // int error = Convert.ToInt32(threadReturn);


            if (error == (int)Constants.ErrorNum.ExceedPulseDiffLimit)
                DisplayMessage("[Cartesian JOG] Velocity and Acceleration Parameters are too high, please change velocity and acceleration parameters");
            else if (error == (int)Constants.ErrorNum.MovingDistanceShort)
                DisplayMessage("[Cartesian JOG] Moving Distance is too short");
            else if (error == (int)Constants.ErrorNum.InverseKineFail)
                DisplayMessage("[Cartesian JOG] Current Position has reached workspace limit");
            else if (error == (int)Constants.ErrorNum.AngleOutOfRange)
                DisplayMessage("[Cartesian JOG] Current Position has reached the Joint limit");
            else if (error == (int)Constants.ErrorNum.ForwardKineFail)
                DisplayMessage("[Cartesian JOG] Forward Kinematics Fail, cannot calculate current position as the tool frame is not within the workspace");
            else if (error == (int)Constants.ErrorNum.VelocityAccTooSlow)
                DisplayMessage("[Cartesian JOG] Velocity and Acceleration settings are too low, please increase velocity and acceleration");
            else if (error == (int)Constants.ErrorNum.NoConstantVelocityPeriod)
                DisplayMessage("[Cartesian JOG] No Constant Velocity Profile, please decrease velocity and acceleration");
            else
                DisplayMessage("[Cartesian JOG] Translation Start in the " + s_plane + " direction, velocity: " + (vel * direction).ToString() + " mm/s, accel: " + acc.ToString() + " mm/s^2");

            if(error != ErrorCode.None)
            {
                Wmx3Lib.AbortCyclicBuffer();
                Global.startCyclicBuffer = false;
                Global.isInJogMotion = false;
            }
        }

        private void btn_CartJogAngle_Down(object sender, MouseEventArgs e)
        {
            if (Global.isInJogMotion == true)
                return;

            Button BtnEvent = (Button)sender;
            Global.StopBuffer = false;

            double vel;
            double acc;
            double angle;

            int error_input = 0;
            if (!Double.TryParse(txtBox_JogAngVel.Text, out vel))
                error_input++;
            if (!Double.TryParse(txtBox_JogAngAcc.Text, out acc))
                error_input++;
            if (!Double.TryParse(txtBox_CartJogAngle.Text, out angle))
                error_input++;

            if (error_input > 0)
            {
                DisplayMessage("[Cartesian JOG] Invalid angular velocity, acceleration, and angle inputs");
                return;
            }

            Wmx3Lib.AbortCyclicBuffer();

            angle *= kinematic_5dof_robot.DEG_TO_RAD;

            int direction = Convert.ToInt32(BtnEvent.Tag.ToString());
            int plane = Convert.ToInt32(BtnEvent.TabIndex.ToString());

            string s_plane = "X";

            switch (plane)
            {
                case 0:
                    s_plane = "X";
                    break;
                case 1:
                    s_plane = "Y";
                    break;
                case 2:
                    s_plane = "Z";
                    break;
            }

            CartAngularJog(angle, vel, acc, plane, direction, s_plane);

        }

        private async Task CartAngularJog(double angle, double vel, double acc, int plane, int direction, string s_plane)
        {

            int error = 0;
            await Task.Run(() =>
            {
                Global.startCyclicBuffer = true;
                Global.isInJogMotion = true;
                error = RobotLib.JogMotionAngular(angle, vel, acc, plane, direction);
             });

            if (error == (int)Constants.ErrorNum.ExceedPulseDiffLimit)
                DisplayMessage("[Cartesian JOG] Angular Velocity and  angular acceleration parameters are too high, please change angular velocity and angular acceleration parameters");
            else if (error == (int)Constants.ErrorNum.MovingDistanceShort)
                DisplayMessage("[Cartesian JOG] Moving Distance is too short");
            else if (error == (int)Constants.ErrorNum.NoConstantVelocityPeriod)
                DisplayMessage("[Cartesian JOG] No Constant Velocity Period");
            else if (error == (int)Constants.ErrorNum.ForwardKineFail)
                DisplayMessage("[Cartesian JOG] Forward Kinematics Fail, cannot calculate current position as the tool frame is not within the workspace");
            else if (error == (int)Constants.ErrorNum.InverseKineFail)
                DisplayMessage("[Cartesian JOG] Current Position has reached the workspace boundary. Inverse Kinematics Solution does not exist");
            else
                DisplayMessage("[Cartesian JOG] Rotation Start around the " + s_plane + "-axis, velocity: " + (vel * direction).ToString() + " rad/s, accel: " + acc.ToString() + " rad/s^2");

            if (error != ErrorCode.None)
            {
                Wmx3Lib.AbortCyclicBuffer();
                Global.startCyclicBuffer = false;
                Global.isInJogMotion = false;
            }
        }


        private void btn_CartJog_Up(object sender, MouseEventArgs e)
        {
           // int error = Wmx3Lib.StopCyclicMotion();

            //DisplayMessage(error.ToString());
        }

        private void btn_P2PStart_Click(object sender, EventArgs e)
        {
            double vel;
            double acc;
            Global.StopBuffer = false;

            COORDINATE p2pCoord = new COORDINATE();

            int error_input = 0;

            if (!double.TryParse(txtBox_P2PVel.Text, out vel))
                error_input++;
            if (!double.TryParse(txtBox_P2PAcc.Text, out acc))
                error_input++;

            if (!double.TryParse(txtBox_Tx.Text, out p2pCoord.x))
                error_input++;
            if (!double.TryParse(txtBox_Ty.Text, out p2pCoord.y))
                error_input++;
            if (!double.TryParse(txtBox_Tz.Text, out p2pCoord.z))
                error_input++;

            if (!double.TryParse(txtBox_Rx.Text, out p2pCoord.roll))
                error_input++;
            if (!double.TryParse(txtBox_Ry.Text, out p2pCoord.pitch))
                error_input++;
            if (!double.TryParse(txtBox_Rz.Text, out p2pCoord.yaw))
                error_input++;

            if (error_input > 0)
            {
                DisplayMessage("[P2P Motion] Invalid user inputs");
                return;
            }

            p2pCoord.roll *= kinematic_5dof_robot.DEG_TO_RAD;
            p2pCoord.pitch *= kinematic_5dof_robot.DEG_TO_RAD;
            p2pCoord.yaw *= kinematic_5dof_robot.DEG_TO_RAD;

            int error = RobotLib.P2P(p2pCoord, vel, acc);

            if (error == (int)Constants.ErrorNum.ForwardKineFail)
                DisplayMessage("[P2P Motion] Failed to calculate current robot tool frame position, please try again");
            else if (error == (int)Constants.ErrorNum.InverseKineFail)
                DisplayMessage("[P2P Motion] Final Position is out of the work space");
            else if (error == (int)Constants.ErrorNum.AngleOutOfRange)
                DisplayMessage("[P2P Motion] Final position exceeds the Joint limit range");
            else if (error != ErrorCode.None)
                DisplayMessage("[P2P Motion] WMX Interpolation Error, Error Code: " + error + ", " + Global.lastError);
            else
            {
                DisplayMessage("[P2P Motion] P2P Motion start, velocity: " + vel.ToString() + " pulse/s, acc: " + acc.ToString() + " pulse/s^2, Target: ("
                   + txtBox_Tx.Text + ", " + txtBox_Ty.Text + ", " + txtBox_Tz.Text + ", " + txtBox_Rx.Text + ", " + txtBox_Ry.Text + ", " + txtBox_Rz.Text + ")");
                Global.p2pStart = true;
            }
               
        }

        private void btn_StopP2P_Click(object sender, EventArgs e)
        {
            Wmx3Lib.StopAllMotors();
            //grpBox_ProgrammedMotion.Enabled = true;
        }

        private void picBox_Close_MouseEnter(object sender, EventArgs e)
        {
            picBox_Close.Image = IGUS_5DoF_Demo.Properties.Resources.X_mouseOver;
            picBox_Close.Refresh();
        }

        private void picBox_Close_MouseLeave(object sender, EventArgs e)
        {
            picBox_Close.Image = IGUS_5DoF_Demo.Properties.Resources.X_noMouse;
            picBox_Close.Refresh();
        }

        private void picBox_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_StopComm_Click(object sender, EventArgs e)
        {
            Wmx3Lib.StopCommunication();
        }

        private void btn_MotorsOff_Click(object sender, EventArgs e)
        {
            Wmx3Lib.ServoOff();
        }

        private void btn_ClearSysMsg_Click(object sender, EventArgs e)
        {
            txtBox_SysMsg.Clear();
        }

        private void btn_LineMotion_Click(object sender, EventArgs e)
        {
            double vel;
            double length;

            Global.StopBuffer = false;

            int error_inputs = 0;

            if (!Double.TryParse(txtBox_LineVel.Text, out vel))
                error_inputs++;
            if (!Double.TryParse(txtBox_SquareLength.Text, out length))
                error_inputs++;

            if(error_inputs > 0)
            {
                DisplayMessage("[Line Motion] Invalid velocity and length inputs");
                return;
            }

            object threadReturn = null;

            Thread line = new Thread(() => threadReturn = RobotLib.LineMotion(vel, length));
            line.Start();
            line.Join();

            int error = Convert.ToInt32(threadReturn);

            if (error == (int)Constants.ErrorNum.InverseKineFail)
                DisplayMessage("[Line Motion] The Square exceeds workspace limits");
            else if (error == (int)Constants.ErrorNum.AngleOutOfRange)
                DisplayMessage("[Line Motion] One of the positions of the square exceed the angle limit");
            else if (error == (int)Constants.ErrorNum.NoConstantVelocityPeriod)
                DisplayMessage("[Line Motion] Please change velocity and length parameters");
            else
                DisplayMessage("[Line Motion] Line Motion Start, length: " + length.ToString() + " mm, Velocity: " + vel.ToString() + " mm/s");

            Wmx3Lib.StartCyclicBuffer();

            Global.CheckCyclicBuffer = true;
        }

        private void Btn_Stop_Click(object sender, EventArgs e)
        {
            if( Global.demoStart )
            {
                Global.startCyclicBuffer = false;
                Global.StopBuffer = false;
                RobotLib.StopDemo();
                Wmx3Lib.StopCyclicMotion();
                //Wmx3Lib.EmergencyStop();

            }
            else if (Global.CheckCyclicBuffer)
            {
                Global.StopBuffer = false;
                Wmx3Lib.StopCyclicMotion();
                Global.CheckCyclicBuffer = false;
                tabControl_Motion.Enabled = true;
            }
            else if(Global.HomingStart)
            {
                Global.HomingStart = false;
                Wmx3Lib.StopAllMotors();
                tabControl_Motion.Enabled = true;
            }
            else if (Global.SingleHomingStart)
            {
                Global.SingleHomingStart = false;
                Wmx3Lib.StopAllMotors();
            }
            else
            {
                Global.StopBuffer = false;
                Wmx3Lib.StopAllMotors();
                Wmx3Lib.StopCyclicMotion();
                tabControl_Motion.Enabled = true;
            }
        }

        private void btn_ClearAlarm_Click(object sender, EventArgs e)
        {
            Wmx3Lib.ClearAlarm();
        }

        private void btn_StartComm_MouseLeave(object sender, EventArgs e)
        {
            btn_StartComm.BackgroundImage = Properties.Resources.btn_start_comm_n;
        }

        private void btn_StartComm_MouseEnter(object sender, EventArgs e)
        {
            btn_StartComm.BackgroundImage = Properties.Resources.btn_start_comm_o1;
        }

        private void btn_StopComm_MouseEnter(object sender, EventArgs e)
        {
            btn_StopComm.BackgroundImage = Properties.Resources.btn_stop_comm_o1;
        }

        private void btn_StopComm_MouseLeave(object sender, EventArgs e)
        {
            btn_StopComm.BackgroundImage = Properties.Resources.btn_stop_comm_n;
        }

        private void btn_MotorOn_MouseEnter(object sender, EventArgs e)
        {
            btn_MotorOn.BackgroundImage = Properties.Resources.btn_moter_on_o1;
        }

        private void btn_MotorOn_MouseLeave(object sender, EventArgs e)
        {
            btn_MotorOn.BackgroundImage = Properties.Resources.btn_moter_on_n;
        }
        private void btn_MotorsOff_MouseEnter(object sender, EventArgs e)
        {
            btn_MotorsOff.BackgroundImage = Properties.Resources.btn_moter_off_o1;
        }

        private void btn_MotorsOff_MouseLeave(object sender, EventArgs e)
        {
            btn_MotorsOff.BackgroundImage = Properties.Resources.btn_moter_off_n;
        }

        private void btn_StopP2P_MouseEnter(object sender, EventArgs e)
        {
            btn_StopP2P.BackgroundImage = Properties.Resources.btn_motion_stop_o1;
        }

        private void btn_StopP2P_MouseLeave(object sender, EventArgs e)
        {
            btn_StopP2P.BackgroundImage = Properties.Resources.btn_motion_stop_n;
        }

        private void btn_P2PStart_MouseEnter(object sender, EventArgs e)
        {
            btn_P2PStart.BackgroundImage = Properties.Resources.btn_motion_start_o1;
        }

        private void btn_P2PStart_MouseLeave(object sender, EventArgs e)
        {
            btn_P2PStart.BackgroundImage = Properties.Resources.btn_motion_start_n;
        }

        private void btn_DemoStop_Click(object sender, EventArgs e)
        {
            RobotLib.StopDemo();
            //Wmx3Lib.StopCyclicMotion();
        }

        private void btn_LaserSwitch_Click(object sender, EventArgs e)
        {
            int _byte;
            int bit;
            int.TryParse(txtBox_byteAddr.Text, out _byte);
            int.TryParse(txtBox_bitAddr.Text, out bit);
            Wmx3Lib.SetIOBit(_byte, bit);
        }

        private void bt_DemoStart_Click(object sender, EventArgs e)
        {
            //double vel = 380;

            grpBox_ProgrammedMotion.Enabled = false;
            grpBox_P2PMotion.Enabled = false;
            bt_DemoStart.Enabled = false;

            Global.StopBuffer = false;
            Global.demoStart = true;
            double velOverride;

            Double.TryParse(txtBox_DemoVelocity.Text, out velOverride);
            if(velOverride > 100 || velOverride <10)
            {
                DisplayMessage("[Multiple Shape Demo] Velocity has to be between 10% and 100%");
                grpBox_ProgrammedMotion.Enabled = true;
                grpBox_P2PMotion.Enabled = true;
                bt_DemoStart.Enabled = true;
                return;
            }    
            velOverride = velOverride / 100;

            object threadReturn = null;
            
            Thread star = new Thread(() => threadReturn = RobotLib.MultipleShapeFSM(velOverride, chckBox_DemoLoop.Checked));
            star.Start();
            //star.Join();
            int error = Convert.ToInt32(threadReturn);

            if (error == (int)Constants.ErrorNum.InverseKineFail)
                DisplayMessage("[Multiple Shape Demo] Inverse Kinematics Error");
        }

        private void btn_Circular_Click(object sender, EventArgs e)
        {
            Global.StopBuffer = false;
            double vel;
            int numCircle;
            double radius;
            //double yCen;

            int error_inputs = 0;


            if (!int.TryParse(txtBox_circNum.Text, out numCircle))
                error_inputs++;
            if (!Double.TryParse(txtBox_angVel.Text, out vel))
                error_inputs++;
            if (!Double.TryParse(txtBox_Radius.Text, out radius))
                error_inputs++;

            if (error_inputs > 0)
            {
                DisplayMessage("[Circular Motion] Invalid velocity and length inputs");
                return;
            }


            object threadReturn = null;

            Thread circle = new Thread(() => threadReturn = RobotLib.CircularMotion(vel, numCircle, radius));
            circle.Start();
            circle.Join();

            int error = Convert.ToInt32(threadReturn);


            if (error == (int)Constants.ErrorNum.CircleExceedsWrkSpace)
                DisplayMessage("[Circular Motion] Circle exceeds workspace limits");
            else if (error == (int)Constants.ErrorNum.AngleOutOfRange)
                DisplayMessage("[Line Motion] One of the positions of the circle exceed the angle limit");
            else
                DisplayMessage("[Circular Motion] Circular Motion start, radius: " + radius.ToString() + " mm, velocity: " + vel.ToString() + "rad/s, num of circles: "
                    + numCircle.ToString());

            Wmx3Lib.StartCyclicBuffer();

            Global.CheckCyclicBuffer = true;
        }

        #endregion
    }
}
