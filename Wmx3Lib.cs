﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WMX3ApiCLR;
using System.Threading;

namespace IGUS_5DoF_Demo
{
    public static class Wmx3Lib
    {
        private static WMX3Api wmxLib;
        private static CoreMotion coreMotionLib;
        private static CyclicBuffer cyclicBufferLib;
        private static AxisSelection axisSelection;
        public static CyclicBufferMultiAxisStatus cycle_status;
        public static CoreMotionStatus coreMotionStatus;
        private static Io ioLib;

        #region Initialize

        public static bool Open()
        {
            wmxLib = new WMX3Api();
            coreMotionLib = new CoreMotion(wmxLib);

            cyclicBufferLib = new CyclicBuffer(wmxLib);

            ioLib = new Io(wmxLib);

            coreMotionStatus = new CoreMotionStatus();

            cycle_status = new CyclicBufferMultiAxisStatus();

            axisSelection = new AxisSelection();

            string lib_path = "C:\\Program Files\\SoftServo\\WMX3\\";
            int error = wmxLib.CreateDevice(lib_path, DeviceType.DeviceTypeNormal, 10000);
            if (error != ErrorCode.None)
            {
                wmxLib.SetDeviceName("IGUS_5DoF_Robot_Controller");
            }
            return true;
        }

        public static void SetAxis()
        {
            axisSelection.AxisCount = Constants.NUM_OF_AXIS;

            for (int i = 0; i < Constants.NUM_OF_AXIS; i++)
            {
                axisSelection.Axis[i] = i + Constants.AXES_OFFSET;
            }
        }

        public static void Close()
        {
            StopCommunication();

            cyclicBufferLib.CloseCyclicBuffer(axisSelection);

            wmxLib.CloseDevice();
        }

        public static int StartCommunication()
        {
            return wmxLib.StartCommunication();

        }

        public static void StopCommunication()
        {
            wmxLib.StopCommunication();
        }

        #endregion


        public static int ServoOn()
        {
            return coreMotionLib.AxisControl.SetServoOn(axisSelection, 1);
        }

        public static void ServoOff()
        {
            coreMotionLib.AxisControl.SetServoOn(axisSelection, 0);
        }

        public static int StartHome()
        {
            int error = coreMotionLib.Home.StartHome(axisSelection);
            if (error != ErrorCode.None)
                Global.lastError = CoreMotion.ErrorToString(error);

            return error;
        }

        public static void ClearAlarm()
        {
            coreMotionLib.AxisControl.ClearAmpAlarm(axisSelection);
            coreMotionLib.AxisControl.ClearAxisAlarm(axisSelection);
        }

        public static int SetParam()
        {
            //set gear ratio 
            // set home param
            Config.HomeParam homeParam = new Config.HomeParam();
            int error;

            coreMotionLib.Config.GetHomeParam(Constants.Axis0, ref homeParam);
            homeParam.HomeType = Config.HomeType.HSZPulse;
            homeParam.HomeDirection = Config.HomeDirection.Positive;

            homeParam.HomeShiftDistance = -4500;

            error = coreMotionLib.Config.SetHomeParam(Constants.Axis0, homeParam);
            if (error != ErrorCode.None)
                return error;

            homeParam.HomeDirection = Config.HomeDirection.Negative;
            homeParam.HomeShiftDistance = 54000;

            error = coreMotionLib.Config.SetHomeParam(Constants.Axis3, homeParam);
            if (error != ErrorCode.None)
                return error;

            homeParam.HomeDirection = Config.HomeDirection.Positive;

            homeParam.HomeShiftDistance = -5000;

            error = coreMotionLib.Config.SetHomeParam(Constants.Axis1, homeParam);
            if (error != ErrorCode.None)
                return error;

            homeParam.HomeShiftDistance = -2000;

            error = coreMotionLib.Config.SetHomeParam(Constants.Axis2, homeParam);
            if (error != ErrorCode.None)
                return error;

            homeParam.HomeShiftDistance = 0;
            homeParam.HomeType = Config.HomeType.CurrentPos;

            error = coreMotionLib.Config.SetHomeParam(Constants.Axis4, homeParam);
            if (error != ErrorCode.None)
                return error;

            error = coreMotionLib.Config.SetAxisPolarity(Constants.Axis0, -1);

            if (error != ErrorCode.None)
                return error;
            else
            {
                return ErrorCode.None;
            }

        }

        public static void EmergencyStop()
        {
            coreMotionLib.ExecEStop(EStopLevel.Level1);
        }

        public static void UpdateStatus()
        {
            //Global.rwStatusLock.WaitOne();
            coreMotionLib.GetStatus(ref coreMotionStatus);

            cyclicBufferLib.GetStatus(axisSelection, ref cycle_status);
            //Global.rwStatusLock.ReleaseMutex();
        }

        public static int Jog(bool _bforward, double dVel, double dAcc, int axisNo)
        {

            int jogDir = -1;
            if (_bforward)
                jogDir = 1;

            Motion.JogCommand jogCmd = new Motion.JogCommand();
            jogCmd.Axis = axisNo;
            jogCmd.Profile.Type = ProfileType.SCurve;
            jogCmd.Profile.Velocity = dVel * jogDir;
            jogCmd.Profile.Acc = dAcc;
            jogCmd.Profile.Dec = dAcc;

            return coreMotionLib.Motion.StartJog(jogCmd);
        }

        public static int Stop(int axisNo)
        {
            return coreMotionLib.Motion.Stop(axisNo);
        }

        public static int StopAllMotors()
        {
            return coreMotionLib.Motion.Stop(axisSelection);
        }

        public static int runIntpl(AXESDATA abspos, double vel, double acc)
        {
            Motion.LinearIntplCommand lin = new Motion.LinearIntplCommand();

            lin.AxisCount = Constants.NUM_OF_AXIS;

            for (int i = 0; i < Constants.NUM_OF_AXIS; i++)
            {
                lin.Axis[i] = i + Constants.AXES_OFFSET;

                lin.MaxAcc[i] = acc;
                lin.MaxDec[i] = acc;
                lin.MaxVelocity[i] = vel;
            }

            lin.Profile.Type = ProfileType.SCurve;
            lin.Profile.Velocity = vel;

            lin.Target[0] = abspos.Axis0;
            lin.Target[1] = abspos.Axis1;
            lin.Target[2] = abspos.Axis2;
            lin.Target[3] = abspos.Axis3;
            lin.Target[4] = abspos.Axis4;

            int error = coreMotionLib.Motion.StartLinearIntplPos(lin);
            if (error != ErrorCode.None)
                Global.lastError = CoreMotion.ErrorToString(error);

            return error;
        }

        public static int SingleJointHome(int axis)
        {
            int ret = coreMotionLib.Home.StartHome(axis);
            if(ret != 0)
            {
                return ret;
            }
            coreMotionLib.Motion.Wait(axis);
            return ret;
        }

        #region Cyclic Buffer
        public static void CyclicMotion(int size, List<AXESDATA> listJog)
        {
            CyclicBufferMultiAxisCommands cycle_buff_commands = new CyclicBufferMultiAxisCommands();
            //CyclicBufferMultiAxisOption cycle_options = new CyclicBufferMultiAxisOption();

            //int error_test = cyclicBufferLib.CloseCyclicBuffer(axisSelection);

            for (int j = 0; j < Constants.NUM_OF_AXIS; j++)
            {
                cycle_buff_commands.Cmd[j + Constants.AXES_OFFSET].IntervalCycles = 1;
                cycle_buff_commands.Cmd[j + Constants.AXES_OFFSET].Type = CyclicBufferCommandType.AbsolutePos;
            }

            for (int i = 0; i < size; i++)
            {
                if (Global.StopBuffer == true)
                    return; 

                try
                {
                    cycle_buff_commands.Cmd[0 + Constants.AXES_OFFSET].Command = listJog[i].Axis0;
                    cycle_buff_commands.Cmd[1 + Constants.AXES_OFFSET].Command = listJog[i].Axis1;
                    cycle_buff_commands.Cmd[2 + Constants.AXES_OFFSET].Command = listJog[i].Axis2;
                    cycle_buff_commands.Cmd[3 + Constants.AXES_OFFSET].Command = listJog[i].Axis3;
                    cycle_buff_commands.Cmd[4 + Constants.AXES_OFFSET].Command = listJog[i].Axis4;
                }
                catch
                {
                    return;
                }
               
                cyclicBufferLib.AddCommand(axisSelection, cycle_buff_commands);

            }
            
        }

        public static int StartCyclicBuffer()
        {
            //if (Global.startCyclicBuffer)
                return cyclicBufferLib.Execute(axisSelection);
            //else
                //return -1;
        }

        public static bool OpenCyclicBuffer()
        {
            int error = cyclicBufferLib.OpenCyclicBuffer(axisSelection, (uint)100000);
            if (error != ErrorCode.None)
                return false;
            else
                return true;
        }

        public static void AbortCyclicBuffer()
        {
            int error = cyclicBufferLib.Abort(axisSelection);
        }

        public static int StopCyclicMotion()
        {
            int error = coreMotionLib.Motion.ExecQuickStop(axisSelection);

            cyclicBufferLib.Abort(axisSelection);

            Global.startCyclicBuffer = false;

            if (error != ErrorCode.None)
                return error;
            else
                return 0;
        }

        public static void SetIOBit(int _byte, int bit)
        {
            byte data = 0;
            ioLib.GetOutBit(_byte, bit, ref data);
            if (data == 0)
                data = 1;
            else
                data = 0;

            ioLib.SetOutBit(_byte, bit, data);
        }

        public static void SetFeedbackPos(int joint, double value)
        {
            coreMotionLib.Home.SetFeedbackPos(joint, value);
        }

        public static void SetCommandPosToFeedbackPos(int joint)
        {
            coreMotionLib.Home.SetCommandPosToFeedbackPos(joint);
        }
        #endregion
    }
}
